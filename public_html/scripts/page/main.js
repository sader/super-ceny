$(function(){
   
    $('.box').on('click', 'h2' , function(){
        $(this).parent().find('.content').slideUp();
        $(this).addClass('slided'); 
    });
   
    $('.box').on('click', 'h2.slided' , function(){
        $(this).parent().find('.content').slideDown();
        $(this).removeClass('slided'); 
    });
    
    
    
    
    $('#add-rewiev .button').on('click',function(e){
        e.preventDefault();
        $("#add-rewiev form").slideDown();
    });
    Shadowbox.init();
   // $.localScroll({offset: -150});
    
    $('#bx-slider').bxSlider({
        minSlides: 4,
        maxSlides: 20,
        slideWidth: 145,
        slideMargin: 10,
        nextSelector : $('#navigate .next'),
        prevSelector : $('#navigate .prev')
    });
  
  //  $(document).scroll(function() {
  //      var $scroll = $(document).scrollTop();
  //      if($scroll > 112){
  //          $('#float').addClass('fixed');
  //          
  //      }
  //      else{
  //          $('#float').removeClass('fixed');
  //          
  //      }
  //  });
    
    $("#navigate button").on('click',function(e){
       e.preventDefault();


      $.ajax({
                type: "POST",
                url: '/page/index/shop',
                data : 'id=' + $('#breadcrumbs li.last a').attr('data'),
                dataType : 'json'
            }).done(function(data) {

                $('#shop-items').append(data);
                 $('.container.product').slideDown();

            });

      
    });
    $("#float").on('click', '.toggle' , function(){
        $("#top").hide();
        $("#float .visible").hide();
        $('#float').css('top' , '0px');
        $(this).removeClass('toggle').addClass('togglehide');
    });
    
    
    $("#float").on('click', '.togglehide', function(){
        $("#top").show();
        $("#float .visible").show();
        $('#float').css('top' , '58px');
        $(this).removeClass('togglehide').addClass('toggle');
    });
    
});

