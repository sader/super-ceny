var manager = {
init : function() {
    if (manager.widgets.init !== 'undefined')
        manager.widgets.init();
    if (manager.events.init !== 'undefined')
        manager.events.init();
    if (manager.handlers.init !== 'undefined')
        manager.handlers.init();
},
        widgets : {
        init : function() {
        }
        },
        events : {
        init : function() {

            $('body').on("click", "#type-3", function() {
                manager.handlers.subcategory(this);

            });

            $('body').on("click", "#type-4", function() {
                manager.handlers.subcategoryFilter(this);
            });

            $('body').on("click", "#all-items3", function() {
                manager.handlers.subcategorySelect(this);
            });
            
                $('body').on("click", "#all-items4", function() {
                manager.handlers.subcategoryFilterSelect(this);
            });
            
            $('body').on("click", "#submit", function() {
                manager.handlers.beforeSubmit();

            });



        }
        },
        handlers : {
        init : function() {
            manager.handlers.subcategory($('#type-3'));
             manager.handlers.subcategoryFilter($('#type-4'));
            
             manager.handlers.FilterEdit(); 
             
        },


           FilterEdit : function(){
               
               if(typeof(elem3) !== 'undefined')
               {
                   $('#all-items3').attr('checked' , false);
                   manager.handlers.subcategorySelect($('#all-items3') , elem3);
                    
               }
               
                 if(typeof(elem4) !== 'undefined')
               {
                   $('#all-items4').attr('checked' , false);
                         manager.handlers.subcategoryFilterSelect($('#all-items4') , elem4);
                   
               }
               
                
             
               
           },



            beforeSubmit : function() {

                if( $('#type-3').is(':checked'))
                {
                    if($('#all-items3').is(':checked'))
                    {
                       $('#cat3res').val('all');
                    }
                    else
                    {
                     $('#cat3res').val(($('#categories3').select2('val')));    
                    }
                }
                
                if( $('#type-4').is(':checked'))
                {
                    
                    if($('#all-items4').is(':checked'))
                    {
                        
                       $('#cat4res').val('all');
                        
                    }
                    else
                    {
                     $('#cat4res').val(($('#categories4').select2('val')));    
                    }
                }
                
            },



                subcategory : function(elem) {

                    if ($('#type-3').is(':checked'))
                    {
                        var checked = $('<input />').attr('type', 'checkbox')
                                .attr('id', 'all-items3')
                                .attr('checked', 'checked')
                                .attr('name', 'all-items3')
                                .after($('<span />').attr('class', 'check').text('Wszystkie podkategorie'));
                        var box = $('<div />').attr('id', 'box-3');
                        box.append(checked);
                        $(elem).closest('.checkboxes').addClass('box-selected');
                        $(elem).closest('.checkboxes').append(box);
                    }
                    else
                    {
                        $('#box-3').remove();
                        $(elem).closest('.checkboxes').removeClass('box-selected');
                    }



                },
                        subcategoryFilter : function(elem) {

                    if ($('#type-4').is(':checked'))
                    {
                        var checked = $('<input />').attr('type', 'checkbox')
                                .attr('id', 'all-items4')
                                .attr('checked', 'checked')
                                .attr('name', 'all-items4')
                                .after($('<span />').attr('class', 'check').text('Wszystkie podkategorie'));
                        var box = $('<div />').attr('id', 'box-4');
                        box.append(checked);
                        $(elem).closest('.checkboxes').addClass('box-selected');
                        $(elem).closest('.checkboxes').append(box);
                    }
                    else
                    {
                        $('#box-4').remove();
                        $(elem).closest('.checkboxes').removeClass('box-selected');
                    }



                },
                subcategorySelect : function(elem, data) {

                    if ($(elem).is(':checked'))
                    {
                         $('#categories3').select2('destroy');  
                         $('#categories3').remove(); 
                    }
                    else
                    {
                        $.ajax({
                            type: "POST",
                            url: '/admin/tag/multi',
                            dataType: 'json'
                        }).done(function(response) {
                                var multicheckbox = $('<select multiple />').attr('id', 'categories3'); 
                                $.each(response, function(index, value){
                                    var opt = $('<optgroup />').attr('label' , index); 
                                    $.each(value, function(ind, val){
                                        opt.append($('<option />').val(val.id).text(val.name)); 
                                    }); 
                                    multicheckbox.append(opt); 
                                    
                                }); 
                                
                            $(elem).parent().append(multicheckbox);
                            

                            if(typeof(data) !== 'undefined')
                            {
                                 $('#categories3').val(data).select2(); 
                            }
                            else
                            {
                                $('#categories3').select2(); 
                            }


                        });
                    }

                },
                 subcategoryFilterSelect : function(elem, data) {

                    if ($(elem).is(':checked'))
                    {
                         $('#categories4').select2('destroy');  
                         $('#categories4').remove(); 
                    }
                    else
                    {
                        $.ajax({
                            type: "POST",
                            url: '/admin/tag/multi',
                            dataType: 'json'
                        }).done(function(response) {
                                var multicheckbox = $('<select multiple />').attr('id', 'categories4'); 
                                $.each(response, function(index, value){
                                    var opt = $('<optgroup />').attr('label' , index); 
                                    $.each(value, function(ind, val){
                                        opt.append($('<option />').val(val.id).text(val.name)); 
                                    }); 
                                    multicheckbox.append(opt); 
                                    
                                }); 
                                
                            $(elem).parent().append(multicheckbox);
                              if(typeof(data) !== 'undefined')
                            {
                                 $('#categories4').val(data).select2(); 
                            }
                            else
                            {
                                $('#categories4').select2(); 
                            }

                        });
                    }

                }
            },
                helpers : {
                }
        };
        $(document).ready(function() {
    manager.init();
});
