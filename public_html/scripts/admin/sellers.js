var manager = {
    init : function(){
        if (manager.widgets.init !== 'undefined')
            manager.widgets.init();  
        if (manager.events.init !== 'undefined')
            manager.events.init();  
        if (manager.handlers.init !== 'undefined')
            manager.handlers.init();  
    }, 
    widgets : {
        init : function(){
      
      
           
        }
    }, 
    events : {
        init : function(){
            
          
            $('body').on('click' , '.btn-success' , function(){
                manager.handlers.manageActivation(this);
                return false; 
            });
          
            $('body').on('click' , '#submit' , function(){
                manager.handlers.manageActivationSend();
                return false; 
            });
          
            
        }
    },
  
    handlers : {
        init : function(){
            
            
        },
        
      
        
        manageActivation : function(link){
            
            $.ajax({
                type: "GET",
                url: $(link).attr('href'),
                dataType : 'json'
            }).done(function(data) {
		
                if(data.status == 'form')
                {
                    $('#dialog').html(data.content).dialog({width: 600}); 
                }
                else alertify.error('Problem z dodaniem promocji'); 
            });  
        },
        manageActivationSend : function(){
            
            $.ajax({
                type: "POST",
                url: $('#atv').attr('action'),
                dataType : 'json',
                data : $('#atv').serialize()
            }).done(function(data) {
		
                if(data.status == 'form')
                {

                    $('#dialog').html(data.content).dialog({width: 600}); 
                    
                }
                else 
                    {
                    alertify.success('Aktywacja zakończona'); 
                   $('#dialog').dialog('destroy'); 
            }
                
            });  
            
            
        }
        
    }, 
    helpers : {
}
};

$(document).ready(function(){
    
    manager.init(); 
    
}); 
