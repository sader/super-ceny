

var manager = {
 
 focusedElement : null,
 
 init : function() {
        this.widgets.init(); 
        this.events.init();
        this.handlers.init(); 
    }, 
    widgets : {
        init : function() {
        }
    },
    events : {
        init : function() {
            
            $('.focusable').focus(function(){
                manager.focusedElement = $(this).attr('id'); 
                
            }); 
                
                            
            $('.add-tag').click(function(){
                
                
                if(manager.focusedElement === null) return false; 
                
                var name = '[' + $(this).text() + ']'; 
                
              var oldval = $('#' + manager.focusedElement).val(); 
              
              $('#' + manager.focusedElement).val(oldval + name); 
                
              
            }); 
            
            
        }
    },
    handlers : {
        init : function(){
        }
    }
};
       
$(document).ready(function(){
    manager.init(); 
});  