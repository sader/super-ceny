var manager = {

    linq : '',  
    init : function(){
        if (manager.widgets.init !== 'undefined')
            manager.widgets.init();  
        if (manager.events.init !== 'undefined')
            manager.events.init();  
        if (manager.handlers.init !== 'undefined')
            manager.handlers.init();  
    }, 
    widgets : {
        init : function(){
      
      
           
        }
    }, 
    events : {
        init : function(){
            
            $('body').on('click' , '.expand' , function(){
                
                manager.handlers.expand(this);
                return false; 
            });
            
            $('body').on('click' , '.collapse' , function(){
                manager.handlers.collapse(this);
                return false; 
            });
            
            $('body').on('click' , '.promote' , function(){
                manager.handlers.promote(this);
                return false; 
            });
            
            $('body').on('click' , '.unpromote' , function(){
                manager.handlers.unpromote(this);
                return false; 
            });
            
            $('body').on('click' , '.filter' , function(){
                manager.handlers.filterDisplay(this);
                return false; 
            });
            
            $('body').on('click' , '.param_remove' , function(){
                manager.handlers.paramRemove(this);
                return false; 
            });
            
            $('body').on('click' , '.thumb' , function(){
                manager.handlers.thumbBox(this);
                return false; 
            });

             $('body').on('click' , '.description' , function(){
                manager.linq = this; 
                 manager.handlers.descriptionBox(this);
                return false;
            });
            
            $('#dialog').on('click' , '.show_params' , function(){
                manager.handlers.paramsDisplay(this);
                return false; 
            });
            
            $('#dialog').on('click' , '#submit' , function(){
                manager.handlers.paramAdd(this);
                return false; 
            });


            $('#dialog').on('click' , '#cat_submit' , function(){

                manager.handlers.descriptionSet(this);
                return false; 
            });


            $('#dialog').on('change' , '#type' , function(){
                manager.handlers.paramTypeChange(); 
                return false; 
            });
            
        }
    },
  
    handlers : {
        init : function(){
            
            
        },

          descriptionBox : function(link)
        {

            $('#dialog').html('<div style="text-align:center"><textarea id="cat_desc" rows="10"></textarea><p><a id="cat_submit" href="' + $(link).attr('href') + '" class="btn">Ustaw</a></p></div>').dialog({
                title : 'Ustaw opis', buttons: [ { text: "Zamknij", click: function() { $( this ).dialog( "close" ); } } ]
            });


            $('#cat_desc').text($(link).attr('data'));

        },


        thumbBox : function(link)
        {
            
            $('#dialog').html('<div style="text-align:center"><div id="thumb"></div><br /><div class="btn" id="thumbUpload">Dodaj foto</div></div>').dialog({
                title : 'Ustaw obrazek', buttons: [ { text: "Zamknij", click: function() { $( this ).dialog( "close" ); } } ]
            }); 
            
           var myDropzone = new Dropzone("div#thumbUpload", { previewsContainer : '#thumb' , url: "/admin/category/thumb/level/" + $(link).attr('level') + "/id/" +  $(link).attr('data')});
            
           
                                             
            myDropzone.on("success", function(file, response) {

                        console.log(response);
                        var thumb = response;
                        
                        $('#thumb').html('<img width="100px" height="100px" src="' + thumb + '">'); 
                        $(".thumb[data='" + $(link).attr('data') + "']").attr('src' , thumb);
                        
                        
                });
                                             
        },
        
        expand : function(link) {
            $.ajax({
                type: "POST",
                url: $(link).attr('href'),
                dataType : 'json'
            }).done(function(data) {
		
                
                $(link).parent().parent().after('<tr><td colspan="5"><table style="95%" class="subcategory table table-bordered">' + data.content + '</table></td></tr>'); 
                $(link).parent().parent().addClass('selected'); 
                $(link).html('<i class="icon-minus"></i>').removeClass('expand').addClass('collapse');
            });  
        
            return false; 
        }, 
        filterDisplay : function(link) {
          
            $.ajax({
                type: "POST",
                url: $(link).attr('href'),
                dataType : 'json'
            }).done(function(response) {
		
                $('#dialog').html(response.content).dialog({
                    title : 'Lista filtrów', 
                    width : 1024, 
                    height : 400,
                    buttons: [ { text: "Zamknij", click: function() { $( this ).dialog( "close" ); } } ]
                });
              
            });  
        
            return false; 
        }, 
        
        paramRemove : function(link) {
            
            $.ajax({
                type: "POST",
                url: $(link).attr('href'),
                dataType : 'json'
            }).done(function(response) {
		
                  $(link).parent().parent().hide('slow' , function(){ $(this).remove()});
              
            });  


        }, 
    
        
       
        descriptionSet : function() {

            $.ajax({
                type: "POST",
                url: $('#cat_submit').attr('href'),
                dataType : 'json',
                data : 'description=' + $('#cat_desc').val()
            }).done(function(response) {

                $(manager.linq).attr('data' , $('#cat_desc').val());
                $('#dialog').dialog('close'); 
                
            });

            return false;
        },




        
        paramsDisplay : function(link) {
          
            $.ajax({
                type: "POST",
                url: $(link).attr('href'),
                dataType : 'json'
            }).done(function(response) {
		
                $('#show_params').html(response.content);
                manager.handlers.paramTypeChange(); 
              
            });  
        
            return false; 
        }, 
        
        collapse : function(link)
        {   
            $(link).parent().parent().next().remove(); 
            $(link).parent().parent().removeClass('selected'); 
            $(link).html('<i class="icon-plus"></i>').addClass('expand').removeClass('collapse'); 
        },
        
        promote : function(link){
            
            $.ajax({
                type: "POST",
                url: $(link).attr('href'),
                dataType : 'json'
            }).done(function(data) {
		
                if(data.status)
                {
                    $(link).replaceWith(data.link); 
                }
                else alert('Problem z dodaniem promocji'); 
                
                
            });  
            
            
        },
        unpromote : function(link){
            
            $.ajax({
                type: "POST",
                url: $(link).attr('href'),
                dataType : 'json'
            }).done(function(data) {
                if(data.status)
                {
                    $(link).parent().parent().find('.promotion').remove(); 
                    $(link).replaceWith(data.link); 
                }
                else alert('Problem z dodaniem promocji'); 
            });  

            
            
        }
        
        
        
    }, 
    helpers : {
}
};

$(document).ready(function(){
    
    manager.init(); 
    
}); 
