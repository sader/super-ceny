var manager = {
    init : function(){
        if (manager.widgets.init !== 'undefined')
            manager.widgets.init();  
        if (manager.events.init !== 'undefined')
            manager.events.init();  
        if (manager.handlers.init !== 'undefined')
            manager.handlers.init();  
    }, 
    widgets : {
        init : function(){
      
      
           
        }
    }, 
    events : {
        init : function(){
            
          
            $('body').on('click' , '.remove_error' , function(){
                manager.handlers.remove(this);
                return false; 
            });
          
            
          
            
        }
    },
  
    handlers : {
        init : function(){
            
            
        },
        
      
        
       remove : function(link){
            
            $.ajax({
                type: "POST",
                url: $(link).attr('href'),
                dataType : 'json'
            }).done(function(data) {
		
                if(data.status)
                {
                    $(link).parent().parent().remove(); 
                    alertify.success('Zrobione');
                }
                else alertify.error('Problem z dodaniem promocji'); 
                
                
            });  
            
            
        }
        
        
    }, 
    helpers : {
}
};

$(document).ready(function(){
    
    manager.init(); 
    
}); 
