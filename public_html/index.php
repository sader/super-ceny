<?php

function replace_unicode_escape_sequence($v) {
  $v = strtr($v[0], array('\\u' => ''));
    return mb_convert_encoding(pack('H*', $v), 'UTF-8', 'UTF-16BE');
            }

set_time_limit(100000);
// Define path to application directory
defined('APPLICATION_PATH')
        || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
defined('APPLICATION_ENV')
        || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
// Ensure library/ is on include_path
set_include_path(
        APPLICATION_PATH . '/../library' . PATH_SEPARATOR .
        APPLICATION_PATH . '/../library/Zend'
);

/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
                APPLICATION_ENV,
                array(
                    'pluginPaths' => array(
                        'ZendX_Application_Resource' => 'ZendX/Application/Resource'
                    ),
                    'config' => APPLICATION_PATH . '/configs/application.ini'
                )
);

$application->bootstrap()->run();


