<?php
set_time_limit(100000);
define('APPLICATION_ROOT', realpath(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..'));
define('APPLICATION_PATH', APPLICATION_ROOT . DIRECTORY_SEPARATOR . 'application');

set_include_path(
        APPLICATION_PATH . '/../library' . PATH_SEPARATOR .
        APPLICATION_PATH . '/../library/Zend'
);

define('CLI_MODE', 1);

require_once 'Zend/Loader/Autoloader.php';
Zend_Loader_Autoloader::getInstance();


$oOpts = new Zend_Console_Getopt(
                array(
                    'environment|e=s' => 'Srodowisko aplikacji - domyślnie : development',
                    'action|a=s' => 'Akcja do wykonania w formacie - module.controller.action'
                )
);
$oOpts->parse();


if (defined('APPLICATION_ENV') === false) {

    $sEnv = isset($oOpts->environment) ? $oOpts->environment : 'development';
    define('APPLICATION_ENV', $sEnv);
}

if (isset($oOpts->a)) {
    list($sModule, $sController, $sAction) = explode('.', $oOpts->a);

    $oApplication = new Zend_Application(
                    APPLICATION_ENV,
                    APPLICATION_PATH . '/configs/application.ini');
    $oApplication
            ->bootstrap();

    $oFront = Zend_Controller_Front::getInstance()
            ->setRouter(new App_Controller_Router_Console())
            ->setRequest(new Zend_Controller_Request_Simple($sAction, $sController, $sModule))
          //  ->setResponse(new Zend_Controller_Response_Cli());
    ->registerPlugin(new Zend_Controller_Plugin_ErrorHandler(array('module' => 'console')));
    try {
        $oApplication->run();
    } catch (Exception $e) {
        echo $e->getMessage();
    }
}
else
    echo $oOpts->getUsageMessage(); 