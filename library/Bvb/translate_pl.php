<?php

$translate = array(
    'Apply Filter' => 'Filtruj',
    'All' => '',
    'Clear Filters' => 'Wyczyść',
    'Clear Order' => 'Nie sortuj',
    'No records found' => 'Brak wyników'
); 
return $translate;
