<?php

// ustawienia globalne

switch (APPLICATION_ENV) {
    case 'production':
        define("DB_HOST", "localhost");
        define("DB_USER", "gen");
        define("DB_PASSWORD", "das2sfasf");
        define("DB_NAME", "gen");

        break;

    default:
        define("DB_HOST", "localhost");
        define("DB_USER", "root");
        define("DB_PASSWORD", "");
        define("DB_NAME", "wakacje");
        break;
}


///
define("USE_Multibyte_String", "1"); // może być 0 lub 1

define("SQL_1", "select S1, S2,S3 from tabela where S1 = '%s' ");

define("SQL_2", "select 
          S2.SLOWO
        from 
		  synonimy S1, synonimy S2
        WHERE
          S1.slowo = '%s'
        AND
           S1.LINIA = S2.LINIA ");

define("SQL_3", "select S1,S2,S3 from tabela where S2 in ( %s ) ");
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//ł ączenie i wybranie bazy
$link_db = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD) or die('Nie można się płączyć' . mysql_error());
mysql_select_db(DB_NAME, $link_db) or die('Nie mozna wybrac bazy danych');
mysql_query("SET CHARACTER SET utf8", $link_db);

// koniec laczenia sie z baza 
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///alternatywna funkcja UPPER dla unocode lepiej strosować wersję MB
///////////////////////////////////////////////////////////////////////////////
function s4y_upper($str) {
    if (USE_Multibyte_String == "1")
        return mb_strtoupper($str, "utf-8");
    else
        return strtoupper($str);
}

///////////////////////////////////////////////////////////////////////////////
///alternatywna funkcja LOWER dla unocode lepiej strosowaćwersję MB
///////////////////////////////////////////////////////////////////////////////
function s4y_lower($str) {
    if (USE_Multibyte_String == "1")
        return mb_strtolower($str, "utf-8");
    else
        return strtolower($str);
}

///////////////////////////////////////////////////////////////////////////////
///alternatywna funkcja do zmiany kodowania znaków
///////////////////////////////////////////////////////////////////////////////
function s4y_convert_encoding($str, $encoding_out, $encoding_in) {
    if (USE_Multibyte_String == "1")
        return mb_convert_encoding($str, $encoding_out, $encoding_in);
    else
        return $str;
}

///////////////////////////////////////////////////////////////////////////////
/// UstawWielkoscLiter w zależności od parametrów wejsciowych
/// @param p_syn 
/// @param p_slowo
///////////////////////////////////////////////////////////////////////////////
function UstawWielkoscLiter($p_syn, $p_slowo) {
    $result = $p_syn;

    if ($p_slowo == s4y_upper($p_slowo)) {
        $result = s4y_upper($p_syn);
    }
    if ($p_slowo == s4y_lower($p_slowo)) {
        $result = s4y_lower($p_syn);
    }
    if ((strlen($p_syn) > 0) AND (strlen($p_slowo) > 0)) {
        if ($p_slowo[0] == s4y_upper($p_slowo[0])) {
            $s = s4y_upper($p_syn);
            $result[0] = $s[0];
        }
    }
    return $result;
}

///////////////////////////////////////////////////////////////////////////////
/// JestSynonimSlowa
/// @return TRUE/FALSE w przypadku gdy znaleziono synonimy
/// @param p_slowo - słowo dla którego szukamy synonimów
/// @param p_synonim - synonim zwracany przez funkcję gdy wynik jest TRUE
///////////////////////////////////////////////////////////////////////////////
function JestSynonimSlowa($p_slowo, &$p_synonim) {
    global $link_db;

    $p_synonim = "";

    // weryfikacja parametrów wejsciowych
    // brak słowa RETURN
    if ($p_slowo == "")
        return false;

    // pierwszy poziom  I  [ szukam słowa w słowniku]
    $query = sprintf(SQL_1, mysql_escape_string($p_slowo));
    // echo $query;
    $result = mysql_query($query) or die('Zapytanie zakończone niepowodzeniem: ' . mysql_error());
    $num_rows = mysql_num_rows($result);

    // jeżeli nie ma danych to słowo nie istnbieje w słowniku RETURN
    if ($num_rows < 1)
        return false;

    $line = mysql_fetch_array($result, MYSQL_ASSOC);
    //print_r($line);

    $S3 = $line['S3'];
    $S2 = $line['S2'];

    mysql_free_result($result);

    // drugi poziom  II  [ szukam synonimów]
    $query = sprintf(SQL_2, $S2);
    $result = mysql_query($query) or die('Zapytanie zakończone niepowodzeniem: ' . mysql_error());
    $num_rows = mysql_num_rows($result);

    // jeżeli nie ma danych to słowo nie istnbieje w słowniku synonimów RETURN
    if ($num_rows < 1)
        return false;

    //buduje liste słów
    $slowa = "";
    while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
        $slowa .= ",'" . $line['SLOWO'] . "'";
    }
    //usuwam pierwszy przecinek 
    if ($slowa[0] == ",")
        $slowa[0] = ' ';


    mysql_free_result($result);

    // trzeci poziom I II  [ szukam form synonimów]
    $query = sprintf(SQL_3, $slowa);
    //echo "<br>".$query."<br>";

    $result = mysql_query($query) or die('Zapytanie zakończone niepowodzeniem: ' . mysql_error());
    $num_rows = mysql_num_rows($result);

    // jeżeli nie ma danych to słowo nie istnbieje w słowniku synonimów RETURN
    if ($num_rows < 1)
        return false;

    $wyniki = array();
    while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
        $wyniki[] = $line;
    }
    //sprawdzam czy cos sie znalazło
    if (count($wyniki) == 0)
        return false;

    $wynik2 = array();
    foreach ($wyniki as $wiersz) {
        //echo $S3."=".$wiersz['S3'] ."\n" ;
        //print_r($wiersz);
        if ($wiersz['S3'] == $S3) {
            $wynik2[] = $wiersz;
        }
    }
    if (count($wynik2) == 0)
        return false;

    $losuj = rand(0, count($wynik2) - 1);

    $p_synonim = $wynik2[$losuj]['S1'];
    $p_synonim = UstawWielkoscLiter($p_synonim, $p_slowo);
    return true;
}

;

///////////////////////////////////////////////////////////////////////////////
/// DajSynonimSlowa
/// @return Zwraca synonim dla słowa
/// @param p_slowo - słowo dla którego szukamy synonimów
///////////////////////////////////////////////////////////////////////////////
function DajSynonimSlowa($p_slowo) {
    if (JestSynonimSlowa($p_slowo, $synonim) == true)
        return $synonim;
    else
        return $p_slowo;
}

///////////////////////////////////////////////////////////////////////////////
/// DajSynonimyZdania
/// @return Zwraca synonim dla zdania
/// @param s - zdanie dla którego szukamy synonimów
/// @param fu_name -nazwa funkcji wykorzystwanej
///////////////////////////////////////////////////////////////////////////////
function DajSynonimyZdania($s, $fu_name = "DajSynonimSlowa") {
    $znaki_przestankowe = array(" ", ",", ".", "?", "(", ")", "[", "]", "/", "!", "$", "%", "^", "&");
    $l = strlen($s);
    $pos = 0;
    $jest_slowo = FALSE;
    $start = 0;
    $result = "";
    while ($pos < $l) {
        $zn = $s[$pos];
        //echo $pos ." = = ".$zn."\n";
        if (in_array($zn, $znaki_przestankowe)) {
            if ($jest_slowo == true) {
                $str = substr($s, $start, $pos - $start);
                if (function_exists($fu_name)) {
                    $str = $fu_name($str);
                }
                $jest_slowo = false;
                $result .= $str;
            }
            $result .= $zn;
        } else {
            if ($jest_slowo == false) {
                $jest_slowo = TRUE;
                $start = $pos;
            }
        }
        $pos++;
    }
    if ($jest_slowo == true) {
        $str = substr($s, $start);
        if (function_exists($fu_name)) {
            $str = $fu_name($str);
        }
        $result .= $str;
    }
    return $result;
}

///////////////////////////////////////////////////////////////////////////////
/// Synonimek
/// @return Zwraca tekst 
/// @param input - tekst wejsciowy (moze byc z HTML)
/// @param encoding tekstu
///////////////////////////////////////////////////////////////////////////////
function synonimek($input, $encoding_IN="UTF-8", $encoding_OUT="UTF-8") {
    $content = "";
    $end = 1;
    while ($end > 0) {
        $start = strpos($input, "<");
        if ((strpos($input, "!--") - 1) == $start) {
            $end = strpos($input, "-->");
            if ($end > 0) {
                $comment = substr($input, $start + 4, $end - $start - 4);
                $content .= "<!--" . $comment . "-->";
                $input = substr($input, $end + 3);
            }
        } elseif ((strpos($input, "!") - 1) == $start) {
            $end = strpos($input, ">");
            if ($end > 0) {
                $doctype = substr($input, $start + 2, $end - $start - 2);
                $content .= "<!" . $doctype . ">";
                $input = substr($input, $end + 1);
            }
        } else {
            $end = strpos($input, ">");
            if ($end > 0) {
                $tag = trim(substr($input, $start + 1, $end - $start - 1));
                $s1 = substr($input, 0, $start);
                //
                if ($encoding_IN != "UTF-8")
                    $s1 = s4y_convert_encoding($s1, "UTF-8", "latin2");

                $s1 = DajSynonimyZdania($s1);

                if ($encoding_OUT != "UTF-8")
                    $s1 = s4y_convert_encoding($s1, "latin2", "UTF-8");

                $content .= $s1;
                $content .= "<" . $tag . ">";
                $input = substr($input, $end + 1);
            }
        }
    }
    $content .= $input;
    return $content;
}