<?php

class App_Controller_Admin_Abstract extends App_Controller_Abstract {

    private $sLoginEntry = 'admin:auth:login';

    public function init() {

        parent::init();
        
                $oTranslate = new Zend_Translate(array(
                    'adapter' => 'array',
                    'content' => APPLICATION_PATH . 
                                 DIRECTORY_SEPARATOR . '..' . 
                                 DIRECTORY_SEPARATOR . 'library' .
                                 DIRECTORY_SEPARATOR . 'Bvb' .
                                 DIRECTORY_SEPARATOR . 'translate_pl.php',
                    'language' => 'PL'));

        Zend_Registry::set('Zend_Translate', $oTranslate);

        App_Addons::getInstance()->add('jQuery', 'jQueryUi', 'TwitterBootstrap', 'Alertify.js' , 'Select2');
        $this->_helper->_layout->setLayout('admin');
    }

    public function preDispatch() {

        
        
        if (Zend_Auth::getInstance()->hasIdentity())
        {
            return true;
        }
        
        $sRoute = implode(":", array($this->getRequest()->getModuleName(),
            $this->getRequest()->getControllerName(),
            $this->getRequest()->getActionName()));

        if ($sRoute == $this->sLoginEntry)
            return;

        $this->_redirect($this->view->url(array('module' => 'admin', 'controller' => 'auth', 'action' => 'login')));
    }

}