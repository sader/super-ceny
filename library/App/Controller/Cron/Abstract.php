<?php

class App_Controller_Cron_Abstract extends App_Controller_Abstract {

    
    
    public function init() {
        parent::init();

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
    }
    
    public function preDispatch() {
        parent::preDispatch();
    
  
        
     $iAcceptedLifeTime = 3600;    
     $sFilePath = APPLICATION_PATH . DIRECTORY_SEPARATOR .  'data'  . DIRECTORY_SEPARATOR . 'cronjobs' . DIRECTORY_SEPARATOR; 
     $sActionName =   $this->getRequest()->getControllerName().'_'.$this->getRequest()->getActionName(); 
     
       
      if(file_exists($sFilePath.$sActionName))
      {
          
          
          $sFileTime = filemtime($sFilePath. $sActionName);
   
          $iTime = time()  - $sFileTime; 
          
                    
          if($iTime < $iAcceptedLifeTime)
          {
              die('Error : Akcja zablokowana. Nastepny cykl najpozniej za '.ceil(($iAcceptedLifeTime - $iTime)/60).' minut'); 
          }
          else 
          {
              unlink($sFilePath . $sActionName); 
          }
      }
      else
      {
          file_put_contents($sFilePath.$sActionName, 1); 
      }
      
        
    }
    
    
    public function postDispatch() {
        parent::postDispatch();
        
         $sFilePath = APPLICATION_PATH . DIRECTORY_SEPARATOR .  'data'  . DIRECTORY_SEPARATOR . 'cronjobs' . DIRECTORY_SEPARATOR; 
        $sActionName =   $this->getRequest()->getControllerName().'_'.$this->getRequest()->getActionName(); 
     
         
         unlink($sFilePath . $sActionName); 
         
    }

}

