<?php

class App_Controller_Page_Abstract extends App_Controller_Abstract
{
       public function init() {

        parent::init();


        App_Addons::getInstance()->add('jQuery' , 'jQueryUi',  'Alertify.js', 'fancybox', 'inputMask', 'raty');
        Model_Logic_Category::getInstance()->manageCategoryGrid($this);
        $this->view->categoriesOne = Model_DbTable_CategoryOne::getInstance()->getList()->query()->fetchAll(Zend_Db::FETCH_ASSOC);
      
    }

    
}

