<?php

class App_Auth {
    
    
    /**
     *
     * @param string $sName
     * @return Zend_Auth
     */
    public static function get($sName)
    {
          return Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session($sName)); 
    }
    
    
    
}

