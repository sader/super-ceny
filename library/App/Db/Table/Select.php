<?php

/**
 * App_Db_Table_Select
 * Nakładka na Zend_Db_Table_Select dodająca cache
 *
 * @author Krzysztof Janicki
 */
class App_Db_Table_Select extends Zend_Db_Table_Select
{
	
	/**
	 * Klucz pod którym znajduje się defaultowy obiekt Cache w Zend_Registry 
	 */
	const CACHE_REGISTRY_KEY = 'db_cache';
	
	/**
	 * Nazwa cache w Resource'ach 
	 */
	const CACHE_RESOURCE_KEY = 'default';
	


	/**
	 * Znacznik włączenie cache
	 * @var boolean 
	 */
	private $__bCache = false;
	
	/**
	 * Obiekt Cache
	 * @var Zend_Cache_Core 
	 */
	private $__oCache = null;
	
	/**
	 * Typ fetch
	 * @var integer 
	 */
	private $__fetchMode = null; 
	
	/**
	 * Tablica zmiennych dla metody bind
	 * @var array 
	 */
	private $__bind = null; 
	
	/**
	 * Metoda ustawia cache
	 * @param Zend_Cache $oCache
	 * @return App_Db_Table_Select 
	 */
	public function setCache(Zend_Cache_Core $oCache)
	{

		$this->__oCache = $oCache;
		return $this;
	}

	/**
	 * Metoda zwaraca obiekt cache
	 * @return Zend_Cache 
	 */
	public function getCache()
	{
		return $this->__oCache;
	}

	/**
	 * Metoda ustawia domyślny cache. Sprawdza Zend_Registry oraz Resource'y wg defaultowych kluczy
	 * @return boolean 
	 */
	private function __setDefaultCache()
	{
		if (Zend_Registry::isRegistered(self::CACHE_REGISTRY_KEY)) {
			$this->setCache(Zend_Registry::get(self::CACHE_REGISTRY_KEY));
			return true;
		}
		if (Zend_Controller_Front::getInstance()->getParam('bootstrap')->hasPluginResource('cachemanager')) {
			$oCacheManager = Zend_Controller_Front::getInstance()->getParam('bootstrap')
					->getPluginResource('cachemanager')
					->getCacheManager();
			if ($oCacheManager->hasCache(self::CACHE_RESOURCE_KEY)) {
				$this->setCache($oCacheManager->getCache(self::CACHE_RESOURCE_KEY));
				return true;
			}
		}
		return false;
	}

	/**
	 * Metoda ustawia cache na włączony / wyłączony. Opcjonalnie przyjmuje obiekt cache 
	 * Przy braku obiektu cache i próbie włączenia wywoływana jest próba ustawienia defaultowego cache
	 * @param boolean $bStatus
	 * @param Zend_Cache $oCache
	 * @return App_Db_Table_Select
	 */
	public function setCacheEnable($bStatus = true, $oCache = null)
	{
		$this->__bCache = (boolean) $bStatus;

			if ($bStatus && $this->__oCache === null) {
				if (!$this->__setDefaultCache())
					throw new Exception('No cache specified');
			}

		return $this;
	}
	
	/**
	 * Haszowanie zapytania wraz z parametrami
	 * @return string[32] 
	 */
	private function __hashQuery()
	{
		
		return md5($this->__toString() . implode(func_get_args())); 
	}

	
	/**
	 * Przesłonięcie metody query która przy włączonym cache emuluje sobą metody fetch adaptera
	 * @param integer $fetchMode
	 * @param array $bind
	 * @return \App_Db_Table_Select 
	 */
	public function query($fetchMode = null, $bind = array())
	{
		
		if(!$this->__bCache) return parent::query ($fetchMode, $bind); 
		else
		{
			$this->__fetchMode = $fetchMode; 
			$this->__bind = $bind; 
			return $this;
		}
	}
	
	/**
	 * Wrapper dla fetch adaptera dla cache
	 * @param integer $iMode
	 * @return array 
	 */
	public function fetch($iMode = null)
	{
		$sKey = $this->__hashQuery('fetch' , $iMode); 
		
		if(($aData = $this->__oCache->load($sKey)) === false)
		{
			$aData = parent::query($this->__fetchMode, $this->__bind)->fetch($iMode); 
        }
		$this->__oCache->save($aData, $sKey, $this->__getTablesAsTags()); 
		return $aData;
	}
	

	/**
	 * Wrapper dla fetchAll adaptera dla cache
	 * @param integer $iMode
	 * @return array 
	 */
	public function fetchAll($iMode = null)
	{
		$sKey = $this->__hashQuery('fetchAll' , $iMode); 
		
		if(($aData = $this->__oCache->load($sKey)) === false)
		{
			$aData = parent::query($this->__fetchMode, $this->__bind)->fetchAll($iMode); 
        }
		$this->__oCache->save($aData, $sKey, $this->__getTablesAsTags()); 
		return $aData;
	}
	
	/**
	 * Wrapperd dla fetchObj adaptera dla cache
	 * @param integer $iMode
	 * @return array 
	 */
		public function fetchObj()
	{
		$sKey = $this->__hashQuery('fetchObj'); 
		
		if(($aData = $this->__oCache->load($sKey)) === false)
		{
			$aData = parent::query($this->__fetchMode, $this->__bind)->fetchObj(); 
        }
		$this->__oCache->save($aData, $sKey, $this->__getTablesAsTags()); 
		
		return $aData;
	}
	
	
	private function __getTablesAsTags()
	{
		$aJoined = array(); 
		
		if(!empty ($this->_parts['from']))
		{
			foreach($this->_parts['from'] as $aFrom)
			{
				
				if($aFrom['tableName'] instanceof Zend_Db_Select)
				{
					foreach( $aFrom['tableName']->_parts['from'] as $aFromSubselect)
					{
							$aJoined[] = $aFromSubselect['tableName']; 
					}
				}
				else $aJoined[] = $aFrom['tableName']; 
			}
		}
		
		return array_unique(array_merge($aJoined , array($this->_table->getDbName()))); 
		
	}
}
