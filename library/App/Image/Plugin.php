<?php

class App_Image_Plugin extends Zend_Controller_Plugin_Abstract {

    public function routeStartup(Zend_Controller_Request_Abstract $request) {

        if (defined('CLI_MODE'))
        {
            return false;
        }

        $oRouter = Zend_Controller_Front::getInstance()->getRouter();

        $oRoute = new Zend_Controller_Router_Route_Regex('(.*)-(\d+)x(\d+)\.(jpg|png|jpeg|bmp)', array('module' => 'default',
            'controller' => 'index',
            'action' => 'index'), array(1 => 'file',
            2 => 'width',
            3 => 'height',
            4 => 'ext'));

        $oRouter->addRoute(App_Image::IMAGE_ROUTE, $oRoute);
    }

    public function routeShutdown(Zend_Controller_Request_Abstract $request) {

           if (defined('CLI_MODE'))
        {
            return false;
        }

        $sDispatchedRoute = Zend_Controller_Front::getInstance()->getRouter()->getCurrentRouteName();
        if ($sDispatchedRoute == App_Image::IMAGE_ROUTE) {
            App_Image::display($request);
        }
    }

}