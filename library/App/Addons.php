<?php

class App_Addons {

    private $_addons = array();

    private $_publicDir =  '/addons/';
    private $_publicCacheDir =  '/addons/cached/';

    /**
     * Instancja klasy.
     * @var App_Addons
     */
    static private $_oInstance;

    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * @return App_Addons
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function add() {
        $aCalledParams = func_get_args();
        
        $this->_addons = array_unique(array_merge($this->_addons, $aCalledParams));
    }

    public function deploy() {
        
        $sBase =   APPLICATION_PATH . 
                              DIRECTORY_SEPARATOR . 
                              '..' . 
                              DIRECTORY_SEPARATOR . 
                              'public_html' . 
                              DIRECTORY_SEPARATOR;
     
        
        if (!empty($this->_addons)) {
            
            $sJsDeployed =  $this->_publicCacheDir . md5(implode('', $this->_addons)) . '.js'; 
            $sCssDeployed = $this->_publicCacheDir. md5(implode('', $this->_addons)) . '.css'; 
           
            $aFileBody = array();
            $sManifest = $sBase . $this->_publicDir . '%s' . DIRECTORY_SEPARATOR . '%s';

            foreach ($this->_addons as $sAddon) {
                    $sM = sprintf($sManifest, $sAddon, 'manifest'); 
                if (file_exists($sM)) {
                    $aManifestData = file($sM);

                    foreach ($aManifestData as $sRow) {
                        list($sType, $sFile) = explode(':', trim($sRow));

                        if (file_exists(sprintf($sManifest, $sAddon, $sFile))) {
                            $aFileBody[$sType][] = file_get_contents(sprintf($sManifest, $sAddon, $sFile));
                        }
                    }
                    
                }
            }
            $sReturn = null; 
            if(!empty($aFileBody['js']))
            {
            file_put_contents($sBase . $sJsDeployed, implode('', $aFileBody['js'])); 
            $sReturn  =  sprintf ('<script src="%s"></script>'.PHP_EOL, $sJsDeployed); 

            }
          if(!empty($aFileBody['css']))
            {
            file_put_contents($sBase . $sCssDeployed, implode('', $aFileBody['css']));  
            $sReturn .=  sprintf (' <link rel="stylesheet" href="%s">'.PHP_EOL, $sCssDeployed); 

            }
            
                return $sReturn; 
        }
    }

}