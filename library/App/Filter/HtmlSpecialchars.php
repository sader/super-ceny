<?php

require_once 'Zend/Filter/Interface.php';

/**
 * @category   Zend
 * @package    Zend_Filter
 * @copyright  Copyright (c) 2005-2008 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class App_Filter_HtmlSpecialchars implements Zend_Filter_Interface
{
    /**
     * Corresponds to second htmlspecialchars() argument
     *
     * @var integer
     */
    protected $_quoteStyle;

    /**
     * Corresponds to third htmlspecialchars() argument
     *
     * @var string
     */
    protected $_charSet;

    /**
     * Corresponds to fourth htmlspecialchars() argument
     *
     * @var string
     */
    protected $_doubleEncode;

    /**
     * Sets filter options
     *
     * @param  integer $quoteStyle
     * @param  string  $charSet
     * @return void
     */
    public function __construct($quoteStyle = ENT_COMPAT, $charSet = 'UTF-8', $doubleEncode = true)
    {
        $this->_quoteStyle = $quoteStyle;
        $this->_charSet    = $charSet;
        $this->_doubleEncode = $doubleEncode;
    }

    /**
     * Returns the quoteStyle option
     *
     * @return integer
     */
    public function getQuoteStyle()
    {
        return $this->_quoteStyle;
    }

    /**
     * Sets the quoteStyle option
     *
     * @param  integer $quoteStyle
     * @return Zend_Filter_HtmlEntities Provides a fluent interface
     */
    public function setQuoteStyle($quoteStyle)
    {
        $this->_quoteStyle = $quoteStyle;
    }

    /**
     * Returns the charSet option
     *
     * @return string
     */
    public function getCharSet()
    {
        return $this->_charSet;
    }

    /**
     * Sets the charSet option
     *
     * @param  string $charSet
     * @return Zend_Filter_HtmlEntities Provides a fluent interface
     */
    public function setCharSet($charSet)
    {
        $this->_charSet = $charSet;
        return $this;
    }

    /**
     * Defined by Zend_Filter_Interface
     *
     * Returns the string $value, converting characters to their corresponding HTML entity
     * equivalents where they exist
     *
     * @param  string $value
     * @return string
     */
    public function filter($value)
    {
        return htmlspecialchars((string) $value, $this->_quoteStyle, $this->_charSet, $this->_doubleEncode);
    }
}