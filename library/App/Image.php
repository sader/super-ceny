<?php

class App_Image {

    const IMAGE_ROUTE = 'imagemanager';
    const IMAGE_PATH = '/images/batch';
    const IMAGE_FILE_PATH = '../public_html/images/batch';
    const DEFAULT_IMAGE_EXTENSION = 'png';
    const DEFAULT_IMAGE = '';

    public static function saveImageFromWeb($sImage) {
        $oClient = new Zend_Http_Client(null, array('timeout' => 30));
        $oResponse = $oClient->setUri($sImage)
                ->request(Zend_Http_Client::GET);
        if ($oResponse->isSuccessful()) {
            $sExt = strtolower(substr($sImage, 0, -3));

            if (!in_array($sExt, array('jpg', 'png', 'jpeg', 'bmp'))) {
                $sExt = self::DEFAULT_IMAGE_EXTENSION;
            }

            require_once 'App/Image/WideImage/WideImage.php';
            $aUri = self::createUri();
            WideImage::loadFromString($oResponse->getBody())->saveToFile($aUri['path'] . '.' . $sExt);
            return $aUri['file'] . '.' . $sExt;
        }
    }

    public function saveImageFromUpload($sUploadName) {

        
        if (!empty($_FILES) && $_FILES[$sUploadName]['error'] == 0) {
            require_once 'App/Image/WideImage/WideImage.php';
            $aUri = self::createUri();

            $sExt = strtolower(substr($_FILES[$sUploadName]['name'], 0, -3));

            if (!in_array($sExt, array('jpg', 'png', 'jpeg', 'bmp'))) {
                $sExt = self::DEFAULT_IMAGE_EXTENSION;
            }

            WideImage::loadFromUpload($sUploadName)->saveToFile($aUri['path'] . '.' . $sExt);
            return $aUri['file'] . '.' . $sExt;
        }
        else return false; 
    }

    private static function createUri() {
        
        $sName = str_replace('.', '', (string)(microtime(true))); 
        $sPath = APPLICATION_PATH .
                DIRECTORY_SEPARATOR .
                self::IMAGE_FILE_PATH .
                DIRECTORY_SEPARATOR .
                substr($sName, 3, 3) .
                DIRECTORY_SEPARATOR .
                substr($sName, 6, 3);


        if (!is_dir($sPath)) {
            mkdir($sPath, 0775, true);
        }

        return array('file' => $sName, 'path' => $sPath . DIRECTORY_SEPARATOR . $sName);
    }

    public static function uri($sName, $sSize = null) {
        list($sFile, $sExt) = explode('.', $sName);

        $sPath = self::IMAGE_PATH .
                '/' .
                substr($sName, 3, 3) .
                '/' .
                substr($sName, 6, 3) .
                '/' .
                $sFile;

        if ($sSize !== null && preg_match('/\d{1,4}x\d{1,4}/', $sSize)) {
            $sPath .= '-' . $sSize;
        }
        $sPath .= '.' . $sExt;
        return $sPath;
    }

    public static function display($oRequest) {
        //$this->_helper->layout()->disableLayout();
        //$this->_helper->viewRenderer->setNoRender(true);

        $sName = $oRequest->getParam('file');
        $sWidth = $oRequest->getParam('width');
        $sHeight = $oRequest->getParam('width');
        $sExtension = $oRequest->getParam('ext');


        $sUrl = $_SERVER['REQUEST_URI'];

        $sPath = APPLICATION_PATH .
                DIRECTORY_SEPARATOR .
                '..' .
                DIRECTORY_SEPARATOR .
                'public_html';


        $sRequestedRealFile = $sPath . DIRECTORY_SEPARATOR . $sName . '.' . $sExtension;


        if (file_exists($sRequestedRealFile)) {


            $sWorkFile = $sRequestedRealFile;
        } elseif (self::DEFAULT_IMAGE !== '') {

            $sWorkFile = APPLICATION_PATH .
                    DIRECTORY_SEPARATOR .
                    self::DEFAULT_IMAGE;
        } else {
            die();
        }

        require_once 'App/Image/WideImage/WideImage.php';

        $oFile = WideImage::loadFromFile($sWorkFile)->resizeDown($sWidth, $sHeight);

        $oSaved = clone($oFile);
        $oSaved->saveToFile($sPath . $sUrl);
        $oFile->output($sExtension);
        die();
    }

}