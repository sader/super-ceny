<?php

class App_Validate_Db_NoRecordExists extends App_Validate_Db_Abstract
{
    public function isValid($value, $aContext = null)
    {
        $valid = true;
        $this->_setValue($value);
        $this->parseOptFields($aContext); 


        $result = $this->_query($value);
        if ($result) {
            $valid = false;
            $this->_error(self::ERROR_RECORD_FOUND);
        }

        return $valid;
    }
}
