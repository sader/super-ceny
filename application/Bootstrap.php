<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

    protected function _initLoader(){
    
   $oModuleLoader = new Zend_Application_Module_Autoloader(
                        array('namespace' => '',
                              'basePath' => APPLICATION_PATH)
        );
    } 
   
    protected function _initRouter(){
        
             
            $sRouteFilepath = APPLICATION_PATH . 
                          DIRECTORY_SEPARATOR. 
                          'configs'.
                          DIRECTORY_SEPARATOR.
                          'routes.ini'; 
         
                $oRoute = new Zend_Config_Ini($sRouteFilepath);
                Zend_Controller_Front::getInstance()
                                       ->getRouter()
                                       ->addConfig($oRoute, 'routes');
        
    }
    
    
    protected function _initModuleAutoloaders() 
    { 
        $this->bootstrap('FrontController'); 
        $front = $this->getResource('FrontController'); 

        foreach ($front->getControllerDirectory() as $module => $directory) { 
            $module = ucfirst($module); 
            $loader = new Zend_Application_Module_Autoloader(array( 
                'namespace' => $module, 
                'basePath'  => dirname($directory), 
            )); 
                  
             $loader->addResourceType('grid', 'grids' , 'Grid');
        } 
    }

    public function _initLuceneDefaults()
    {
          Zend_Search_Lucene_Analysis_Analyzer::setDefault(
                new Zend_Search_Lucene_Analysis_Analyzer_Common_Utf8Num_CaseInsensitive());

        Zend_Search_Lucene_Search_QueryParser::setDefaultEncoding('UTF-8');

    }
    
   
    
    
}

