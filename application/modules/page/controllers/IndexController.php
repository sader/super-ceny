<?php

class Page_IndexController extends App_Controller_Page_Abstract {

    public function indexAction() {


        Model_Logic_Product::getInstance()->manageMainPagePromoted($this);
        Model_Logic_Box::getInstance()->mainPage($this);
        Model_Logic_Meta::getInstance()->index($this);
    }

    public function categoryAction() {
        Model_Logic_Category::getInstance()->manageCategoryList($this);
        Model_Logic_Product::getInstance()->manageCategoryPromoted($this);
        Model_Logic_Box::getInstance()->category($this);
        Model_Logic_Meta::getInstance()->category($this);
    }

    public function groupAction() {
        Model_Logic_Product::getInstance()->manageGroup($this);
        Model_Logic_Box::getInstance()->group($this);
        Model_Logic_Meta::getInstance()->group($this);
    }

    public function subcategoryAction() {
        Model_Logic_Product::getInstance()->manageSubcategory($this);
        Model_Logic_Box::getInstance()->subcategory($this);
        $this->getRequest()->getParams();
        if ($this->tagged()) {
            if ($this->filtered()) {
                Model_Logic_Tag::getInstance()->subcategoryFilter($this);
            } else {
                Model_Logic_Tag::getInstance()->subcategory($this);
            }
        } else {
               
            if ($this->filtered()) {
                Model_Logic_Meta::getInstance()->subcategoryFilters($this);
              
            } else {
  
                Model_Logic_Meta::getInstance()->subcategory($this);
            }
        }
    }

    public function subcategoryextraAction() {
        Model_Logic_Product::getInstance()->manageSubcategoryExtra($this);
        Model_Logic_Box::getInstance()->subcategory($this);
        Model_Logic_Meta::getInstance()->subcategoryExtra($this);
    }

    public function productAction() {
        Model_Logic_Product::getInstance()->manageDetails($this);
        //  Model_Logic_Product::getInstance()->manageSimiliars($this);
        Model_Logic_Box::getInstance()->product($this);

        if ($this->tagged()) {

            Model_Logic_Tag::getInstance()->product($this);
        } else {
            Model_Logic_Meta::getInstance()->detail($this);
        }
    }

    public function shopsAction() {
        $this->_helper->json(Model_Logic_Product::getInstance()->managetShopsForProduct($this));
    }

    public function shopAction() {
        $this->_helper->json(Model_Logic_Skapiec_Api::getInstance()->manageShops($this));
    }

    public function iframeAction() {
        $this->_helper->json(Model_Logic_Nokaut_Api::getInstance()->manageProductUrl($this));
    }

    public function searchAction() {
        Model_Logic_Search::getInstance()->manageSearch($this);
        Model_Logic_Box::getInstance()->search($this);

        if ($this->tagged()) {
            Model_Logic_Tag::getInstance()->search($this);
        } else {
            Model_Logic_Meta::getInstance()->search($this);
        }
    }

    public function urlhelperAction() {
        $sType = $this->getRequest()->getParam('type');



        $aParams = $this->getRequest()->getParam('data');
        $aParams['filters']['pf'] = $this->getRequest()->getParam('pf');
        $aParams['filters']['pt'] = $this->getRequest()->getParam('pt');
        switch ($sType) {
            case 'category':
                $this->_helper->json(array('url' => $this->view->subcategoryUrl($aParams)));
                break;
            case 'group':
                $this->_helper->json(array('url' => $this->view->groupUrl($aParams)));
                break;
            case 'search':
                $this->_helper->json(array('url' => $this->view->searchUrl($aParams)));
                break;
            default:
                $this->_helper->json(array('url' => $this->view->subcategoryUrl($aParams)));
                break;
        }
    }

    public function redirectidAction() {
        Model_Logic_Product::getInstance()->manageRedirectId($this);
        Model_Logic_Meta::getInstance()->randomId($this);
    }

    public function redirectnameAction() {
        Model_Logic_Product::getInstance()->manageRedirectName($this);
        Model_Logic_Meta::getInstance()->randomName($this);
    }

    private function tagged() {
        $sTag = $this->getRequest()->getParam('tag');

        return !empty($sTag);
    }

    private function filtered() {

        $sFilter = $this->getRequest()->getParam('filters');
        return !empty($sFilter);
    }

}
