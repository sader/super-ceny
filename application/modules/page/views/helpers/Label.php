<?php

class Zend_View_Helper_Label {

    public function label($sString) {
        return empty($sString) ? '<span class="label label-important">Nie</span>' 
                               : '<span class="label label-success">Tak</span>'; 
    }

}
