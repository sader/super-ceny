<?php

class Zend_View_Helper_SubcategoryUrl extends Zend_View_Helper_Abstract {

    public $defaultSort = 'c';

    public function subcategoryUrl($aParams = array(), $aAppend = null, $sReduce = null) {

        $sReturn = '/3/';


        $sReturn .= $aParams['url'];

        if (isset($aAppend['producent']) && !empty($aAppend['producent'])) {
            $aParams['producent'] = $aAppend['producent'];
        }

        if (!empty($aParams['producent']) && $sReduce != 'producent') {
            $sReturn .= '/' . $this->view->seo($aParams['producent']);
        }


        if (isset($aAppend['city']) && !empty($aAppend['city'])) {

            $aParams['city'] = $aAppend['city'];
        }

        if (!empty($aParams['city']) && $sReduce != 'city') {
            $sReturn .= '/m/' . $this->view->seo($aParams['city']);
        }


        if (isset($aAppend['params']) && !empty($aAppend['params'])) {

            foreach ($aAppend['params'] as $iAKey => $aApp) {
                $aParams['params'][$iAKey] = $aApp;
            }
        }


        if (!empty($aParams['params']) && is_array($aParams['params'])) {


            if (isset($sReduce['params'])) {
                unset($aParams['params'][$sReduce['params']]);
            }

            $aX = array();


            if (count($aParams['params']) > 0) {
                foreach ($aParams['params'] as $iKey => $iParam) {
                    $aX[] = trim($iKey) . '-' . trim($iParam);
                }
                $sReturn .= '/f/' . implode(',', $aX);
            }
        }


        if(isset($aParams['price']) && ($aParams['price']['from'] !== null || $aParams['price']['to'] !== null) && $sReduce != 'price')
        {

           $sReturn .= '/p/';
           if($aParams['price']['from'] !== null)
           {
               $sReturn .= $aParams['price']['from']; 
           }


            if($aParams['price']['to'] !== null)
           {
               $sReturn .= '-'.$aParams['price']['to'];
           }

        }

        if (isset($aAppend['sort']) && !empty($aAppend['sort'])) {

            $aParams['sort'] = $aAppend['sort'];
        }

        if (!empty($aParams['sort']) && $sReduce != 'sort') {


            if(empty($aParams['page']) && !isset($aAppend['page']))
            {
                $aAppend['page'] = 1; 
            }

            $sReturn .= '/' . $aParams['sort'];
        }

        if (isset($aAppend['page']) && !empty($aAppend['page'])) {

            

            $aParams['page'] = $aAppend['page'];
        }

        if (!empty($aParams['page']) && $sReduce != 'page') {


            if (empty($aParams['sort'])) {
                $sReturn .= '/' . $this->defaultSort;
            }

            $sReturn .= $aParams['page'];
        }


        if (!empty($aParams['sort']) || !empty($aParams['page'])) {
            $sReturn .= '.html';
        }


        return $sReturn;
    }

}
