<?php

class Zend_View_Helper_SubcategoryUrl extends Zend_View_Helper_Abstract {

    public function subcategoryUrl($aParams = array(), $aAppend = null, $sReduce = null, $sPaginator = false) {


        $aBuild = array();
        $aFilters = array();

        $aBuild = '/3/' . $aParams['url'];

        if ($aAppend !== null && isset($aAppend['filters'])) {

            if (count($aParams['filters']) == 2) {

                $aParams['filters'] = array_slice($aParams['filters'], 0, 1, true);
            }

            foreach ($aAppend['filters'] as $sFilterName => $sFilter) {

                if (isset($aParams['filters'][$sFilterName])) {
                    unset($aParams['filters'][$sFilterName]);
                }

                $aFilters[] = $sFilterName . '-' . str_replace(',', '.', $sFilter);
            }
        }

        if (isset($aParams['filters'])) {
            foreach ($aParams['filters'] as $sFilterName => $sFilter) {

                if ($sReduce != $sFilterName) {
                    $aFilters[] = $sFilterName . '-' . str_replace(',', '.', $sFilter);
                }
            }
        }


        sort($aFilters);

        if (count($aFilters) > 0) {
            $aBuild .= '/f/' . implode(',', $aFilters);
        }

        if (isset($aAppend['page']) && !empty($aAppend['page']) && $aAppend['page'] > 1) {


            $aBuild .= '/p/' . $aAppend['page'];
        } elseif (isset($aParams['page']) && $aParams['page'] > 1 && $aAppend['page'] > 1 && $sPaginator) {

            $aBuild .= '/p/' . $aParams['page'];
        }


        if (!empty($aParams['tag']) && $sReduce != 'tag') {
            $aBuild .= '/t/' . $aParams['tag'];
        }


        return $aBuild;
    }

}
