<?php

class Zend_View_Helper_SearchUrl extends Zend_View_Helper_Abstract {

    public function searchUrl($aParams = array(), $aAppend = null, $sReduce = null) {
        
        
        
                
        $aBuild = array();
        $aFilters = array();

        $aBuild = '/s/' . $this->view->seo($aParams['search']);

        

      
          if ($aAppend !== null && isset($aAppend['filters'])) {

            foreach ($aAppend['filters'] as $sFilterName => $sFilter) {
                    
                if(isset($aParams['filters'][$sFilterName]))
                {
                unset($aParams['filters'][$sFilterName]); 
                }
                
                
                $aFilters[] = $sFilterName . '-' . str_replace(',', '.', $sFilter);
            }
        }
        
        
         if(isset($aParams['filters']))
        {
        
        foreach ($aParams['filters'] as $sFilterName => $sFilter) {

            if ($sReduce != $sFilterName) {
                $aFilters[] = $sFilterName . '-' . str_replace(',', '.', $sFilter);
            }
        }
        }
        
        if (count($aFilters) > 0) {
            $aBuild .= '/f/' . implode(',', $aFilters);
        }
        
        
         if (isset($aAppend['group']) && !empty($aAppend['group'])) {


            $aBuild .= '/g/' . $aAppend['group'];
        } elseif (!empty($aParams['group']) && $sReduce != 'group') {

            $aBuild .= '/g/' . $aParams['group'];
        }
        
        
         if (isset($aAppend['category']) && !empty($aAppend['category'])) {


            $aBuild .= '/c/' . $aAppend['category'];
        } elseif (!empty($aParams['category']) && $sReduce != 'category') {

            $aBuild .= '/c/' . $aParams['category'];
        }
        
       

        if (isset($aAppend['page']) && !empty($aAppend['page'])) {


            $aBuild .= '/p/' . $aAppend['page'];
            
        } elseif (!empty($aParams['page']) && $sReduce != 'page') {

            $aBuild .= '/p/' . $aParams['page'];
        }
        
       
        if (!empty($aParams['tag']) && $sReduce != 'tag') {
            $aBuild .= '/t/' . $aParams['tag'];
        }


        return $aBuild;
    }

}
