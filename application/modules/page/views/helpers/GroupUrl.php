<?php

class Zend_View_Helper_GroupUrl extends Zend_View_Helper_Abstract {

    public function groupUrl($aParams = array(), $aAppend = null, $sReduce = null) {
        
        
        $aBuild = array();
        $aFilters = array(); 
        
        $aBuild = '/2/'.$aParams['url'];


            if (isset($aParams['filters']['pf']) && !empty($aParams['filters']['pf']) && $sReduce != 'pf') {
                $aFilters[] = 'pf-' . str_replace(',' , '.' , $aParams['filters']['pf']);
            }


            if (isset($aParams['filters']['pt']) && !empty($aParams['filters']['pt']) && $sReduce != 'pt') {
                $aFilters[] = 'pt-' . str_replace(',' , '.' , $aParams['filters']['pt']);
            }
        
        if (count($aFilters) > 0) {
            $aBuild .= '/f/' . implode(',', $aFilters);
        }

        if (isset($aAppend['page']) && !empty($aAppend['page'])) {


            $aBuild .= '/p/' . $aAppend['page'];
        } elseif (!empty($aParams['page']) && $sReduce != 'page') {

            $aBuild .= '/p/' . $aAppend['page'];
        }


        if (!empty($aParams['tag']) && $sReduce != 'tag') {
            $aBuild .= '/t/' . $aParams['tag'];
        }


        return $aBuild; 
    }

}
