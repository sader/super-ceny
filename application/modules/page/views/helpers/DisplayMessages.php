<?php
/**
 * Zend_View_Helper_DisplayMessages
 * @author Krzysztof Janicki
 */
class Zend_View_Helper_DisplayMessages extends Zend_View_Helper_Abstract {

    
     
    /**
     * Metoda zwraca sformatowane wiadomości
     * @param array $aData
     * @return string 
     */
    
    public function displayMessages() {
    
        
        $oFlashMessenger = Zend_Controller_Action_HelperBroker::getStaticHelper('flashMessenger');
        $aData = array_merge($oFlashMessenger->getCurrentMessages() , $oFlashMessenger->getMessages()); 
        if(empty($aData) || !is_array($aData)) return ''; 
        
        return "<script type=\"text/javascript\"> alertify.alert('".$aData[0]['message']."');</script>";
            
        }
       
    

}
