<?php

/**
 * Zend_View_Helper_DisplayMessages
 * @author Krzysztof Janicki
 */
class Zend_View_Helper_BuildPriceParam extends Zend_View_Helper_Abstract {

    /**
     * Metoda zwraca sformatowane wiadomości
     * @param array $aData
     * @return string 
     */
    public function buildPriceParam($sString) {

        $aMatches = array();
        $aReturn = array();

        switch (true) {
            case preg_match('/.*(\d+)-(\d+).*/', $sString, $aMatches):

                $aReturn['pf'] = $aMatches[1];
                $aReturn['pt'] = $aMatches[2];
                break;
            case preg_match('/(\d+)\sdo\s(\d+).*/', $sString, $aMatches):
                $aReturn['pf'] = $aMatches[1];
                $aReturn['pt'] = $aMatches[2];

                break;
            
             case preg_match('/od\s(\d+).*?do\s(\d+).*/', $sString, $aMatches):
                $aReturn['pf'] = $aMatches[1];
                $aReturn['pt'] = $aMatches[2];

                break;


            case preg_match('/do\s(\d+).*/', $sString, $aMatches):
                $aReturn['pt'] = $aMatches[1];
                break;

            case preg_match('/(pow\.|powyżej|ponad|pow)\s(\d+).*/', $sString, $aMatches):
                $aReturn['pf'] = $aMatches[2];
                break;

            default:
            case preg_match('/od\s(\d+).*/', $sString, $aMatches):
                     $aReturn['pf'] = $aMatches[1];
                break;
        }
        
        return $aReturn; 
        
    }

}
