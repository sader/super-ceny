<?php

/**
 * formatLangs
 *
 * @author Krzysztof Janicki
 */
class Zend_View_Helper_Image extends Zend_View_Helper_Abstract {
    
    
    
    private $width = null; 
    private $height = null; 
    private $defaultWidth = 640; 
    private $defaultHeight = 480; 

    public function image($iId, $sName = null, $iWidth  = null, $iHeight = null) {
        
        if($sName == null) return ''; 
        
        $this->width = empty($iWidth) ? $this->defaultWidth : $iWidth; 
        $this->height = empty($iHeight) ? $this->defaultHeight : $iHeight; 
        
        
     return sprintf('<img src="%s" width="%s" , height="%s" />' , 
                     Model_Logic_Gallery::getInstance()->getImageSrc($sName, $iId),
                     $this->width,
                     $this->height); 
        
    }

}
