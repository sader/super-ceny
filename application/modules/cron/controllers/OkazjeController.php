<?php

class Cron_OkazjeController extends App_Controller_Cron_Abstract {

    public function indexAction() {

        $aClasses = get_class_methods(__CLASS__);
        foreach ($aClasses as $sClass) {
            if (preg_match('/Action/', $sClass)) {
                $sAction = str_replace('Action', '', $sClass); 
                $sUrl = '/' . $this->getRequest()->getModuleName() . '/' . $this->getRequest()->getControllerName() . '/' . $sAction;
                echo '<a href="' . $sUrl . '">' . $sAction . '</a><br />';
            }
        }
    }

    public function maincategoriesAction() {
        Model_Logic_Okazje_Parser::getInstance()->getMainCategories(); 
    }
    
    public function categoryAction()
    {
        Model_Logic_Okazje_Parser::getInstance()->manageCategory(); 
    }
       
}

