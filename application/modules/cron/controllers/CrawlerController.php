<?php

class Cron_CrawlerController extends App_Controller_Cron_Abstract {

    public function indexAction() {

        $aClasses = get_class_methods(__CLASS__);
        foreach ($aClasses as $sClass) {
            if (preg_match('/Action/', $sClass)) {
                $sAction = str_replace('Action', '', $sClass);
                $sUrl = '/' . $this->getRequest()->getModuleName() . '/' . $this->getRequest()->getControllerName() . '/' . $sAction;
                echo '<a href="' . $sUrl . '">' . $sAction . '</a><br />';
            }
        }
    }

    public function categoriesAction() {
        Model_Logic_Skapiec_Parser::getInstance()->manageCategory();
    }

    public function productsAction() {
        Model_Logic_Skapiec_Parser::getInstance()->manageProducts();
    }

    public function filtersAction() {
        Model_Logic_Skapiec_Api::getInstance()->manageFiltersForCategory();
    }

    public function parameterAction() {
        Model_Logic_Skapiec_Parser::getInstance()->manageParameter();
    }

    public function filtersandparametersAction() {

        include 'phpQuery.php';
        $aCategories = Model_DbTable_CategoryThree::getInstance()->getFilterListJob(10);

        foreach ($aCategories as $aCategory) {
            $iResult = Model_Logic_Skapiec_Parser::getInstance()->manageParamsForCategory($aCategory['id']) ? 1 : 2;
            Model_DbTable_CategoryThree::getInstance()->update(array('job_filters' => $iResult), array('id = ? ' => $aCategory['id']));
        }

        
    }

}
