<?php

class Cron_GoogleController extends App_Controller_Cron_Abstract {

    public function indexAction() {

        $aClasses = get_class_methods(__CLASS__);
        foreach ($aClasses as $sClass) {
            if (preg_match('/Action/', $sClass)) {
                $sAction = str_replace('Action', '', $sClass);
                $sUrl = '/' . $this->getRequest()->getModuleName() . '/' . $this->getRequest()->getControllerName() . '/' . $sAction;
                echo '<a href="' . $sUrl . '">' . $sAction . '</a><br />';
            }
        }
    }

    public function wordsAction() {
        
     $aArrays = array(); 
     $aArrays['one'] =   Model_DbTable_CategoryOne::getInstance()->getNames(); 
     $aArrays['two'] =   Model_DbTable_CategoryTwo::getInstance()->getNames(); 
 
   
     
     $aArrays['three'] =  Model_DbTable_CategoryThree::getInstance()->getNames(); 
     
     
     $aArrays['params'] =    Model_DbTable_Parameter::getInstance()->getNamesWithCategoryThreeName();
     $aFile = file(APPLICATION_PATH . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'frazy.txt'); 
     
     $aFileArray = array(); 
     foreach($aFile as $sName)
     {
         $aFileArray[] = array('name' => trim($sName));
     }
     
     $aArrays['fromfile'] = $aFileArray; 
     
     
     
        $aData = array_merge($aArrays['one'], $aArrays['two'], $aArrays['three'], $aArrays['params'], $aArrays['fromfile']); 
       
        
        Model_DbTable_GoogleNames::getInstance()->multipleInsert($aData); 
        
        
    }

    public function getbywordAction() {
        
        
        $iJobsPerCycle = 10; 
        
      $aJobs = Model_DbTable_GoogleNames::getInstance()->getJobs($iJobsPerCycle);
        
      
      if(count($aJobs) > 0)
      {
          
          $aProxies =  Model_Logic_ProxyMarket_Api::getInstance()->getProxy(); 
          
          foreach($aJobs as $aJob)
          {
              
              $aData = Model_Logic_Google_Parser::getInstance()->getOkazjeInfoWords($aJob, $aProxies[array_rand($aProxies)]);
              
              if(!empty($aData))
              {
                  Model_DbTable_GoogleResults::getInstance()->multipleInsert($aData); 
              }
              
              
              Model_DbTable_GoogleNames::getInstance()->update(array('done' => 1) , array('id = ?' => $aJob['id']));
              
          }
      }
      
        
    }

}
