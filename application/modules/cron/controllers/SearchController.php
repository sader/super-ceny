<?php

class Cron_SearchController extends App_Controller_Cron_Abstract {

    
     public function preDispatch()
     {
     }
    
    public function indexAction() {

        $aClasses = get_class_methods(__CLASS__);
        foreach ($aClasses as $sClass) {
            if (preg_match('/Action/', $sClass)) {
                $sAction = str_replace('Action', '', $sClass);
                $sUrl = '/' . $this->getRequest()->getModuleName() . '/' . $this->getRequest()->getControllerName() . '/' . $sAction;
                echo '<a href="' . $sUrl . '">' . $sAction . '</a><br />';
            }
        }
    }

    public function populateAction() {

        $iPerLoop = 10000;

        $iLastLoop = file_exists('loop') ?  file_get_contents('loop') : 0;
        
        
        for ($iLoop = $iLastLoop; $iLoop < 200; $iLoop++) {
            $iOffset = $iLoop * $iPerLoop;
            
            
            
            $aData = Model_DbTable_Product::getInstance()->getAll($iPerLoop, $iOffset);

            if(empty($aData)) 
            {
             echo "no data for limit $iPerLoop : $iOffset";
                return; 
            }
                $aInsert = array(); 
            
            foreach ($aData as $aRow) {
                $aInsert[] = array('id' => $aRow['id'], 'name' => Model_Logic_Search::getInstance()->seo($aRow['product'], ' '));
            }
            
            Model_DbTable_ProductSearch::getInstance()->multipleInsert($aInsert, 10000); 
            
            file_put_contents('loop', $iLoop); 
            unset($aInsert);
            unset($aData); 
        }
    }
}