<?php

class Cron_LuceneController extends App_Controller_Cron_Abstract {

    public function indexAction() {

        $aClasses = get_class_methods(__CLASS__);
        foreach ($aClasses as $sClass) {
            if (preg_match('/Action/', $sClass)) {
                $sAction = str_replace('Action', '', $sClass); 
                $sUrl = '/' . $this->getRequest()->getModuleName() . '/' . $this->getRequest()->getControllerName() . '/' . $sAction;
                echo '<a href="' . $sUrl . '">' . $sAction . '</a><br />';
            }
        }
    }

    public function populateAction() {
        Model_Logic_Search::getInstance()->populate(); 
    }

}

