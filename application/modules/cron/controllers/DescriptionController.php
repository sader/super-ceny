<?php

class Cron_DescriptionController extends App_Controller_Cron_Abstract {

    public function indexAction() {

        $aClasses = get_class_methods(__CLASS__);
        foreach ($aClasses as $sClass) {
            if (preg_match('/Action/', $sClass)) {
                $sAction = str_replace('Action', '', $sClass);
                $sUrl = '/' . $this->getRequest()->getModuleName() . '/' . $this->getRequest()->getControllerName() . '/' . $sAction;
                echo '<a href="' . $sUrl . '">' . $sAction . '</a><br />';
            }
        }
    }

    public function categoriesAction() {

        $iJobs = 100;
            include('phpquery.php'); 
            
        $aCategories = Model_DbTable_CategoryOne::getInstance()->getBundleCategories($iJobs);

        if (!empty($aCategories)) {

            include 'synonimek.php';

            foreach ($aCategories as $aCategory) {
                $aMatch = Model_DbTable_CategoryInfo::getInstance()->getByUrl($aCategory['seo']);

                if (empty($aMatch)) {
                    $aMatch = Model_DbTable_CategoryInfo::getInstance()->getBySimiliarName($aCategory['name']);
                }


                $aSentences = preg_split('/(?<=[.?!])\s+(?=[a-z])/i', strip_tags($aMatch['description']));

                shuffle($aSentences);
                $sText = synonimek(implode(' ', $aSentences));

                switch ($aCategory['type']) {
                    case 'one':
                        $oAdapter = Model_DbTable_CategoryOne::getInstance();
                        break;
                    case 'two':
                        $oAdapter = Model_DbTable_CategoryTwo::getInstance();
                        break;
                    case 'three':
                        $oAdapter = Model_DbTable_CategoryThree::getInstance();
                        break;
                }

                $oAdapter->update(array('description' => $sText), array('id = ?' => $aCategory['id']));
            }
        }
    }

    public function gridAction() {

        $aCategories = Model_DbTable_CategoryThree::getInstance()->select()->query()->fetchAll(Zend_Db::FETCH_ASSOC);

        foreach ($aCategories as $aCategory) {
            $aProducts = Model_DbTable_Product::getInstance()->getMostPopularProductsByCategory($aCategory['id']);
            Model_DbTable_ProductDescription::getInstance()->multipleInsert($aProducts);
        }
    }

    public function productsAction() {
        
        include('phpQuery.php'); 
        $iJobs = 1;

        $aJobs = Model_DbTable_ProductDescription::getInstance()->getJobs($iJobs);

        foreach ($aJobs as $aJob) {

            $aData = Model_Logic_Skapiec_Parser::getInstance()->getDescriptionForProduct($aJob['id'], $aJob['id_category']);
            
            
        }
        Model_DbTable_ProductDescription::getInstance()->update(array('description' => base64_encode(serialize($aData))), array('id = ?' => $aJob['id']));
    }

}
