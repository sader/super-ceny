<?php

class Zend_View_Helper_InvoiceButtons extends Zend_View_Helper_Abstract {

    public function invoiceButtons($iId, $iStatus) {
        
         return !$iStatus ?  '<a class="btn btn-success" href="/admin/payment/manage/id/'.$iId.'/type/1">Opłacona</a>' 
                         :  '<a  class="btn" href="/admin/payment/manage/id/'.$iId.'/type/0">Nie opłacona</a>';
              
        
    }

}
