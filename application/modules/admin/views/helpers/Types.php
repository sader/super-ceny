<?php

/**
 * formatLangs
 *
 * @author Krzysztof Janicki
 */
class Zend_View_Helper_Types extends Zend_View_Helper_Abstract {



    public function types($sTypes) {

     return  implode('<br />' , array_values(array_intersect_key(Model_Logic_Tag::getTagTypes(), array_flip(explode(',', $sTypes)))));
        
    }

}



