<?php

class Zend_View_Helper_ShopButtons extends Zend_View_Helper_Abstract {

    public function shopButtons($iId, $iPromoted, $iSelled) {
        
            $sPromotionButton = '<a title="%s" class="btn tlp promotion %s" href="%s"><i class="%s"></i></a>'; 
            
            $sReturn = ($iPromoted == 0) 
            ? sprintf($sPromotionButton, 'Promuj' , 'promote', $this->view->url(array('action' => 'promote', 'id' => $iId, 'type' => 1)), 'icon-star')
            : sprintf($sPromotionButton, 'Odpromuj' , 'unpromote', $this->view->url(array('action' => 'promote', 'id' => $iId, 'type' => 0)), 'icon-star-empty');
            
            
            $sReturn .= '<a title="xml" class="btn tlp promotion" href="'.$this->view->url(array('action' => 'xml', 'id' => $iId, 'type' => 1)).'"><i class=" icon-arrow-down"></i></a>';
            
        return $sReturn; 
    }

}
