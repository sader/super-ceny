<?php

class Zend_View_Helper_IsSeller {

    public function isSeller($iId) {
        return $iId == 0 ?     '<span class="label label-important">Nie</span>' 
                               : '<span class="label label-success"><a href="'.$this->view->url(array('module' => 'admin' , 
                                                                                                      'controller' => 'shop' , 
                                                                                                      'action' => 'seller',
                                                                                                      'id' => $iId)).'">Tak</span>'; 
    }

}
