<?php

class Zend_View_Helper_SellerButtons extends Zend_View_Helper_Abstract {

    public function sellerButtons($iId, $iStatus) {
        
            $sReturn = '';
            
            if($iStatus == 0)
            {
            $sReturn .= '<a class="btn btn-success" href="'.$this->view->url(array('action' => 'activate' , 'id' => $iId)).'">Aktywacja</a>';
            }
            else
            {
            //     $sReturn .= '<a class="btn" href="'.$this->view->url(array('action' => 'finance' , 'id' => $iId)).'">Finanse</a>';   
            }
            
            $sReturn .= '<a class="btn" href="'.$this->view->url(array('action' => 'details' , 'id' => $iId)).'">Detale</a>';
            $sReturn .= '<a class="btn btn-warning" href="'.$this->view->url(array('action' => 'remove' , 'id' => $iId)).'">Usuń</a>';
            
        return $sReturn; 
    }

}
