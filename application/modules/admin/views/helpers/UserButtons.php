<?php

class Zend_View_Helper_UserButtons extends Zend_View_Helper_Abstract {

    public function userButtons($iId) {
        
              $sReturn = '<a class="btn btn-warning" href="'.$this->view->url(array('action' => 'remove' , 'id' => $iId)).'">Usuń</a>';
              $sReturn .= '<a class="btn" href="'.$this->view->url(array('action' => 'edit' , 'id' => $iId)).'">Dane</a>';
              
        return $sReturn; 
    }

}
