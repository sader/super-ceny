<?php

class Zend_View_Helper_Label {

    public function label($sString, $sClass= "") {
        
        if(!empty($sClass))
        {
            $sClass = ' '.$sClass; 
        }
        
        return empty($sString) ? '<span class="label label-important'.$sClass.'">Nie</span>' 
                               : '<span class="label label-success'.$sClass.'">Tak</span>'; 
    }

}
