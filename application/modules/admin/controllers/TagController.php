<?php

class Admin_TagController extends App_Controller_Admin_Abstract {
    
    public function init()
    {
        parent::init(); 
        $this->view->active = 'static'; 
    }


    public function indexAction() {
        
        $this->forward('list'); 
    }

    
     public function removeAction() {
        Model_Logic_Tag::getInstance()->manageRemove($this);
    }

    
     public function addAction() {
        Model_Logic_Tag::getInstance()->manageAdd($this);
    }

    
    public function editAction() {
        Model_Logic_Tag::getInstance()->manageEdit($this);
    }


    public function listAction() {
        Model_Logic_Tag::getInstance()->manageList($this);
    }
    
    public function multiAction()
    {
        
        $this->_helper->json(Model_Logic_Category::getInstance()->categorySelectInput($this));
    }
    
   
   
}