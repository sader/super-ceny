<?php

class Admin_StaticController extends App_Controller_Admin_Abstract {
    
    public function init()
    {
        parent::init(); 
        $this->view->active = 'static'; 
    }


    public function indexAction() {
        
        $this->forward('list'); 
    }

    
     public function removeAction() {
        Model_Logic_Static::getInstance()->manageRemove($this);
    }

    
     public function addAction() {
        Model_Logic_Static::getInstance()->manageAdd($this);
    }

    
    public function editAction() {
        Model_Logic_Static::getInstance()->manageEdit($this);
    }


    public function listAction() {
        Model_Logic_Static::getInstance()->manageList($this);
    }
    
   
   
}