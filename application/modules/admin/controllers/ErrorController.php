<?php

class Admin_ErrorController extends App_Controller_Admin_Abstract {
    
    public function init()
    {
        parent::init(); 
    }


    public function indexAction() {
        
        $this->forward('list'); 
    }

    
     public function removeAction() {
        $this->_helper->json(Model_Logic_Error::getInstance()->manageRemove($this));
    }

  
    public function listAction() {
        Model_Logic_Error::getInstance()->manageList($this);
    }
    
  
   
}