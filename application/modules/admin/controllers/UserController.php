<?php

class Admin_UserController extends App_Controller_Admin_Abstract {

    public function listAction() {
        Model_Logic_User::getInstance()->manageList($this);
    }

    public function removeAction() {
       Model_Logic_User::getInstance()->manageRemove($this);
    }
    
    public function editAction()
    {
        
        Model_Logic_User::getInstance()->manageAdminPanel($this);
        
    }
    
    
    

}

