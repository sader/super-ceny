<?php

class Admin_Form_Login extends App_Form {

    public function init() {
        
        parent::init(); 



        $this->_aFields['login'] = new Zend_Form_Element_Text('login');
        $this->_aFields['login']->setLabel('login')
                ->setRequired(true)
                ->addValidator('NotEmpty', true, array('messages' => "Poda login"))
                ->addFilter('StringTrim')
                ->addFilter('HtmlSpecialchars');

        $this->_aFields['password'] = new Zend_Form_Element_Password('password');
        $this->_aFields['password']->setLabel('password')
                ->setRequired(true)
                ->addValidator('NotEmpty', true, array('messages' => "Poda hasło"))
                ->addFilter('StringTrim')
                ->addFilter('HtmlSpecialchars');
                
                


        $this->_aFields['submit'] = new Zend_Form_Element_Submit('submit');
        $this->_aFields['submit']->setLabel('Zaloguj');

        $this->addElements($this->_aFields)
			  ->setMethod(Zend_Form::METHOD_POST); 
			  
    }
	

}


