<?php

class Admin_Form_Static extends App_Form {

    public function init() {

        parent::init();

        $this->_aFields['name'] = new Zend_Form_Element_Text('name');
        $this->_aFields['name']->setLabel('Tytuł')
                ->setRequired(true)
                ->setAttrib('class', 'input-xxlarge')
                ->addFilter('StringTrim')
                ->addFilter('HtmlSpecialchars')
                ->addValidator('Regex' , true, array('pattern' => '/^[a-z0-9\-_]+$/' , 
                                                     'messages' => 'Nawa url strony powinna posiadać jedynie małe litery, cyfry oraz znaki - i _'))
                ->addValidator('NotEmpty', true, array('messages' => "Podaj adres strony"))
                ->addValidator('Db_NoRecordExists', true, array('table' => 'static',
                    'field' => 'name',
                    'messages' => 'Strona o podaniej nazwie istnieje'));

        $this->_aFields['content'] = new Zend_Form_Element_Textarea('content');
        $this->_aFields['content']->setLabel('Treść')
                ->setRequired(true)
                ->setAttrib('class', 'input-xxlarge ckeditor')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty', true, array('messages' => "Brak treści newsa"));


        

        $this->_aFields['submit'] = new Zend_Form_Element_Submit('submit');
        $this->_aFields['submit']->setLabel('Zapisz');

        $this->addElements($this->_aFields)
                ->setMethod(Zend_Form::METHOD_POST);
    }

}