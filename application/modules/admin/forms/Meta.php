<?php

class Admin_Form_Meta extends App_Form {

    public function __construct() {

        parent::__construct();
        
         
        $oElem = new Zend_Form_Element_Text('name');
        $oElem->setLabel('Nazwa tagu')
                ->setAttrib('class', 'input-xxlarge focusable')
                ->setRequired(true)
                ->addValidator('NotEmpty' , true , array('messages'  => 'Pole wymagane'))
                  ->addValidator('Db_NoRecordExists', true, array('table' => 'tags',
                    'field' => 'name',
                    'messages' => 'Tag o podaniej nazwie istnieje'));
        
         $this->addElement($oElem);
        
        $oElem = new Zend_Form_Element_Textarea('title');
        $oElem->setLabel('Title')
                ->setRequired(true)
                ->addValidator('NotEmpty' , true , array('messages'  => 'Pole wymagane'))
                ->setAttrib('class', 'input-xxlarge focusable');
        
        $this->addElement($oElem);

        $oElem = new Zend_Form_Element_Textarea('description');
        $oElem->setLabel('Description')
                ->setRequired(true)
                ->addValidator('NotEmpty' , true , array('messages'  => 'Pole wymagane'))
                ->setAttrib('class', 'input-xxlarge focusable'); 
        
        $this->addElement($oElem);
        
        $oElem = new Zend_Form_Element_Textarea('keywords');
        $oElem->setLabel('Keywords')
                ->setRequired(true)
               ->addValidator('NotEmpty' , true , array('messages'  => 'Pole wymagane'))
                ->setAttrib('class', 'input-xxlarge focusable'); 
        
        $this->addElement($oElem);
        
        $oElem = new Zend_Form_Element_MultiCheckbox('type');
        $oElem->setLabel('Typy adresów związanych z tagiem')
                ->setRequired(true)
                ->addValidator('NotEmpty' , true , array('messages'  => 'Pole wymagane'));
                
        
         $this->addElement($oElem);
         $this->populateSelect(Model_Logic_Tag::getTagTypes(), 'type', false, false);
        
         $oElem = new Zend_Form_Element_Hidden('cat3res');
         $this->addElement($oElem);
         
         $oElem = new Zend_Form_Element_Hidden('cat4res');
         $this->addElement($oElem);
         
         
        $oElem = new Zend_Form_Element_Submit('submit');
        $oElem->setLabel('Dodaj');

        $this->addElement($oElem);
    }
    
    public function editMode()
    {
        $this->getElement('submit')->setLabel('Edytuj');
        $this->getElement('name')->removeValidator('Db_NoRecordExists');
    }

}

