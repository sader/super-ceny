<?php

class Admin_Grid_Tag extends Admin_Grid_Abstract{
 
    
    public function __construct() {
        
        parent::__construct(); 

        

        $this->oGrid->setSource(new Bvb_Grid_Source_Zend_Select(Model_DbTable_Tag::getInstance()->getList()));
        $this->oGrid->setExport(array());

          
          $this->oGrid->updateColumn('id', array('remove' => true));
          $this->oGrid->updateColumn('name', array('title' => 'Tag'));

          
          
        $oDetailColumn = new Bvb_Grid_Extra_Column();
        $oDetailColumn->setPosition('right')
                ->setName('Operacje');

        
            $oDetailColumn->setDecorator('<a data-toggle="tooltip" title="Edytuj" class="btn tlp" href="/admin/tag/edit/id/{{id}}"><i class="icon-edit"></i>'
                    . '<a data-toggle="tooltip" title="Usuń" class="btn tlp btn-danger confirmation" href="#" link="/admin/tag/remove/id/{{id}}"><i class="icon-remove"></i></a>');
              
            
              $this->oGrid->updateColumn('type', array('title' => 'Typy' ,  
                                                   'helper' => array('name' => 'types', 
                                                   'params' => array('{{type}}'))));
              
              
              $this->oGrid->updateColumn('cat3res', array('title' => 'Pokategorie' ,  
                                                   'helper' => array('name' => 'categories', 
                                                   'params' => array('{{cat3res}}'))));
              
              $this->oGrid->updateColumn('cat4res', array('title' => 'Podkategorie z filtrem' ,  
                                                   'helper' => array('name' => 'categories', 
                                                   'params' => array('{{cat4res}}'))));
              
            
          
          $this->oGrid->addExtraColumn($oDetailColumn);
        

        $oRight = new Bvb_Grid_Extra_Column('lp');
        $oRight->position('left')
                ->name('lp')
                ->title('#')
                ->callback(array(
                    'function' => array($this, 'increment'),
                    'params' => array()));
        $this->oGrid->addExtraColumns($oRight);
    }

}