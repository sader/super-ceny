<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Abstrac
 *
 * @author sader
 */
class Admin_Grid_Abstract {

    protected $oGrid = null;
    public $inc = 1;

    public function __construct() {

        $this->oGrid = Bvb_Grid::factory('Table', array(), 'tablegrid');
        $this->oGrid->setImagesUrl('/images/zfdatagrid/');
        $this->oGrid->setDeployOption('enableUnmodifiedFieldPlaceholders', 1);
        $this->oGrid->setCharEncoding('UTF-8');
        $this->oGrid->setUseKeyEventsOnFilters(true);
        $this->oGrid->setExport(array());
        $this->oGrid->setNoFilters(true);
    }

    public function increment() {

        $oReq = Zend_Controller_Front::getInstance()->getRequest();
        $iParam = $oReq->getParam('starttablegrid', 0);
        return $iParam + $this->inc++;
    }

    public function deploy() {
        return $this->oGrid->deploy();
    }

}