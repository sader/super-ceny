<?php

class Model_DbTable_ProductCategory extends App_Db_Table {

    protected $_name = 'product_category_three';




    /**
     * Instancja klasy.
     * 
     * @var Model_DbTable_ProductCategory
     */
    static private $_oInstance;
    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_DbTable_ProductCategory
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    

    public function add($idProduct, $idCategory) {
        $this->insert(array('id_product' => $idProduct, 'id_category_three' => $idCategory));
    }
  

  
    public function isProductCategoryExist($iProduct, $iCategory)
    {
        $aBind = $this->select()
                       ->where('id_product = ?' , $iProduct)
                       ->where('id_category_three = ?', $iCategory)
                       ->query()
                       ->fetch(Zend_Db::FETCH_NUM);
        
        return !empty($aBind); 
    }

}

