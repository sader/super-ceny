<?php

class Model_DbTable_CategoryThree extends App_Db_Table {

    protected $_name = 'category_three';
    protected $_cache = false;

    const JOB_FINISHED = 1;
    const JOB_UNFINISHED = 0;
    const CATEGORY_IS_PROMOTED = 1;
    const CATEGORY_IS_NOT_PROMOTED = 0;

    /**
     * Instancja klasy.
     * 
     * @var Model_DbTable_CategoryThree
     */
    static private $_oInstance;

    // ------------------------------------------------------------------------

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_DbTable_CategoryThree
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function add($aData) {
        return $this->insert($aData);
    }

    public function exists($iId) {
        $aRow = $this->select()->where('id = ?', $iId)->query()->fetch(Zend_Db::FETCH_ASSOC);
        return !empty($aRow);
    }

    public function getProductListJob($iLimit = 6) {
        return $this->select()->order('job_products ASC')
                        ->limit($iLimit)
                        ->query()
                        ->fetchAll(Zend_Db::FETCH_ASSOC);
    }

    public function getFilterListJob($iLimit = 1) {
        return $this->select()->where('job_filters  = ?', self::JOB_UNFINISHED)
                        ->order('promoted DESC')
                        ->limit($iLimit)
                        ->query()
                        ->fetchAll(Zend_Db::FETCH_ASSOC);
    }

    public function getByIdCategoryTwo($iId) {
        return $this->select()->setIntegrityCheck(false)
                        ->setCacheEnable(true)
                        ->from(array('ctt' => 'category_two_three'), null)
                        ->where('ctt.id_category_two = ?', $iId)
                        ->join(array('ct' => 'category_three'), 'ct.id=ctt.id_category_three')
                        ->query()
                        ->fetchAll(Zend_Db::FETCH_ASSOC);
    }
    
     public function getFinishedByIdCategoryTwo($iId) {
        return $this->select()->setIntegrityCheck(false)
                        ->from(array('ctt' => 'category_two_three'), null)
                        ->where('ctt.id_category_two = ?', $iId)
                        ->join(array('ct' => 'category_three'), 'ct.id=ctt.id_category_three')
                        ->where('ct.job_filters  = ?' , 1)
                        ->query()
                        ->fetchAll(Zend_Db::FETCH_ASSOC);
    }
    

    public function getNamesByCategoryTwoUrls($aNames) {

        return $this->select()
                        ->setCache(Model_Logic_Category::getInstance()->getCache('meta'))
                        ->setIntegrityCheck(false)
                        ->from(array('ct' => $this->_name), array('GROUP_CONCAT(ct.name SEPARATOR " ")'))
                        ->join(array('ctt' => 'category_two_three'), 'ct.id=ctt.id_category_three', null)
                        ->where('ctt.id_category_two IN(SELECT id from category_two where url IN(?))', $aNames)
                        ->query()
                        ->fetchAll(Zend_Db::FETCH_COLUMN, 0);
    }

    public function getByIdCategoryTwoMeta($iId) {
        return $this->select()
                        ->setCache(Model_Logic_Category::getInstance()->getCache('meta'))
                        ->setIntegrityCheck(false)
                        ->from(array('ctt' => 'category_two_three'), null)
                        ->where('ctt.id_category_two = ?', $iId)
                        ->join(array('ct' => 'category_three'), 'ct.id=ctt.id_category_three', array('name'))
                        ->query()
                        ->fetchAll(Zend_Db::FETCH_COLUMN, 0);
    }

    public function getByIdCategoryThreeMeta($iId) {
        return $this->select()
                        ->setCache(Model_Logic_Category::getInstance()->getCache('meta'))
                        ->setIntegrityCheck(false)
                        ->from(array('ctt' => 'category_two_three'), null)
                        ->where('ctt.id_category_two = (SELECT id_category_two from category_two_three where id_category_three = ? LIMIT 1)', $iId)
                        ->join(array('ct' => 'category_three'), 'ct.id=ctt.id_category_three', array('name'))
                        ->query()
                        ->fetchAll(Zend_Db::FETCH_COLUMN, 0);
    }

    public function getByIdCategoryTwoWithCount($iId) {
        return $this->select()->setIntegrityCheck(false)
                        ->from(array('ctt' => 'category_two_three'), null)
                        ->where('ctt.id_category_two = ?', $iId)
                        ->join(array('ct' => 'category_three'), 'ct.id=ctt.id_category_three')
                        ->query()
                        ->fetchAll(Zend_Db::FETCH_ASSOC);
    }

    public function setDescription($iId, $sDescription) {
        $this->update(array('description' => $sDescription), array('id = ?' => $iId));
    }

    public function getPromoted() {
        return $this->select()->from($this->_name, array('id'))
                        ->where('promoted = ?', self::CATEGORY_IS_PROMOTED)->query()->fetchAll(Zend_Db::FETCH_COLUMN, 0);
    }

    public function getPromotedByCategoryOne($iId) {
        return $this->select()
                        ->setIntegrityCheck(false)
                        ->distinct()
                        ->from(array('cth' => 'category_three'), array('cth.id'))
                        ->join(array('ctw' => 'category_two_three'), 'ctw.id_category_three=cth.id', null)
                        ->join(array('ct' => 'category_two'), 'ctw.id_category_two=ct.id', null)
                        ->join(array('co' => 'category_one'), 'ct.id_category_one=co.id', null)
                        ->where('cth.promoted = ?', 1)
                        ->where('co.id  = ?', $iId)
                        ->query()
                        ->fetchAll(Zend_Db::FETCH_COLUMN, 0);
    }

    public function getPromotedByCategoryOneUri($sUrl) {
        return $this->select()
                        ->setIntegrityCheck(false)
                        ->distinct()
                        ->from(array('cth' => 'category_three'), array('cth.id'))
                        ->join(array('ctw' => 'category_two_three'), 'ctw.id_category_three=cth.id', null)
                        ->join(array('ct' => 'category_two'), 'ctw.id_category_two=ct.id', null)
                        ->join(array('co' => 'category_one'), 'ct.id_category_one=co.id', null)
                        ->where('cth.promoted = ?', 1)
                        ->where('co.id  = (SELECT id from category_one where seo = "' . $sUrl . '" LIMIT 1)')
                        ->query()
                        ->fetchAll(Zend_Db::FETCH_COLUMN, 0);
    }

    public function getPromotedByCategoryOneUrl($sUrl) {
        return $this->select()
                        ->setIntegrityCheck(false)
                        ->distinct()
                        ->from(array('cth' => 'category_three'), array('cth.id', 'cth.name', 'cth.url'))
                        ->join(array('ctw' => 'category_two_three'), 'ctw.id_category_three=cth.id', null)
                        ->join(array('ct' => 'category_two'), 'ctw.id_category_two=ct.id', null)
                        ->join(array('co' => 'category_one'), 'ct.id_category_one=co.id', null)
                        ->join(array('f' => 'filters'), 'f.id_category_three=cth.id', array('filter_id' => 'id', 'filter_name' => 'name'))
                        ->join(array('p' => 'parameters'), 'f.id=p.id_filter', array('parameter_id' => 'id', 'parameter_name' => 'name'))
                        ->where('cth.promoted = ?', 1)
                        ->where('cth.job_filters = ?', 1)
                        ->where('f.name != ?', 'Cena')
                        ->where('co.seo  = ?', $sUrl)
                        ->query()
                        ->fetchAll(Zend_Db::FETCH_ASSOC);
    }

    public function random($iNum) {
        return $this->select()->where('job_filters = ?', 1)->limit($iNum)->order('RAND()')->query()->fetchAll(Zend_Db::FETCH_ASSOC);
    }

    public function getByUrl($sUrl) {

        return $this->select()
                        ->setIntegrityCheck(false)
                        ->setCacheEnable(true)
                        ->from(array('cth' => $this->_name), array('cth.id', 'cth.name', 'cth.url', 'cth.description'))
                        ->join(array('ctw' => 'category_two_three'), 'ctw.id_category_three=cth.id', null)
                        ->join(array('ct' => 'category_two'), 'ctw.id_category_two=ct.id', array('two_id' => 'id', 'two_name' => 'name', 'two_url' => 'url'))
                        ->join(array('co' => 'category_one'), 'ct.id_category_one=co.id', array('one_id' => 'id', 'one_name' => 'name', 'one_url' => 'seo'))
                        ->where('cth.url = ?', $sUrl)
                        ->limit(1)
                        ->query()
                        ->fetch(Zend_Db::FETCH_ASSOC);
    }

    public function getListForCategoryTwoByIds($sUrl, $aIds) {
       
        
        return $this->select()->setIntegrityCheck(false)
                            ->from(array('ctw' => 'category_two_three') , null)
                            ->join(array('pct' => 'product_category_three') , 'pct.id_category_three=ctw.id_category_three', null)
                            ->join(array('ct' => 'category_three') , 'ctw.id_category_three=ct.id' , array('name' , 'url', 'count' => new Zend_Db_Expr('COUNT(*)')))
                            ->where('pct.id_product IN(?)' , $aIds)
                            ->where('ctw.id_category_two = (SELECT id from category_two where category_two.url = "'.$sUrl.'")')
                            ->group('ct.id')
                            ->query()
                            ->fetchAll(Zend_Db::FETCH_ASSOC);
    }

    public function getDetailedList() {

        return $this->select()
                        ->setIntegrityCheck(false)
                        ->from(array('cth' => $this->_name), array('cth.id', 'cth.name'))
                        ->join(array('ctw' => 'category_two_three'), 'ctw.id_category_three=cth.id', null)
                        ->join(array('ct' => 'category_two'), 'ctw.id_category_two=ct.id', array('two_id' => 'id', 'two_name' => 'name'))
                        ->query()
                        ->fetchAll(Zend_Db::FETCH_ASSOC);
    }
    
      public function getNames()
    {
        
        return $this->select()->from($this->_name, array('name'))->query()->fetchAll(Zend_Db::FETCH_ASSOC); 
    }


}
