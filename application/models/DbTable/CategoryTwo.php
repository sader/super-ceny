<?php

class Model_DbTable_CategoryTwo extends App_Db_Table {

    protected $_name = 'category_two';
    protected $_cache = false;


    /**
     * Instancja klasy.
     * 
     * @var Model_DbTable_CategoryTwo
     */
    static private $_oInstance;
    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_DbTable_CategoryTwo
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

 
    public function getByIds($aIds) {
       return $this->select()->where('id IN (?)' , $aIds)->query()->fetchAll(Zend_Db::FETCH_ASSOC);
    }

     public function getById($aId) {
       return $this->select()->setCacheEnable(true)->where('id = ?' , $aId)->query()->fetch(Zend_Db::FETCH_ASSOC);
    }
    
    public function add($aData) {
       return $this->insert($aData);
    }

    public function getByIdCategoryOne($iId)
    {
        return $this->select()->setCacheEnable(true)->where('id_category_one = ?' , $iId)->query()->fetchAll(Zend_Db::FETCH_ASSOC); 
    }

      public function getNamesByIdCategoryOne($iId)
    {
        return $this->select()->from($this->_name, array('name'))->where('id_category_one = ?' , $iId)->query()->fetchAll(Zend_Db::FETCH_COLUMN, 0);
    }


     public function setDescription($iId, $sDescription)
    {
        $this->update(array('description' => $sDescription), array('id = ?' => $iId));
    }
    
    public function getByIdCategoryThree($iId)
    {   return $this->select()
                            ->setIntegrityCheck(false)
                            ->setCacheEnable(true)
                            ->from(array('ct' => 'category_two'), array('ct.name' , 'ct.id', 'ct.url'))
                            ->join(array('ctw' => 'category_two_three') , 'ctw.id_category_two=ct.id', null)
                            ->where('ctw.id_category_three = ?' , $iId)
                            ->limit(1)
                            ->query()
                            ->fetch(Zend_Db::FETCH_ASSOC);
    }


    
    public function getByProductId($iId)
    {
        
         return $this->select()
                            ->setIntegrityCheck(false)
                            ->from(array('ctw' => 'category_two_three') , null)
                            ->join(array('pct' => 'product_category_three') , 'pct.id_category_three=ctw.id_category_three', null)
                            ->join(array('ct' => 'category_two') , 'ctw.id_category_two=ct.id')
                            ->where('pct.id_product = ?' , $iId)
                            ->query()
                            ->fetch(Zend_Db::FETCH_ASSOC);
        
    }
    
    
    
    public function getByProductIds($iId)
    {
        
         return $this->select()
                            ->setIntegrityCheck(false)
                            ->from(array('ctw' => 'category_two_three') , null)
                            ->join(array('pct' => 'product_category_three') , 'pct.id_category_three=ctw.id_category_three', null)
                            ->join(array('ct' => 'category_two') , 'ctw.id_category_two=ct.id' , array('name' , 'url' , 'count' => new Zend_Db_Expr('COUNT(*)')))
                            ->where('pct.id_product IN(?)' , $iId)
                            ->group('ct.id')
                            ->order('count DESC')
                            ->query()
                            ->fetchAll(Zend_Db::FETCH_ASSOC);
        
    }
    


            public function getByCategoryOne($iCategoryOneId)
    {
        return $this->select()
                            ->setIntegrityCheck(false)
                            ->distinct()
                            ->setCacheEnable(true)
                            ->from(array('cth' => 'category_three'), array('cth.name' , 'cth.id', 'cth.url'))
                           // ->columns('(SELECT COUNT(*) from product_category_three where product_category_three.id_category_three = cth.id) as count')
                            ->join(array('ctw' => 'category_two_three') , 'ctw.id_category_three=cth.id', null)
                            ->join(array('ct' => 'category_two') , 'ctw.id_category_two=ct.id' , array('main_id' => 'ct.id' , 'main_name' => 'ct.name', 'main_seo' => 'ct.url'))
                            ->where('ct.id_category_one = ?' , $iCategoryOneId)
                            ->query()
                            ->fetchAll(Zend_Db::FETCH_ASSOC);
    }

    
                public function getByCategoryOneReal($iCategoryOneId)
    {
        return $this->select()
                            ->setIntegrityCheck(false)
                            ->distinct()
                            ->setCacheEnable(true)
                            ->from(array('ct' => 'category_two'), array('ct.name' , 'ct.id', 'ct.url'))
                           // ->columns('(SELECT COUNT(*) from product_category_three where product_category_three.id_category_three = cth.id) as count')
                            // ->join(array('ctw' => 'category_two_three') , 'ctw.id_category_three=cth.id', null)
                            // ->join(array('ct' => 'category_two') , 'ctw.id_category_two=ct.id' , array('main_id' => 'ct.id' , 'main_name' => 'ct.name', 'main_seo' => 'ct.url'))
                            ->where('ct.id_category_one = ?' , $iCategoryOneId)
                            ->query()
                            ->fetchAll(Zend_Db::FETCH_ASSOC);
    }


      public function getByUrl($sUrl) {

        return $this->select()
                        ->setIntegrityCheck(false)
                        ->setCacheEnable(true)
                        ->from(array('ct' => $this->_name), array('ct.id', 'ct.name' , 'ct.url', 'ct.description'))
                        ->join(array('co' => 'category_one'), 'ct.id_category_one=co.id', array('one_id' => 'id' , 'one_name' => 'name' , 'one_url' => 'seo'))
                        ->columns('(SELECT GROUP_CONCAT(id_category_three) from category_two_three where id_category_two=ct.id) as ids')
                        ->where('ct.url = ?', $sUrl)
                        ->query()
                        ->fetch(Zend_Db::FETCH_ASSOC);
    }
    
      public function getNames()
    {
        
        return $this->select()->from($this->_name, array('name'))->query()->fetchAll(Zend_Db::FETCH_ASSOC); 
    }

 
    
}

