<?php

class Model_DbTable_Meta extends Zend_Db_Table_Abstract {

    protected $_name = 'meta';

    /**
     * Instancja klasy.
     * 
     * @var Model_DbTable_Meta
     */
    static private $_oInstance;
    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_DbTable_Meta
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }
    
    /**
     * Lista eventów
     * @return Zend_Db_Select 
     */
    public function add($aData)
    {
       $this->delete(array('id = ?' => $aData['id']));
       $this->insert($aData);
        
    }
    
    public function get()
    {
        return $this->select()->query()->fetchAll(Zend_Db::FETCH_ASSOC); 
    }
   
    public function  getById($iId)
    {
        return $this->select()->where('id = ?' , $iId)->query()->fetch(Zend_Db::FETCH_ASSOC);
    }
  
    
}