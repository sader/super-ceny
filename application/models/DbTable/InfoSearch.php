<?php

class Model_DbTable_InfoSearch extends App_Db_Table {


    
    
    protected $_name = 'info_search';

    
    /**
     * Instancja klasy.
     * 
     * @var Model_DbTable_InfoSearch
     */
    static private $_oInstance;
    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_DbTable_InfoSearch
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

   
    
    public function getLast($iLimit = self::MAIN_PAGE_SEARCH_ELEMENTS)
    {
        
        return $this->select()
                    ->order('id DESC')
                    ->limit($iLimit)
                    ->query()
                   ->fetchAll(Zend_Db::FETCH_ASSOC); 
        
    }

    public function random($iNum)
    {
      return  $this->getAdapter()->query('CALL getRandomRecords('.$iNum.');')->fetchAll(Zend_Db::FETCH_ASSOC);
    }

   public function similiar($sName)
    {
        return $this->getAdapter()
                    ->query("SELECT name, url FROM search  WHERE MATCH(name) AGAINST ('".$sName."') ORDER BY MATCH(name) AGAINST ('".$sName."') LIMIT 10")
                    ->fetchAll(Zend_Db::FETCH_ASSOC);


    }

}

