<?php

class Model_DbTable_CategoryTwoThree extends App_Db_Table {

    protected $_name = 'category_two_three';
    protected $_cache = true; 


    /**
     * Instancja klasy.
     * 
     * @var Model_DbTable_CategoryTwoThree
     */
    static private $_oInstance;
    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_DbTable_CategoryTwoThree
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

 
    
    public function add($iTwo, $iThree) {
       return $this->insert(array('id_category_two' => $iTwo , 'id_category_three' => $iThree));
    }

 
    
}

