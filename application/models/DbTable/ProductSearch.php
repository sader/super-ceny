<?php

class Model_DbTable_ProductSearch extends App_Db_Table {


    const MAIN_PAGE_SEARCH_ELEMENTS = 40; 
    
    protected $_name = 'product_search';

    
    /**
     * Instancja klasy.
     * 
     * @var Model_DbTable_ProductSearch
     */
    static private $_oInstance;
    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_DbTable_ProductSearch
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

   
    
    public function getLast($iLimit = self::MAIN_PAGE_SEARCH_ELEMENTS)
    {
        
        return $this->select()
                    ->order('id DESC')
                    ->limit($iLimit)
                    ->query()
                   ->fetchAll(Zend_Db::FETCH_ASSOC); 
        
    }

    public function random($iNum)
    {
      return  $this->getAdapter()->query('CALL getRandomRecords('.$iNum.');')->fetchAll(Zend_Db::FETCH_ASSOC);
    }

   public function similiar($sName, $iLimit, $iOffset)
    {
       
       return $this->getAdapter()->query("SELECT p.*, ps.score
        FROM product AS p INNER JOIN
     (SELECT id, MATCH(name) AGAINST ('$sName') AS score 
       FROM product_search WHERE MATCH(name) AGAINST ('$sName') LIMIT $iLimit, $iOffset) "
               . "as ps ON p.id = ps.id ORDER BY ps.score DESC")->fetchAll(Zend_Db::FETCH_ASSOC); 
    }
    
    
    public function getNameByUrl($sUrl)
    {
        return $this->select()->from($this->_name, array('name'))->where('url = ?' , $sUrl)->query()->fetchColumn(); 
    }

}

