<?php

class Model_DbTable_Filter extends App_Db_Table {

    protected $_name = 'filters';
    protected $_cache = false;

    /**
     * Instancja klasy.
     * 
     * @var Model_DbTable_Filter
     */
    static private $_oInstance;

    // ------------------------------------------------------------------------

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_DbTable_Filter
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function getByCategoryThree($iId) {
        return $this->select()
                        ->setIntegrityCheck(false)
                        ->setCacheEnable(true)
                        ->from(array('f' => $this->_name))
                        ->join(array('p' => 'parameters'), 'p.id_filter=f.id', array('param_name' => 'name', 'param_id' => 'id'))
                        // ->columns('(SELECT GROUP_CONCAT(id_product) from product_parameter where product_parameter.id_parameter=p.id) as products')
                        
                        ->where('f.id_category_three = ?', $iId)
                        ->order(new Zend_Db_Expr('f.sort ASC, p.sort ASC'))
                        ->query()
                        ->fetchAll(Zend_Db::FETCH_ASSOC);
    }

    public function getProducent($iId) {
        return $this->select()->setIntegrityCheck(false)->from(array('f' => $this->_name), array('filter_id'  => 'id'))
                        ->join(array('p' => 'parameters'), 'p.id_filter=f.id', array('id', 'name'))
                        ->where('f.name = ?', 'Producent')
                        ->where('f.id_category_three = ?', $iId)
                        ->query()
                        ->fetchAll(Zend_Db::FETCH_ASSOC);
    }
    
     public function getProducentByCategory($iId) {
        return $this->select()->setIntegrityCheck(false)->from(array('f' => $this->_name), array('filter_id'  => 'id'))
                        ->join(array('p' => 'parameters'), 'p.id_filter=f.id', array('param_id' => 'id', 'param_name' => 'name'))
                        ->where('f.name = ?', 'Producent')
                        ->where('f.id_category_three = ?', $iId)
                        ->query()
                        ->fetchAll(Zend_Db::FETCH_ASSOC);
    }


       public function getProducentMultiple($aIds) {
        return $this->select()->setIntegrityCheck(false)->from(array('f' => $this->_name), array('filter_id' => 'id'))
                        ->join(array('p' => 'parameters'), 'p.id_filter=f.id', array('id', 'name'))
                        ->where('f.name = ?', 'Producent')
                        ->where('f.id_category_three IN(?)', $aIds)
                        ->query()
                        ->fetchAll(Zend_Db::FETCH_ASSOC);
    }
    
       public function getFiltersMultiple($aIds) {
        return $this->select()->setIntegrityCheck(false)->from(array('f' => $this->_name), array('filter_id' => 'id'))
                        ->join(array('p' => 'parameters'), 'p.id_filter=f.id', array('id', 'name'))
                        ->where('f.id_category_three IN(?)', $aIds)
                        ->query()
                        ->fetchAll(Zend_Db::FETCH_ASSOC);
    }
    


    public function getProducentForMeta($iId) {
        return $this->select()
                       // ->setCache(Model_Logic_Filter::getInstance()->getCache('meta'))
                        ->setIntegrityCheck(false)
                        ->from(array('f' => $this->_name), null)
                        ->join(array('p' => 'parameters'), 'p.id_filter=f.id', array('name'))
                        ->where('f.name = ?', 'Producent')
                        ->where('f.id_category_three IN (SELECT id_category_three from category_two_three where id_category_two = ?)', $iId)
                        ->query()
                        ->fetchAll(Zend_Db::FETCH_COLUMN, 0);
    }


    public function getProducentsForGroup($aIds) {
        return $this->select()
                        ->setIntegrityCheck(false)
                        ->from(array('f' => $this->_name))
                        ->join(array('p' => 'parameters'), 'p.id_filter=f.id', array('param_name' => 'name', 'param_id' => 'id'))
                        ->columns('(SELECT GROUP_CONCAT(id_product) from product_parameter where product_parameter.id_parameter=p.id AND p.id IN('.  implode(',', $aIds).')) as products')
                        ->where('f.name = ?' , 'Producent')
                        ->order('p.name')
                        ->query()
                        ->fetchAll(Zend_Db::FETCH_ASSOC);
    }



    public function getProducentForSearch($aIds) {
        return $this->select()
                        ->setIntegrityCheck(false)
                        ->from(array('f' => $this->_name))
                        ->join(array('p' => 'parameters'), 'p.id_filter=f.id', array('param_name' => 'name', 'param_id' => 'id'))
                        ->columns('(SELECT GROUP_CONCAT(id_product) from product_parameter where product_parameter.id_parameter=p.id AND product_parameter.id_product IN('.  implode(',', $aIds).')) as products')
                        ->where('f.name = ?' , 'Producent')
                        ->where('p.id IN(SELECT parameters.id from parameters JOIN product_parameter ON product_parameter.id_parameter=parameters.id WHERE product_parameter.id_product IN('.  implode(',', $aIds).'))')
                        ->query()
                        ->fetchAll(Zend_Db::FETCH_ASSOC);
    }
    
    public function getActualProducent($iProductId)
    {
        return $this->select()->setIntegrityCheck(false)
                              ->from(array('p' => 'parameters'), array('param_id' => 'id' , 'param_name' => 'name'))
                              ->join(array('f' => 'filters') , 'p.id_filter=f.id', array('filter_id' => 'id' , 'filter_name' => 'name'))
                              ->join(array('pp' => 'product_parameter') , 'p.id=pp.id_parameter', null)
                              ->where('pp.id_product  = ?', $iProductId)
                              ->where('f.name = ?' , 'Producent')
                              ->query()
                              ->fetch(Zend_Db::FETCH_ASSOC); 
    }
    
    
    public function  getFilterWithParams($aParams)
    {
       $oQ = $this->select()->setIntegrityCheck(false)
                              ->from(array('p' => 'parameters'), array('param_id' => 'id' , 'link'))
                              ->join(array('f' => 'filters') , 'p.id_filter=f.id', array('filter_id' => 'id' , 'filter_name' => 'name'))
                              ->where('CONCAT_WS("-", f.id , p.id) IN (?)' , $aParams);
        
        return $oQ->query()
                  ->fetchAll(Zend_Db::FETCH_ASSOC); 
    }
    

}