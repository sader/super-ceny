<?php

class Model_DbTable_ProductDescription extends App_Db_Table {

    protected $_name = 'product_description';




    /**
     * Instancja klasy.
     * 
     * @var Model_DbTable_ProductDescription 
     */
    static private $_oInstance;
    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_DbTable_ProductDescription 
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }
    
    public function getJobs($iJobs = 10)
    {
        
        return $this->select()
                    ->where('description = ? ' , '')
                    ->limit($iJobs)
                    ->query()
                    ->fetchall(Zend_Db::FETCH_ASSOC); 
    }

    


}

