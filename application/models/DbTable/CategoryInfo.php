<?php

class Model_DbTable_CategoryInfo extends App_Db_Table {

    protected $_name = 'info_category';
    protected $_cache = false;

    const JOB_FINISHED = 1;
    const JOB_UNFINISHED = 0;

    /**
     * Instancja klasy.
     * 
     * @var Model_DbTable_CategoryInfo
     */
    static private $_oInstance;

    // ------------------------------------------------------------------------

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_DbTable_CategoryInfo
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function getJob() {
        return $this->select()
                        ->where('description IS NULL')
                        ->limit(1)
                        ->query()
                        ->fetch(Zend_Db::FETCH_ASSOC);
    }

    public function jobDone($iId) {

        $this->update(array('job' => self::JOB_FINISHED), array('id = ?' => $iId));
    }
    
    public function add($aData)
    {
       return $this->insert($aData);
    }
    
    public function getByUrl($sUrl)
    {
        return $this->select()->where('seo = ? ' , $sUrl)->query()->fetch(Zend_Db::FETCH_ASSOC);
    }
    
    
     public function getBySimiliarName($sName)
    {
        return $this->getAdapter()
                    ->query('SELECT * FROM info_category WHERE MATCH (name) AGAINST ("'.$sName.'")')
                    ->fetch(Zend_Db::FETCH_ASSOC); 
    }
 



}

