<?php

class Model_DbTable_Parameter extends App_Db_Table {

    protected $_name = 'parameters';
    protected $_cache = false;

  

    /**
     * Instancja klasy.
     * 
     * @var Model_DbTable_Parameter
     */
    static private $_oInstance;

    // ------------------------------------------------------------------------

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_DbTable_Parameter
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function getJobs($iLimit = 5)
    {

        return $this->select()
                    ->where('job = ?' , 0)
                    ->limit($iLimit)
                    ->query()
                    ->fetchAll(Zend_Db::FETCH_ASSOC);

    }


    public function getByIds($aIds)
    {
        return $this->select()->setIntegrityCheck(false)
                              ->from(array('p' => $this->_name), array('param_name' => 'name'))
                              ->join(array('f' => 'filters' , 'f.id=p.id_filter' , array('filter_name' => 'name')))
                              ->where('p.id IN(?)' , $aIds)
                              ->query()
                              ->fetchAll(Zend_Db::FETCH_ASSOC);
    }
    
    public function getNamesWithCategoryThreeName()
    {
        return $this->select()->setIntegrityCheck(false)
                    ->from(array('p' => 'parameters'), array('name' => new Zend_Db_Expr('CONCAT_WS(" ", c.name, p.name)')))
                    ->join(array('f' => 'filters'), 'f.id=p.id_filter' , null)
                    ->join(array('c' => 'category_three'), 'c.id=f.id_category_three' , null)
                    ->query()
                    ->fetchAll(Zend_Db::FETCH_ASSOC);
        
        
        
    }
    
      public function getParamNames($aIds)
    {
        return $this->select()->setIntegrityCheck(false)
                    ->from(array('p' => 'parameters'), array('parameter' => 'p.name'))
                    ->join(array('f' => 'filters'), 'f.id=p.id_filter' , array('filter' => 'f.name'))
                    ->where('p.id IN(?)' , $aIds)
                    ->query()
                    ->fetchAll(Zend_Db::FETCH_ASSOC);
        
        
        
    }


}

