<?php

class Model_DbTable_GoogleNames extends App_Db_Table {

    protected $_name = 'google_names';
    protected $_cache = false;

    const JOB_FINISHED = 1;
    const JOB_UNFINISHED = 0;

    /**
     * Instancja klasy.
     * 
     * @var Model_DbTable_CategoryInfo
     */
    static private $_oInstance;

    // ------------------------------------------------------------------------

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_DbTable_CategoryInfo
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }
    
    public function getJobs($iLimit = 10)
    {
        return $this->select()
                    ->where('done = ?' , 0)
                    ->limit($iLimit)
                    ->query()
                    ->fetchAll(Zend_Db::FETCH_ASSOC); 
    }




}

