<?php

class Model_DbTable_Product extends App_Db_Table {

    protected $_name = 'product';

    const JOB_FINISHED = 1;
    const JOB_UNFINISHED = 0;

    
    protected $_cache = true; 

    /**
     * Instancja klasy.
     * 
     * @var Model_DbTable_Product
     */
    static private $_oInstance;

// ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_DbTable_Product
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function add($aData) {
        return $this->insert($aData);
    }
    
    public function getAll($iLimit, $iOffset)
    {
        
        return $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('p' => $this->_name), array('id', 'product' => 'CONCAT_WS(" ", ct.name , ct.name2, p.name)'))
                    ->join(array('pct' => 'product_category_three') , 'p.id=pct.id_product', null)
                    ->join(array('ct' => 'category_three'), 'ct.id=pct.id_category_three', null) 
                    ->limit($iLimit, $iOffset)
                    ->query()
                    ->fetchAll(Zend_Db::FETCH_ASSOC); 
        
        
    }


    
    public function getSimiliarProductsBySameParams($iId)
    {
        return $this->select()->from(array('p' => $this->_name))
                        ->join(array('pp' => 'product_parameter'), 'pp.id_product=p.id' , null)
                        ->where('pp.id IN(?)' , 'SELECT id_parameter from product_parameters where id_product = "'.$iId.'"')
                        ->query()
                        ->fetchAll(Zend_Db::FETCH_ASSOC); 
        
        
    }
    
    public function getMostPopularProductsByCategory($iCategoryId, $iLimit = 50)
    {
        return $this->select()->setIntegrityCheck(false)->from(array('p' => $this->_name), array('id' , 'id_category' => 'CONCAT("'.$iCategoryId.'")'))
                            ->join(array('pc' => 'product_category_three') , 'pc.id_product=p.id', null)
                            ->where('pc.id_category_three  = ?' , $iCategoryId)
                            ->order('p.offers DESC')
                            ->limit($iLimit)
                            ->query()
                            ->fetchAll(zend_Db::FETCH_ASSOC); 
        
        
    }


    public function getByCategoryThree($iCategoryId, $iLimit = 100) {
        return $this->select()
                        ->setIntegrityCheck(false)
                        ->distinct()
                        ->from(array('p' => $this->_name))
                        ->join(array('pct' => 'product_category_three'), 'pct.id_product=p.id', null)
                        ->where('pct.id_category_three = ?', $iCategoryId)
                        ->order('p.offers DESC')
                        ->limit($iLimit)
                        ->query()
                        ->fetchAll(Zend_Db::FETCH_ASSOC);
    }

    public function getByCategoryThreeSelect($iCategoryId, $aIds= array(), $aLocalParams = array()) {

        $oSelect = $this->select()
                ->setIntegrityCheck(false)
                ->distinct()
                ->from(array('p' => $this->_name)); 
                
        
           if(empty($aIds))
           {
             
               $oSubselect = $this->select()->setIntegrityCheck(false)->from('product_category_three' , array('id_product'))
                            ->where('product_category_three.id_category_three = ?' , $iCategoryId);
               
               
               $oSelect->join(array('t1' => $oSubselect), 't1.id_product=p.id' , null);
         
           }
           else
           {
               $oSelect->where('p.id IN(?)' , $aIds);
               
           }
                
        

      if (!empty($aLocalParams['filters']['pf'])) {
            $oSelect->where('p.min_price >= ?', $aLocalParams['filters']['pf']);
            unset($aLocalParams['filters']['pf']);
        }

        if (!empty($aLocalParams['filters']['pt'])) {
            $oSelect->where('p.min_price <= ?', $aLocalParams['filters']['pt']);
            unset($aLocalParams['filters']['pt']);
        }
        
        
        
        return $oSelect->order('offers DESC');
    }


        public function getPromotedByCategoryThree($iCategoryId, $aParams = array(), $iLimit = 100) {

        $oSelect = $this->select()
                ->setIntegrityCheck(false)
                ->distinct()
                ->from(array('p' => $this->_name))
                ->where('p.id IN(SELECT id from product join product_category_three as pct on pct.id_product=p.id where pct.id_category_three = ? order by offers DESC)', $iCategoryId);


        if (!empty($aParams['price']['from'])) {
            $oSelect->where('p.min_price >= ?', $aParams['price']['from']);
        }

        if (!empty($aParams['price']['to'])) {
            $oSelect->where('p.min_price <= ?', $aParams['price']['to']);
        }

        if (!empty($aParams['producent'])) {
            $iProducentId = Model_Logic_Filter::getInstance()->getProducentParamId($iCategoryId, $aParams['producent']);
            if ($iProducentId !== false) {
                $aParams['params']['prod'] = $iProducentId;
            }
        }

      
        switch ($aParams['sort']) {
            case 'c':
                $oSelect->order('min_price ASC');
                break;
            case 'ocena':
                $oSelect->order('offers DESC');
                break;
            case 'najnowsze':
                $oSelect->order('id DESC');
                break;
            default:
                $oSelect->order('id DESC');
                break;
        }

        $oSelect->limit($iLimit); 

        return $oSelect;
    }


    public function getForLucene($iOffset, $iLimit) {


        $oCategory = $this->select()->setIntegrityCheck(false)
                                    ->from(array('c' => 'category_three'), array('category' => new Zend_Db_Expr('GROUP_CONCAT(CONCAT_WS("|" , c.id, c.name, c.url) SEPARATOR "||")')))
                                    ->join(array('pc' => 'product_category_three'), 'pc.id_category_three=c.id', null)
                                    ->where('pc.id_product=p.id');
                                    // ->group('pc.id_category_three');
        
        
        
        $oParameters = $this->select()->setIntegrityCheck(false)
                                    ->from(array('pm' => 'parameters'), array('parameters' => 'GROUP_CONCAT(name SEPARATOR " ")'))
                                    ->join(array('pp' => 'product_parameter'), 'pp.id_parameter=pm.id'  , null)
                                    ->where('pp.id_product=p.id')
                                    ->group('pp.id_product');
     
        $aResults =  $this->select()->setIntegrityCheck(false)
                        ->from(array('p' => $this->_name), array('id', 'name'))
                        ->columns('(' . $oCategory->assemble() . ') as category')
                        ->columns('(' . $oParameters->assemble() . ') as parameters')
                           ->limit($iLimit, $iOffset)
                           ->query()
                           ->fetchAll(Zend_Db::FETCH_ASSOC);
        
        return $aResults; 
        
    }

    public function getByCategoryTwoSelect($aCategoriesId, $aParams = array()) {
        
        
        $oSelect = $this->select()
                ->setIntegrityCheck(false)
                ->distinct()
                ->from(array('p' => $this->_name))
                ->join(array('pct' => 'product_category_three'), 'pct.id_product=p.id', null)
                ->where('pct.id_category_three IN(?)', $aCategoriesId);


        if (!empty($aParams['filters']['pf'])) {
            $oSelect->where('p.min_price >= ?', $aParams['filters']['pf']);
        }

        if (!empty($aParams['filters']['pt'])) {
            $oSelect->where('p.min_price <= ?', $aParams['filters']['pt']);
        }

        return  $oSelect->order('offers DESC');
    }

    public function search($aIds, $aParams = array()) {
        
        
        
        $oSelect = $this->select()
                ->setIntegrityCheck(false)
                ->distinct()
                ->from(array('p' => $this->_name))
                ->columns('(SELECT GROUP_CONCAT(name SEPARATOR " ") from category_three ct join product_category_three as pc on pc.id_category_three=ct.id where pc.id_product = p.id) as categories')
                
                ->where('p.id IN(?)', $aIds);


        if (!empty($aParams['filters']['pf'])) {
            $oSelect->where('p.min_price >= ?', $aParams['filters']['pf']);
        }

        if (!empty($aParams['filters']['pt'])) {
            $oSelect->where('p.min_price <= ?', $aParams['filters']['pt']);
        }

      return $oSelect; 
                 // return $oSelect->order('FIELD(id, '.implode(',' , $aIds).')');
    }

    public function getByPromotedCategory() {
        return $this->select()->setIntegrityCheck(false)
                        ->from(array('pct' => 'product_category_three'), array('id' => 'id_product'))
                        ->join(array('ct' => 'category_three'), 'ct.id=pct.id_category_three', null)
                        ->where('ct.promoted = ?', Model_DbTable_CategoryThree::CATEGORY_IS_PROMOTED)
                        ->query()
                        ->fetchAll(Zend_Db::FETCH_COLUMN, 0);
    }

    public function getByIds($aIds) {
        return $this->select()->where('id IN(?)', $aIds)->query()->fetchAll(Zend_Db::FETCH_ASSOC);
    }


     public function getById($iId) {
        return $this->select()->where('id = ?', $iId)->query()->fetch(Zend_Db::FETCH_ASSOC);
    }

    
      public function getDetailedById($iId) {
        return $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('p' => $this->_name))
                    ->joinLeft(array('pd' => 'product_description'), 'pd.id=p.id' , array('description'))
                    ->where('p.id = ?', $iId)
                    ->query()
                    ->fetch(Zend_Db::FETCH_ASSOC);
    }
    
    
    public function getIdsFromStatement(Zend_Db_Select $oStatement) {
        return $oStatement->query()->fetchAll(Zend_Db::FETCH_COLUMN, 0);
    }


     public function getCategoryThreeForProduct($iId)
    {
        return $this->select()
                            ->setIntegrityCheck(false)
                            ->distinct()
                            ->from(array('cth' => 'category_three'), array('three_id' => 'id' ,  'three_name' => 'cth.name', 'three_url' =>  'cth.url'))
                            ->join(array('ctw' => 'category_two_three') , 'ctw.id_category_three=cth.id', null)
                            ->join(array('ct' => 'category_two') , 'ctw.id_category_two=ct.id' , array('two_name' => 'name' , 'two_url' => 'url', 'two_id' => 'id'))
                            ->join(array('co' => 'category_one') , 'ct.id_category_one=co.id' , array('one_name' => 'co.name', 'one_url' => 'seo' , 'one_id' => 'id'))
                            ->join(array('pct' => 'product_category_three') , 'pct.id_category_three=cth.id' , null)
                            ->where('pct.id_product = ?' , $iId)
                            ->query()
                            ->fetch(Zend_Db::FETCH_ASSOC);
    }
}