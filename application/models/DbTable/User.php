<?php

class Model_DbTable_User extends Zend_Db_Table_Abstract {

    protected $_name = 'user';

    /**
     * Instancja klasy.
     * 
     * @var Model_DbTable_User
     */
    static private $_oInstance;
    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_DbTable_User
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function addUser($aData) {
        $aData['created'] = date('Y-m-d H:i:s');
        $this->insert($aData);
    }

    public function getList() {
        return $this->select()
                        ->from($this->_name, array('id', 'login', 'user' => 'CONCAT_WS(" ", surname, name)', 'email', 'city', 'postcode', 'newsletter'));
    }

    public function editUser($aData, $iUserId) {
        $this->update($aData, array('id = ?' => $iUserId));
    }

    public function getById($iId) {
        return $this->select()->where('id = ?', $iId)->query()->fetch(Zend_Db::FETCH_ASSOC);
    }

    public function getUserByEmail($sEmail) {
        return $this->select()->where('email = ?', $sEmail)->query()->fetch(Zend_Db::FETCH_ASSOC);
    }

    public function changePassword($sPassword, $iUserId) {
        $this->update(array('password' => $sPassword), array('id = ?' => $iUserId));
    }



    public function getListOfFavorites($aCategories, $sSort = 'najlepsze', $aFilters = array()) {
        $oSelect = $this->select()->setIntegrityCheck(false)->from(array('uf' => $this->_name))
                ->join(array('p' => 'product'), 'p.id=uf.id_product')
                ->columns('(SELECT COUNT(*) from product_shop where product_shop.id_product = p.id) as shops')
                ->columns('(SELECT price from product_shop where product_shop.id_product = p.id order by price ASC LIMIT 1) as price')
                ->columns('(SELECT actual from product_raty where product_raty.id_product = p.id) as raty')
                ->columns('(SELECT COUNT(id) from product_opinion where product_opinion.id_product = p.id) as opinions')
                ->where('uf.id_user = ?', $iUserId)
                ->having('shops > ?', 0);
    }
    
    public function getEmailForNewsletter()
    {
        return $this->select()->from($this->_name, array('email'))->where('newsletter = ?' , 1)->query()->fetchAll(); 
    }
    
    

}

