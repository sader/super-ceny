<?php

class Model_DbTable_CategoryOne extends App_Db_Table {

    protected $_name = 'category_one';
    protected $_cache = true;

    const JOB_FINISHED = 1;
    const JOB_UNFINISHED = 0;

    /**
     * Instancja klasy.
     * 
     * @var Model_DbTable_CategoryOne
     */
    static private $_oInstance;

    // ------------------------------------------------------------------------

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_DbTable_CategoryOne
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function getJob() {
        return $this->select()
                        ->where('job = ?', self::JOB_UNFINISHED)
                        ->query()
                        ->fetch(Zend_Db::FETCH_ASSOC);
    }

    public function jobDone($iId) {

        $this->update(array('job' => self::JOB_FINISHED), array('id = ?' => $iId));
    }

    public function getList()
    {
        return $this->select()->order('name'); 
    }

    public function getByUrl($sUrl)
    {
        return $this->select()->setCacheEnable(true)->where('seo = ?' , $sUrl)->query()->fetch(Zend_Db::FETCH_ASSOC); 
    }

    public function getListGrid()
    {
        return $this->select()
                            ->setIntegrityCheck(false)
                            ->distinct()
                            ->setCacheEnable(true)
                            ->from(array('cth' => 'category_three'), array('cth.name' , 'cth.id', 'cth.url'))
                            ->join(array('ctw' => 'category_two_three') , 'ctw.id_category_three=cth.id', null)
                            ->join(array('ct' => 'category_two') , 'ctw.id_category_two=ct.id' , null)
                            ->join(array('co' => 'category_one') , 'ct.id_category_one=co.id' , array('main_id' => 'co.id' , 'main_name' => 'co.name', 'main_seo' => 'co.seo'))
                            ->where('cth.promoted = ?' , 1)
                            ->query()
                            ->fetchAll(Zend_Db::FETCH_ASSOC);
    }

      public function setDescription($iId, $sDescription)
    {
        $this->update(array('description' => $sDescription), array('id = ?' => $iId));
    }
    
    public function getNames()
    {
        
        return $this->select()->from($this->_name, array('name'))->query()->fetchAll(Zend_Db::FETCH_ASSOC); 
    }

    
    
    public function getBundleCategories($iLimit)
    {
        $aOne = $this->select()->setIntegrityCheck(false)->from('category_one' , array('id', 'name', 'seo', 'type' => 'CONCAT("one")'))->where("description = ''"); 
        $aTwo = $this->select()->setIntegrityCheck(false)->from('category_two' , array('id', 'name', 'seo' => 'url', 'type' => 'CONCAT("two")'))->where("description = ''"); 
        $aThree = $this->select()->setIntegrityCheck(false)->from('category_three' , array('id', 'name', 'seo' => 'url', 'type' => 'CONCAT("three")'))->where("description = ''"); 
        
        return $this->select()->setIntegrityCheck(false)->union(array($aOne, $aTwo, $aThree))->limit($iLimit)->query()->fetchAll(Zend_Db::FETCH_ASSOC); 
        
    }
    
    
       public function fullCategorySearchByUrl($sUrl)
    {
        $aOne = $this->select()->setIntegrityCheck(false)->from('category_one' , array('seo', 'type' => 'CONCAT("1")'))->where('category_one.seo = ?' , $sUrl); 
        $aTwo = $this->select()->setIntegrityCheck(false)->from('category_two' , array('seo' => 'url', 'type' => 'CONCAT("2")'))->where('category_two.url = ?' , $sUrl); 
        $aThree = $this->select()->setIntegrityCheck(false)->from('category_three' , array('seo' => 'url', 'type' => 'CONCAT("3")'))->where('category_three.url = ?' , $sUrl);
        
        return $this->select()->setIntegrityCheck(false)
                            ->union(array($aOne, $aTwo, $aThree))
                            ->query()
                            ->fetch(Zend_Db::FETCH_ASSOC); 
        
    }
    


}

