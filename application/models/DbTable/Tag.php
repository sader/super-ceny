<?php

class Model_DbTable_Tag extends Zend_Db_Table_Abstract {

    protected $_name = 'tags';

    /**
     * Instancja klasy.
     * 
     * @var Model_DbTable_Tag
     */
    static private $_oInstance;
    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_DbTable_Tag
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }
    
    public function add($aData)
    {
        $aData['type'] = implode(',', $aData['type']); 
                
                $this->insert($aData);
                
    }
    
    
    public function edit($aData , $iId)
    {
        $aData['type'] = implode(',', $aData['type']); 
                
                $this->update($aData, array('id = ?' => $iId));
                
    }
    
    public function getById($iId)
    {
        return $this->select()->where('id = ?' , $iId)->query()->fetch(Zend_Db::FETCH_ASSOC); 
    }


    
    public function remove($iId)
    {
        $this->delete(array('id = ?' => $iId)); 
    }
    
    
    public function getList()
    {
        return $this->select()->from($this->_name); 
    }
    
    public function getByNameAndType($sTag, $iType)
    {
        
        return $this->select()->where('name = ?' , $sTag)
                                ->where('FIND_IN_SET("'.$iType.'", type)')
                                ->query()
                                ->fetch(Zend_Db::FETCH_ASSOC);
        
    }
   
  
    
}