<?php

class Model_DbTable_ProductParameter extends App_Db_Table {

    protected $_name = 'product_parameter';




    /**
     * Instancja klasy.
     * 
     * @var Model_DbTable_ProductParameter
     */
    static private $_oInstance;
    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_DbTable_ProductParameter
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    

    public function add($idProduct, $idCategory) {
        $this->insert(array('id_product' => $idProduct, 'id_category_three' => $idCategory));
    }

    public function getByProductId($iId)
    {
        return $this->select()
                     ->from($this->_name, array('id_parameter'))
                     ->where('id_product = ?', $iId)
                     ->query()
                     ->fetchAll(Zend_Db::FETCH_COLUMN, 0);
    }


    public function getByParameterId($iId)
    {
        return $this->select()
                     ->from($this->_name, array('id_product'))
                     ->where('id_parameter = ?', $iId)
                     ->query()
                     ->fetchAll(Zend_Db::FETCH_COLUMN, 0);
    }





    public function isProductCategoryExist($iProduct, $iCategory)
    {
        $aBind = $this->select()
                       ->where('id_product = ?' , $iProduct)
                       ->where('id_category_three = ?', $iCategory)
                       ->query()
                       ->fetch(Zend_Db::FETCH_NUM);
        
        return !empty($aBind); 
    }

    public function getParameterNamesForProduct($iId)
    {
        return $this->select()->setIntegrityCheck(false)->from(array('pp' => $this->_name), 'GROUP_CONCAT(p.name  SEPARATOR " ")')
                    ->join(array('p' => 'parameters'), 'p.id=pp.id_parameter' , null)
                    ->query()->fetchColumn(); 

    }

}

