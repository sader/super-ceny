<?php

class Model_DbTable_News extends App_Db_Table {

    protected $_name = 'news';
    protected $_cache = true; 

    /**
     * Instancja klasy.
     * 
     * @var Model_DbTable_News
     */
    static private $_oInstance;
    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_DbTable_News
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }
    
    /**
     * Lista eventów
     * @return Zend_Db_Select 
     */
    public function getListQuery()
    {
        return $this->select(); 
    }
    
    /**
     * Dodaj event
     * @param array $aEvent
     * @return integer (last_inserted_id) 
     */
    public function add($aEvent)
    {
        $aEvent['added'] = date('Y-m-d'); 
        return $this->insert($aEvent); 
    }
    
    public function edit($aEvent, $iEventId)
    {
        $this->update($aEvent, array('id = ?' => $iEventId)); 
    }


    
    /**
     * Znajdź po Id
     * @param integer $iId
     * @return array 
     */
    public function getById($iId)
    {
        return $this->select()
                    ->where('id = ?' , $iId)
                    ->query()
                    ->fetch(Zend_db::FETCH_ASSOC); 
    }
    
    public function remove($iId)
    {
        $this->delete(array('id = ?' => $iId)); 
    }
    
    public function get($iLimit)
    {
        return $this->select()
                    ->order('added DESC')
                    ->limit($iLimit)
                    ->query()
                    ->fetchAll(Zend_Db::FETCH_ASSOC); 
    }
    
}