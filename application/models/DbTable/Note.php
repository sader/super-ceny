<?php

class Model_DbTable_Note extends Zend_Db_Table_Abstract {

    protected $_name = 'product_opinion';

    /**
     * Instancja klasy.
     * 
     * @var Model_DbTable_Note
     */
    static private $_oInstance;
    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_DbTable_Note
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }
    
   
    public function addNote($aData)
    {
        $this->insert($aData); 
    }
    
    public function getUnaccepted()
    {
        return $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('po' => $this->_name))
                    ->join(array('p' => 'product') , 'p.id=po.id_product' , array('name'))
                    ->where('po.status = ?' , 0);
                
    }
    
    public function getByIdProduct($iId)
    {
        return $this->select()->where('id_product = ?' , $iId)->where('status = ?', 1)->order('date DESC'); 
        
    }
    
    public function getByIdUser($iId)
    {
        
        return $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('po' => $this->_name))
                    ->join(array('p' => 'product') , 'p.id=po.id_product' , array('name'))
                    ->where('po.id_user = ?' , $iId)
                    ->order('date DESC');
        
        
    }
    
}