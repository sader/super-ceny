<?php

class Model_DbTable_GoogleResults extends App_Db_Table {

    protected $_name = 'google_results';
    protected $_cache = false;

    const JOB_FINISHED = 1;
    const JOB_UNFINISHED = 0;

    /**
     * Instancja klasy.
     * 
     * @var Model_DbTable_GoogleResults
     */
    static private $_oInstance;

    // ------------------------------------------------------------------------

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_DbTable_GoogleResults
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }
    
     public function similiar($sName, $iLimit = 10)
    {
        return $this->getAdapter()
                    ->query("SELECT name FROM google_results  WHERE MATCH(name) AGAINST ('$sName') ORDER BY MATCH(name) AGAINST ('$sName') LIMIT $iLimit")
                    ->fetchAll(Zend_Db::FETCH_COLUMN);


    }

    public function getLast($iNum = 10) {
        
        return $this->select()
                    ->order('id DESC')
                    ->limit($iNum)
                    ->query()
                    ->fetchAll(Zend_Db::FETCH_ASSOC); 
        
    }
    
     public function random($iNum)
    {
      return  $this->getAdapter()->query('CALL getRandomRecords('.$iNum.');')->fetchAll(Zend_Db::FETCH_ASSOC);
    }


}

