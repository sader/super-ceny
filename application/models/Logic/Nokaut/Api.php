<?php

/**
 * Model_Logic_Api
 *
 * @author Krzysztof Janicki
 */
class Model_Logic_Nokaut_Api extends Model_Logic_Abstract {

    private $_sKey = '8a2f056dc4da9c477b2400fedf140af1';
    private $sUrl = 'http://api.nokaut.pl/?';
    private $sFormat = 'json';
    private $iProductLimit = 400;
    private $iFilterPopulate = 100;
    private $pid = '#pid=49423';

    /**
     * Instancja klasy.
     * 
     * @var Model_Logic_Nokaut_Api
     */
    static private $_oInstance;

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_Logic_Nokaut_Api
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }


    public function manageProductUrl(App_Controller_Page_Abstract $oCtrl)
    {
        $aReturn['url'] = Model_Logic_Nokaut_Parser::NOKAUT_URL; 

        $sName = $oCtrl->getRequest()->getParam('name', null);

        if(!empty($sName))
        {
         $sApiCall = sprintf('%sformat=%s&key=%s&method=nokaut.product.getByKeyword&keyword=%s&limit=1',
                                        $this->sUrl, $this->sFormat, $this->_sKey, urlencode($sName));

         $aData = $this->_call($sApiCall);

         if($aData['success'] && count($aData['success']['items'] > 0))
         {
            $aReturn['url'] = $aData['success']['items']['1item']['url'];
         }
        }

        $aReturn['url'] .= $this->pid; 

        return $aReturn;

    }


    public function manageCategories() {

        $sApiCall = sprintf('%sformat=%s&key=%s&method=nokaut.Category.getAll', $this->sUrl, $this->sFormat, $this->_sKey);

        

        foreach ($aData['success']['items'] as $aRow) {

            $aCategory = array(
                'id' => $aRow['id'],
                'name' => $aRow['name'],
                'url' => $aRow['url'],
                'seo' => $this->makeSeo($aRow['tree_name']),
                'parent_id' => $aRow['parent_id'],
                'tree' => $aRow['tree'],
                'tree_name' => $aRow['tree_name'],
                'tree_level' => $aRow['tree_level'],
                'is_leaf' => $aRow['is_leaf']
            );


            Model_DbTable_Category::getInstance()->add($aCategory);
        }

        echo 'Done';
    }

    public function manageProducts() {

        $aCategories = Model_DbTable_Category::getInstance()->getUnfinishedJobProducts(10);

        if (!empty($aCategories)) {
            foreach ($aCategories as $aCategory) {
                $sApiCall = sprintf('%sformat=%s&key=%s&method=nokaut.Product.getByCategory&category=%s&limit=%s', $this->sUrl, $this->sFormat, $this->_sKey, $aCategory['id'], $this->iProductLimit);

                $aData = $this->_call($sApiCall);
                 
                try {
                    Zend_Db_Table::getDefaultAdapter()->beginTransaction();

                    foreach ($aData['success']['items'] as $aRow) {

                        $aProduct = array(
                            'id' => $aRow['id'],
                            'name' => $aRow['name'],
                            'url' => $aRow['url']
                        );

                        if (!Model_DbTable_Product::getInstance()->isProductExist($aProduct['id'])) {
                            Model_DbTable_Product::getInstance()->add($aProduct);
                        }

                        if (!Model_DbTable_ProductCategory::getInstance()->isProductCategoryExist($aProduct['id'], $aCategory['id'])) {
                            Model_DbTable_ProductCategory::getInstance()->add($aProduct['id'], $aCategory['id']);
                        }
                    }

                    Model_DbTable_Category::getInstance()->jobDone($aCategory['id'], 'products');
                    Zend_Db_Table::getDefaultAdapter()->commit();
                } catch (Exception $e) {
                    Zend_Db_Table::getDefaultAdapter()->rollBack();
                    $this->getLog()->log($e, Zend_Log::CRIT);
                    echo $e->getMessage();
                }
            }
        }
        echo 'Done';
    }

    
    public function manageShopsByProductName($sName)
    {
                     $sApiCall = sprintf('%sformat=%s&key=%s&method=nokaut.Price.getByProductName&name=%s', 
                             $this->sUrl, $this->sFormat, $this->_sKey, urlencode($sName));

                       $aData = $this->_call($sApiCall);
                       
                       return isset($aData['success']['items']) ? $aData['success']['items'] : false; 
        
    }
    
    
    public function manageFiltersPopulate() {

        $aFilters = Model_DbTable_FilterParams::getInstance()->getUnfinishedJobs(30);

        if (!empty($aFilters)) {
            foreach ($aFilters as $aFilter) {


                $sApiCall = sprintf('%sformat=%s&key=%s&method=nokaut.Product.getByCategory&category=%s&limit=%s', $this->sUrl, $this->sFormat, $this->_sKey, $aFilter['id_category'], $this->iFilterPopulate);

                if ($aFilter['type'] == Model_DbTable_FilterParams::PARAM_TYPE_EQ) {
                    $sApiCall .= sprintf('&filters[filter_%s]=%s', $aFilter['id_remote'], urlencode($aFilter['eq']));
                } elseif ($aFilters['type'] == Model_DbTable_FilterParams::PARAM_TYPE_COMPARE) {
                    if ($aFilter['from'] !== null) {
                        $sApiCall .= sprintf('&filters[filter_%s][from]=%s', $aFilter['id_remote'], $aFilter['from']);
                    }

                    if ($aFilter['to'] !== null) {
                        $sApiCall .= sprintf('&filters[filter_%s][to]=%s', $aFilter['id_remote'], $aFilter['to']);
                    }
                }


                $aData = $this->_call($sApiCall);

                try {
                    Zend_Db_Table::getDefaultAdapter()->beginTransaction();

                    if (isset($aData['success'])) {

                        foreach ($aData['success']['items'] as $aRow) {

                            $aProduct = array(
                                'id' => $aRow['id'],
                                'name' => $aRow['name'],
                                'url' => $aRow['url']
                            );

                            if (!Model_DbTable_Product::getInstance()->isProductExist($aProduct['id'])) {
                                Model_DbTable_Product::getInstance()->add($aProduct);
                                Model_DbTable_ProductCategory::getInstance()->add($aProduct['id'], $aFilter['id_category']);
                            }

                            Model_DbTable_ProductParam::getInstance()->add($aProduct['id'], $aFilter['id_param']);
                        }

                        Model_DbTable_FilterParams::getInstance()->jobDone($aFilter['id_param']);
                    }
                    Zend_Db_Table::getDefaultAdapter()->commit();
                } catch (Exception $e) {
                    Zend_Db_Table::getDefaultAdapter()->rollBack();
                    $this->getLog()->log($e, Zend_Log::CRIT);
                    echo $e->getMessage();
                }
            }
        }
        echo 'Done';
    }

    public function manageFilters() {


        $aCategories = Model_DbTable_Category::getInstance()->getUnfinishedJobFilters(10);

        if (!empty($aCategories)) {
            foreach ($aCategories as $aCategory) {
                $sApiCall = sprintf('%sformat=%s&key=%s&method=nokaut.Product.getFilters&filters[category_id]=%s', $this->sUrl, $this->sFormat, $this->_sKey, $aCategory['id']);
                $aData = $this->_call($sApiCall);
                try {
                    if (isset($aData['success'])) {
                        Zend_Db_Table::getDefaultAdapter()->beginTransaction();

                        foreach ($aData['success'] as $sName => $aFilter) {
                            if (!in_array($sName, $this->getUnsupportedFilters())) {

                                $aFilterInsert = array(
                                    'id_filter' => $aFilter['id'],
                                    'id_category' => $aCategory['id'],
                                    'name' => $aFilter['title'],
                                    'type' => $aFilter['type'],
                                    'unit_short' => isset($aFilter['unit_short']) ? $aFilter['unit_short'] : ''
                                );

                                $iFilterId = Model_DbTable_Filter::getInstance()->add($aFilterInsert);

                                foreach ($aFilter['values'] as $mItem) {

                                    if ($aFilter['type'] == 'string_select') {

                                        $aItemInsert = array(
                                            'id_filter' => $iFilterId,
                                            'eq' => $mItem,
                                            'type' => Model_DbTable_FilterParams::PARAM_TYPE_EQ
                                        );
                                    } else {

                                        $aItemInsert = array(
                                            'id_filter' => $iFilterId,
                                            'from' => $mItem['from'],
                                            'to' => $mItem['to'],
                                            'type' => Model_DbTable_FilterParams::PARAM_TYPE_COMPARE
                                        );
                                    }
                                    Model_DbTable_FilterParams::getInstance()->add($aItemInsert);
                                }
                            }
                        }

                        Model_DbTable_Category::getInstance()->jobDone($aCategory['id'], 'filters');
                        Zend_Db_Table::getDefaultAdapter()->commit();
                    }
                } catch (Exception $e) {
                    Zend_Db_Table::getDefaultAdapter()->rollBack();
                    $this->getLog()->log($e, Zend_Log::CRIT);
                    echo $e->getMessage();
                }
            }
        }
    }
    
    public function managePriceUpdate()
    {
        $aShops = Model_DbTable_Shop::getInstance()
                ->select()->order('updated asc')
                 ->limit(1)->query()->fetchAll(Zend_Db::FETCH_ASSOC);
        
        foreach ($aShops as $aShop)
        {
            $iOffset = 1; 
            $iLimit  = 400; 
            $sEnd = false; 
           do {
               
                try
               {
              $sCall = 'http://api.nokaut.pl/?format=json&key=%s&method=nokaut.Price.getByShopId&shop_id=%s&limit=%s&offset=%s';
          
               $aData = $this->_call(sprintf($sCall , $this->_sKey, $aShop['id'], $iLimit, $iOffset));             
               
               if(isset($aData['success']) && count($aData['success']['items']) > 0)
               {    
                   foreach($aData['success']['items'] as $aItem)
                   {
                       Model_DbTable_ProductShop::getInstance()->updatePrice($aItem['shop_id'], $aItem['product_id'], $aItem['price']); 
                   }
               }
               
               if(!isset($aData['success']) || count($aData['success']['items']) != $iLimit)
               {
                   $sEnd = true; 
               }
               
               }catch(Exception $e)
               {
                   $this->getLog()->log($e, Zend_Log::CRIT); 
                   $sEnd = true; 
               }
               $iOffset += $iLimit;  
            }
            while ($sEnd !== true); 
            
            Model_DbTable_Shop::getInstance()->update(array('updated' => date('Y-m-d H:i:s')), array('id = ?' => $aShop['id'])); 
            
        }
        
        
    }


    
    private function _call($sCall) {

        $oClient = new Zend_Http_Client(null, array('timeout' => 30));
        $oClient->setUri($sCall);

        $oResponse = $oClient->request();

        if ($oResponse->isSuccessful()) {

            return Zend_Json::decode($oResponse->getBody());
        }
        else
            throw new Exception('API call failed', 500);
    }

    private function getUnsupportedFilters() {
        return array('filter_producers', 'filter_price', 'filter_category');
    }

    public function makeSeo($sTreeString) {
        $sTreeString = substr($sTreeString, 1, -1);

        $aRawPieces = explode('|', $sTreeString);

        $aPieces = array();
        foreach ($aRawPieces as $sRawPiece) {
            $aPieces[] = $this->seo($sRawPiece);
        }

        return implode('/', $aPieces);
    }

}