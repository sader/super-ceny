<?php

/**
 * Model_Logic_NoakutApi
 *
 * @author Krzysztof Janicki
 */
class Model_Logic_Nokaut_Parser extends Model_Logic_Abstract {
    const NOKAUT_URL = 'http://nokaut.pl/';

    /**
     * Instancja klasy.
     * 
     * @var Model_Logic_Nokaut_Parser 
     */
    static private $_oInstance;

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_Logic_Nokaut_Parser
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function manageProductData() {

        require_once('phpQuery.php');

        $aProducts = Model_DbTable_Product::getInstance()->getUnfinishedJob(25);

        foreach ($aProducts as $aSavedProduct) {
            try {
                
                Zend_Db_Table::getDefaultAdapter()->beginTransaction(); 
                
                $oData = $this->_call(trim($aSavedProduct['url']));
                
                
                $aProduct = array();


                $aProduct['url'] = $aSavedProduct['url'];
                $aProduct['producent'] = pq($oData)->find('#Groupproducer')->find('a')->eq(0)->text();
                $aProduct['descriptions'] = pq($oData)->find('.ProductDescriptions')->html();
                $aProduct['descriptions'] = preg_replace('/(<\/[^>]+?>)(<[^>\/][^>]*?>)/', '$1 $2', $aProduct['descriptions']);
                $aProduct['descriptions'] = trim(strip_tags($aProduct['descriptions']));
                $aProduct['description'] = pq($oData)->find('#FullDescription')->text();
                $aPhotosTags = pq($oData)->find('.GalleryThumbs')->find('.photo');
                $aProduct['images'] = 0;
                if (count($aPhotosTags) > 0) {
                    foreach ($aPhotosTags as $aPhotoTag) {


                        $sPhoto = pq($aPhotoTag)->parent()->attr('href');

                        if (($sImageBody = $this->_callImage($sPhoto)) !== false) {
                            $aProduct['images']++;
                            file_put_contents($this->imagePath($aSavedProduct['id'], true, true) . DIRECTORY_SEPARATOR . $aProduct['images'] . '.jpg', $sImageBody);
                        }
                    }
                }
                
                $aParamsObj = pq('#ProductDetailedTechData')->find('li');
                $aParams = array();

                if (!empty($aParamsObj)) {
                    foreach ($aParamsObj as $aParamObj) {

                        $aRows = pq($aParamObj)->find('tr');

                        foreach ($aRows as $aRow) {

                            $sParamKey = pq($aRow)->find('th')->text();
                            $sParamValue = pq($aRow)->find('td')->text();

                            if (!empty($sParamKey) && !empty($sParamValue) && $sParamKey != 'Producent') {
                                $aParams[pq($aRow)->find('th')->text()] = pq($aRow)->find('td')->text();
                            }
                        }
                    }
                }

                $aProduct['params'] = serialize($aParams);
                $aProduct['job_parse'] = Model_DbTable_Product::JOB_FINISHED;

                $aQueries = pq($oData)->find('#LastQueryBox')->find('a');

                
                    foreach ($aQueries as $aQuery) {
                        Model_DbTable_Search::getInstance()->add(pq($aQuery)->text());
                    }
                


                
                Zend_Db_Table::getDefaultAdapter()->commit();
                
            } catch (Exception $e) {

                Zend_Db_Table::getDefaultAdapter()->rollBack();
                Model_DbTable_Product::getInstance()->update(array('job_parse' => Model_DbTable_Product::JOB_FINISHED), array('id = ?' => $aSavedProduct['id']));
                $this->getLog()->log($e, Zend_Log::CRIT);
                echo $e->getMessage();
            }
        }
        
        echo 'done';
    }

    public function manageShopProducts() {


        require_once('phpQuery.php');

        $aProducts = Model_DbTable_Product::getInstance()->getUnfinishedShopsJob(25);

  
        
        foreach ($aProducts as $aSavedProduct) {
            try {
                Zend_Db_Table::getDefaultAdapter()->beginTransaction();
                $oData = $this->_callAjax($aSavedProduct['id']);

                $oShops = pq($oData)->find('.ProductOffersListElement');

                foreach ($oShops as $oShop) {

                    $iShopId = pq($oShop)->attr('data-shop_id');


                    if (!Model_DbTable_Shop::getInstance()->isShopExists($iShopId)) {

                        $aShop = array();
                        $aShop['id'] = $iShopId;
                        $aShop['name'] = pq($oShop)->attr('data-shop_name');
                        $aShop['nokaut_url'] = pq($oShop)->find('.ShopAbout')->eq(0)->find('a')->attr('href');

                        Model_DbTable_Shop::getInstance()->add($aShop);
                    }
                    $aShopProduct = array();
                    $aShopProduct['id_shop'] = $iShopId;
                    $aShopProduct['id_product'] = $aSavedProduct['id'];
                    $aShopProduct['name'] = pq($oShop)->attr('data-offer_name');
                    $aShopProduct['price'] = pq($oShop)->find('.PriceValue')->text();
                    $aShopProduct['price'] = str_replace('\szł', '', $aShopProduct['price']);
                    $aShopProduct['price'] = str_replace(',', '.', $aShopProduct['price']);
                    $aShopProduct['direct_link'] = preg_replace('/nokaut|nokaut\.pl/', '', pq($oShop)->attr('data-offer_url'));


                    Model_DbTable_ProductShop::getInstance()->add($aShopProduct);
                    
                }
                Model_DbTable_Product::getInstance()->finishJobShop($aSavedProduct['id']);
                Zend_Db_Table::getDefaultAdapter()->commit();
            } catch (Exception $e) {

                Zend_Db_Table::getDefaultAdapter()->rollBack();
                $this->getLog()->log($e, Zend_Log::CRIT);
                echo $e->getMessage();
                echo $e->getTraceAsString();
            }
        }




        echo 'Done';
    }

    public function populateShop() {


         require_once('phpQuery.php');

        $aShops = Model_DbTable_Shop::getInstance()->getUnfinishedJob(20);

        foreach ($aShops as $aSavedShop) {
            try {
                Zend_Db_Table::getDefaultAdapter()->beginTransaction();
                $oData = $this->_call(self::NOKAUT_URL . $aSavedShop['nokaut_url']);


                $aShop['thumb'] = pq($oData)->find('#ShopMainBox')->find('img')->attr('src');
                $aShop['score'] = pq($oData)->find('.average')->text();
                $aShop['description'] = pq($oData)->find('#ShopMainBox')->find('p')->text();
                $aShop['opineo_score'] = pq($oData)->find('.OpineoRatingStars')->find('span.rating')->text();
                $aShop['opineo_link'] = preg_replace('/\?ref.*/', '', pq('.OpineoRatingUrl')->attr('href'));
                $aShop['postcode'] = pq($oData)->find('span.postal-code')->text();
                $aShop['city'] = pq($oData)->find('span.locality')->text();
                $aShop['address'] = pq($oData)->find('span.street-address')->text();
                $aShop['url'] = pq($oData)->find('#specificationLeft')->find('th:contains(Strona www)')->parent()->find('td')->text();
                $aShop['email'] = pq($oData)->find('span.email')->text();
                $aShop['phone'] = pq($oData)->find('#specificationLeft')->find('th:contains(Telefon)')->parent()->find('td.tel')->find('span.value')->text();
                $aShop['fax'] = pq($oData)->find('#specificationLeft')->find('th:contains(Faks)')->parent()->find('td.tel')->find('span.value')->text();
                $aShop['job_details'] = Model_DbTable_Shop::JOB_FINISHED;
                
                Model_DbTable_Shop::getInstance()->update($aShop, array('id = ?' => $aSavedShop['id']));
                Zend_Db_Table::getDefaultAdapter()->commit();
            } catch (Exception $e) {

                Zend_Db_Table::getDefaultAdapter()->rollBack();
                $this->getLog()->log($e, Zend_Log::CRIT);
                echo $e->getMessage();
                echo $e->getTraceAsString();
            }
        }




        echo 'Done';
    }
    
    
    public function shopLocations()
    {
        
          require_once('phpQuery.php');

       $aShop = Model_DbTable_Shop::getInstance()->getUnfinishedLocationJob(20);

        foreach ($aShop as $aSavedShop) {
            try {
                Zend_Db_Table::getDefaultAdapter()->beginTransaction();
                $oData = $this->_call(self::NOKAUT_URL . $aSavedShop['nokaut_url'].'punkty-sprzedazy.html');
                
                $aLocations = pq($oData)->find('.pointAddressRegion'); 
                
                if(count($aLocations) > 0)
                {
                    foreach($aLocations as $aLocation)
                    {
                        $aData = array(); 
                        $aData['id_shop'] = $aSavedShop['id']; 
                        $aData['city'] = pq($aLocation)->find('strong')->eq(1)->text(); 
                        
                        $aX = explode("\n", pq($aLocation)->html()); 
                        $aParts = explode('<br>', $aX[3]);
                        $aData['address'] = trim($aParts[0]); 
                        $aData['phone']  = trim(strip_tags($aParts[1])); 
                        Model_DbTable_ShopLocations::getInstance()->add($aData); 
                    }
                    
                }
                
               Model_DbTable_Shop::getInstance()->update(array('job_locations' => 1), array('id = ?' => $aSavedShop['id']));
                Zend_Db_Table::getDefaultAdapter()->commit();
            } catch (Exception $e) {

                Zend_Db_Table::getDefaultAdapter()->rollBack();
                $this->getLog()->log($e, Zend_Log::CRIT);
                echo $e->getMessage();
                echo $e->getTraceAsString();
            }
        }




        echo 'Done';
        
        
    }

    private function _call($sCall) {

        $oClient = new App_Http_Client(null, array('timeout' => 30));
        $oClient->setUri($sCall);
        $oResponse = $oClient->request();

        if ($oResponse->isSuccessful()) {

            $htmlCode = mb_convert_encoding($oResponse->getBody(), 'HTML-ENTITIES', "UTF-8");

            return phpQuery::newDocumentHTML($htmlCode, 'utf-8');
        }
        else
            throw new Exception('API call failed', 500);
    }

    private function _callAjax($iProductId) {
        $oClient = new Zend_Http_Client(null, array('timeout' => 30));
        $oClient->setUri('http://www.nokaut.pl/Product/displayComponentUsingAjax/Product3PriceList/WholeProductOffersList');
        $oClient->setMethod(Zend_Http_Client::POST);
        $oClient->setHeaders('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        $oClient->setParameterPost('product_id', $iProductId);
        $oClient->setParameterPost('limit', 100);
        $oClient->setParameterPost('page', 1);
        $oResponse = $oClient->request();

        if ($oResponse->isSuccessful()) {
            $aResponse = json_decode($oResponse->getBody());
            
            if (isset($aResponse->response)) {

                $htmlCode = mb_convert_encoding($aResponse->response, 'HTML-ENTITIES', "UTF-8");
                return phpQuery::newDocumentHTML($htmlCode, 'utf-8');
            }
        }
        else
            throw new Exception('API call failed', 500);
    }

    private function _callImage($sImage) {

        $oClient = new Zend_Http_Client(null, array('timeout' => 30));
        $oResponse = $oClient->setUri($sImage)
                ->request(Zend_Http_Client::GET);
        return $oResponse->isSuccessful() ? $oResponse->getBody() : false;
    }

}