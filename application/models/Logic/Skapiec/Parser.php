<?php

/**
 * Model_Logic_NoakutApi
 *
 * @author Krzysztof Janicki
 */
class Model_Logic_Skapiec_Parser extends Model_Logic_Abstract {

    private $_sMainPageUrl = 'http://www.skapiec.pl/';

    /**
     * Instancja klasy.
     * 
     * @var Model_Logic_Skapiec_Parser 
     */
    static private $_oInstance;

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_Logic_Skapiec_Parser
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function manageParamsForCategory($iCategoryThreeId) {

        

        try
        {
        $oDocument = $this->_call('http://www.skapiec.pl/cat/' . $iCategoryThreeId . '-parasolki-studyjne.html', array(), array('viewbox' => 'list'), true);

        $aFilterContainers = $oDocument->find('.fltr_container');

        foreach ($aFilterContainers as $iFilterKey => $aFilterContainer) {
            $aFilterData = array();
            $aFilterData['name'] = pq($aFilterContainer)->find('h3')->text();
            $aFilterData['id_category_three'] = $iCategoryThreeId;
            $aFilterData['sort'] = $iFilterKey;
            
            $sDesc = $oDocument->find('#help'.$iFilterKey); 
            
            if(!empty($sDesc))
            {
                $sDesc->find('strong')->remove(); 
                
                $aFilterData['description'] = $sDesc->text(); 
            }

                $iFilterId = Model_DbTable_Filter::getInstance()->insert($aFilterData);
                $aParameterContainers = pq($aFilterContainer)->find('a');

                $aParametersData = array();

                foreach ($aParameterContainers as $iKey => $aParameterContainer) {
                    $aParameterData = array();
                    $aParameterData['id_skapiec'] = pq($aParameterContainer)->parent()->parent()->find('input')->attr('value');
                    $aParameterData['id_filter'] = $iFilterId;
                    $aParameterData['name'] = pq($aParameterContainer)->text();
                    $aParameterData['link'] = pq($aParameterContainer)->attr('href');
                    $aParameterData['sort'] = $iKey;

                    if (!empty($aParameterData['link'])) {
                        array_push($aParametersData, $aParameterData);
                    }
                }

                Model_DbTable_Parameter::getInstance()->multipleInsert($aParametersData);
        }
        
                return true; 
                
        }catch(Exception $e)
        {
            echo '<pre>'.$e->getMessage().'</pre>';
            return false; 
        }
    }

    public function manageCategory() {
        $aCategory = Model_DbTable_CategoryOne::getInstance()->getJob();

        if (empty($aCategory)) {
            echo 'Nie ma zadań';
            return;
        }

        require_once('phpQuery.php');

        $oDocument = $this->_call($aCategory['url']);

        $oColumns = pq($oDocument)->find('.ndSubDepsCol h3');

        foreach ($oColumns as $oColumn) {
            $sCategoryTwoName = pq($oColumn)->text();
            $iCategoryTwoId = Model_DbTable_CategoryTwo
                    ::getInstance()
                    ->add(array('name' => $sCategoryTwoName,
                'url' => $this->seo($sCategoryTwoName),
                'id_category_one' => $aCategory['id']));

            $oCategoryThreeDoc = pq($oColumn)->next('ul')->find('li');

            foreach ($oCategoryThreeDoc as $oCategoryThreeElem) {
                $aCategoryThreeRow = array();
                preg_match('/cat\/(\d+)-/', pq($oCategoryThreeElem)->find('a')->attr('href'), $aMatches);
                $aCategoryThreeRow['name'] = trim(pq($oCategoryThreeElem)->find('a')->text());
                $aCategoryThreeRow['url'] = $this->seo($aCategoryThreeRow['name']);
                $aCategoryThreeRow['id'] = $aMatches[1];


                if (!Model_DbTable_CategoryThree::getInstance()->exists(array('id' => $aCategoryThreeRow['id']))) {
                    Model_DbTable_CategoryThree::getInstance()->add($aCategoryThreeRow);
                }

                Model_DbTable_CategoryTwoThree::getInstance()->add($iCategoryTwoId, $aCategoryThreeRow['id']);
            }
        }

        $aPromotedElements = pq($oDocument)->find('.ndTopPop');

        foreach ($aPromotedElements as $aPromotedElement) {
            $aPromoted = array();
            $aPromoted['promoted'] = Model_DbTable_CategoryThree::CATEGORY_IS_PROMOTED;
            $aPromoted['image'] = App_Image::saveImageFromWeb(pq($aPromotedElement)->find('img')->attr('src'));

            preg_match('/cat\/(\d+)-/', pq($aPromotedElement)->find('a')->eq(1)->attr('href'), $aPromoMatches);

            Model_DbTable_CategoryThree::getInstance()->update($aPromoted, array('id = ?' => $aPromoMatches[1]));
        }


        Model_DbTable_CategoryOne::getInstance()->jobDone($aCategory['id']);

        echo 'Koniec : Kategoria ' . $aCategory['name'];
    }

    public function manageProducts() {
        $aJobs = Model_DbTable_CategoryThree::getInstance()->getProductListJob(1);



        require_once('phpQuery.php');

        foreach ($aJobs as $aJob) {

            $iAdded = 0;
            $iUpdated = 0;

            $oDocument = $this->_call('http://www.skapiec.pl/cat/' . $aJob['id'] . '-parasolki-studyjne.html', array('customPageLimit' => 1000), array('viewbox' => 'list'), true);

            

            $oColumns = pq($oDocument)->find('.p_row');
            foreach ($oColumns as $oColumn) {

                $iId = str_replace('row', '', pq($oColumn)->attr('id'));


                $aParamsTable = pq($oColumn)->find('table')->find('tr');

                $aElems = array();

                foreach ($aParamsTable as $aRow) {
                    if (pq($aRow)->find('th')->text() != '') {
                        $aElems[pq($aRow)->find('th')->text()] = pq($aRow)->find('td')->text();
                    }
                }

                $iOffers = pq($oColumn)->find('.count_dealer')->find('strong')->text();
                if (empty($iOffers)) {
                    $iOffers = 1;
                }

                $aProductData = array();
                $aProductData['id'] = $iId;
                $aProductData['params'] = base64_encode(serialize($aElems));
                $aProductData['min_price'] = trim(preg_replace('/(od|zł)/', '', pq($oColumn)->find('.price_black')->text()));

                if (empty($aProductData['min_price'])) {
                    $sText = pq($oColumn)->text();

                    preg_match('/price_black\">(.*)\sz/', $sText, $aMatches);

                    $aProductData['min_price'] = $aMatches[1];
                }
                if (preg_match('/span/', $aProductData['min_price'])) {

                    preg_match('/<span class=\"zl\">(.*)<span class="gr">(.*)/', $aProductData['min_price'], $aMatchesDeep);

                    $aProductData['min_price'] = $aMatchesDeep[1] . $aMatchesDeep[2];
                }

                $aProductData['offers'] = $iOffers;
                $aProductData['name'] = trim(pq($oColumn)->find('.entry-title')->find('a')->text());
                $aProductData['image'] = App_Image::saveImageFromWeb('http://static4.skapiec.pl/' . $iId . '-1-1-4.jpg');
                $aProductData['link'] = 'http://skapiec.pl' . pq($oColumn)->find('.entry-title')->find('a')->attr('href');
                $aProductData['rate'] = round((int) (preg_replace('/.*\s(\d+)px.*/', "$1", pq($oColumn)->find('.opinia_stars_g')->attr('style')) / 15), 1);

                $iOpinions = pq($oColumn)->find('.opinie')->find('strong')->text();

                if ($iOpinions == 'Brak opinii') {
                    $iOpinions = 0;
                }

                $aProductData['opinions'] = (int) $iOpinions;

                if ($aProductData['min_price'] === null) {
                    $aProductData['min_price'] = 0;
                }

                try {
                    if (!Model_DbTable_Product::getInstance()->exists(array('id' => $aProductData['id']))) {
                        Model_DbTable_Product::getInstance()->insert($aProductData);
                        
                        $sProductSearch = $this->seo(implode(' ' , array($aJob['name'], $aJob['name'], $aProductData['name2']))); 
                        Model_DbTable_ProductSearch::getInstance()->insert(array('id' => $aProductData['id'],
                                                                                   'name' => $sProductSearch));   
                        
                        
                    } else {
                        Model_DbTable_Product::getInstance()->update(array('offers' => $aProductData['offers'],
                            'min_price' => $aProductData['min_price']), array('id = ?' => $aProductData['id']));
                        $iUpdated++;
                    }

                    if (!Model_DbTable_ProductCategory::getInstance()->exists(array('id_category_three' => $aJob['id'], 'id_product' => $aProductData['id']))) {
                        Model_DbTable_ProductCategory::getInstance()->insert(array('id_category_three' => $aJob['id'], 'id_product' => $aProductData['id']));
                        $iAdded++;
                    }
                } catch (Exception $e) {
                    echo $e->getMessage();
                    die();
                }
            }
            Model_DbTable_CategoryThree::getInstance()->update(array('job_products' => $aJob['job_products'] + 1), array('id = ?' => $aJob['id']));

            echo 'Kategoria :  ' . $aJob['id'] . ' Dodane : ' . $iAdded . ' Uaktualnione : ' . $iUpdated;
        }
    }

    public function getDescriptionForProduct($iProductId, $iCategoryId) {

        $sUrl = sprintf('http://www.skapiec.pl/site/cat/%s/comp/%s_techniczne', $iCategoryId, $iProductId);

        $aData = $this->_call($sUrl);

        $sInfo = trim(pq($aData)->find('.tech_info')->text());


        $aParams = pq($aData)->find('div.cont');

        $aParamsRawData = array();

        foreach ($aParams as $aParam) {

            $aParamsRawData[] = array('name' => pq($aParam)->find('span')->text(), 'val' => pq($aParam)->find('div.val')->text());
        }
        shuffle($aParamsRawData);

        $sParamBody = '';

        foreach ($aParamsRawData as $aRawParam) {
            $sParamBody .= '<tr><th>' . trim($aRawParam['name']) . '</th><td>' . trim($aRawParam['val']) . '</td></tr>';
        }

        include('synonimek.php');


        $aInfoSentences = preg_split('/(?<=[.?!])\s+(?=[a-z])/i', $sInfo);

        shuffle($aInfoSentences);
        $sInfoFinalText = synonimek(implode(' ', $aInfoSentences));


        return array('text' => $sInfoFinalText, 'params' => synonimek($sParamBody));
    }

    public function manageParameter() {
        $aParameters = Model_DbTable_Parameter::getInstance()->getJobs();

        if (empty($aParameters)) {
            echo 'Nie ma zadań';
            return;
        }

        require_once('phpQuery.php');

        foreach ($aParameters as $aParameter) {

            $oDocument = $this->_call($aParameter['link'], array('customPageLimit' => 5000), array('viewbox' => 'list'));

            $oColumns = pq($oDocument)->find('.p_row');

            foreach ($oColumns as $oColumn) {

                $iId = str_replace('row', '', pq($oColumn)->attr('id'));

                if (Model_DbTable_Product::getInstance()->exists(array('id' => $iId))) {

                    if (!Model_DbTable_ProductParameter::getInstance()->exists(array('id_product' => $iId, 'id_parameter' => $aParameter['id']))) {
                        Model_DbTable_ProductParameter::getInstance()->insert(array('id_product' => $iId, 'id_parameter' => $aParameter['id']));
                    }
                }
            }

            Model_DbTable_Parameter::getInstance()->update(array('job' => 1), array('id = ?' => $aParameter['id']));
        }
    }

    private function _call($sCall, $aPost = array(), $aCookies = array(), $iReloaded = false) {

        $oClient = new Zend_Http_Client(null, array('timeout' => 300));
        $oClient->setUri($sCall);

        if (!empty($aPost)) {
            foreach ($aPost as $sName => $sValue) {
                $oClient->setParameterPost($sName, $sValue);
            }
        }
        
        
        
        if (!empty($aCookies)) {

            // $oClient->setCookieJar(); 

            foreach ($aCookies as $sName => $sValue) {
                $oClient->setCookie($sName, $sValue);
            }
        }


        $oResponse = $oClient->request(Zend_Http_Client::POST);


        if ($iReloaded) {

            $oClient->setUri($oClient->getUri()->__toString());
            if (!empty($aPost)) {
                foreach ($aPost as $sName => $sValue) {
                    $oClient->setParameterPost($sName, $sValue);
                }
            }

            if (!empty($aCookies)) {

                // $oClient->setCookieJar(); 

                foreach ($aCookies as $sName => $sValue) {
                    $oClient->setCookie($sName, $sValue);
                }
            }
            $oResponse = $oClient->request(Zend_Http_Client::POST);
        }

     

        if ($oResponse->isSuccessful()) {
            
            $htmlCode = $this->toUtf($oResponse->getBody());

            $htmlCode = mb_convert_encoding($htmlCode, 'HTML-ENTITIES', "UTF-8");

            return phpQuery::newDocumentHTML($htmlCode, 'utf-8');
        } else
            throw new Exception('API call failed', 500);
    }

    private function toUtf($txt) {
        $txt = str_replace(chr(0xB1), 'ą', $txt);
        $txt = str_replace(chr(0xE6), 'ć', $txt);
        $txt = str_replace(chr(0xEA), 'ę', $txt);
        $txt = str_replace(chr(0xB3), 'ł', $txt);
        $txt = str_replace(chr(0xF1), 'ń', $txt);
        $txt = str_replace(chr(0xF3), 'ó', $txt);
        $txt = str_replace(chr(0xB6), 'ś', $txt);
        $txt = str_replace(chr(0xBC), 'ź', $txt);
        $txt = str_replace(chr(0xBF), 'ż', $txt);
        $txt = str_replace(chr(0xA1), 'Ą', $txt);
        $txt = str_replace(chr(0xC6), 'Ć', $txt);
        $txt = str_replace(chr(0xCA), 'Ę', $txt);
        $txt = str_replace(chr(0xA3), 'Ł', $txt);
        $txt = str_replace(chr(0xD1), 'Ń', $txt);
        $txt = str_replace(chr(0xD3), 'Ó', $txt);
        $txt = str_replace(chr(0xA6), 'Ś', $txt);
        $txt = str_replace(chr(0xAC), 'Ź', $txt);
        $txt = str_replace(chr(0xAF), 'Ż', $txt);


        return $txt;
    }

}
