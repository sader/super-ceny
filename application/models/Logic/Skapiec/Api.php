<?php

/**
 * Model_Logic_Api
 *
 * @author Krzysztof Janicki
 */
class Model_Logic_Skapiec_Api extends Model_Logic_Abstract {

    private $sCategoryListUrl = 'http://api.skapiec.pl/beta_getProductMostPopular.json?category=%s&amount=20';
    private $sFiltersListUrl = 'http://api.skapiec.pl/beta_getFilters.json?category=%s&withOptions=1';
    private $sShopsListUrl = 'http://api.skapiec.pl/beta_getOffersBestPrice.json?id_skapiec=%s&amount=20';
    private $sFilterProducts = 'http://api.skapiec.pl/beta_filterProducts.json?filter=_%s&category=%s&amount=20%s';
    /**
     * Instancja klasy.
     * 
     * @var Model_Logic_Skapiec_Api
     */
    static private $_oInstance;

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_Logic_Skapiec_Api
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }
    
    public function  manageListForCategoryWithFilters($aSubcategory, $sFilters, $iPage = 1)
    {
        
        $sCallLink = sprintf($this->sFilterProducts, $sFilters, $aSubcategory['id'], $iPage > 1 ? '&offset=' . ($iPage - 1) * 20 : ''); 
        
       
        if (($aData = $this->getCache()->load(md5($sCallLink))) === false)
        {
           $aResponse =  $this->_call($sCallLink); 
            
           $aData['ids'] = array_map(create_function('$ar', 'return $ar["id_skapiec"];'), $aResponse['component']);
           $aData['total'] = $aResponse['pagination']['totalRows']; 
           
           $this->getCache()->save($aData, md5($sCallLink), array('api_call'), 3600 * 24 * 30); 
           
        }
        
        return $aData; 
    }

    public function manageListForCategory() {

        $aCategories = Model_DbTable_CategoryThree::getInstance()->getProductListJob();

        if (empty($aCategories)) {
            echo 'Brak zadań do wykonania';
            return;
        }


        foreach ($aCategories as $aCategory) {
            try
            {
            $sApiCall = sprintf($this->sCategoryListUrl, $aCategory['id']);


            if ($aCategory['offset'] > 0) {
                $sApiCall .= '&offset=' . $aCategory['offset'];
            }

            $aData = $this->_call($sApiCall);



            if (isset($aData['component']) && !empty($aData['component'])) {
                foreach ($aData['component'] as $aRow) {

                    if (!Model_DbTable_ProductCategory::getInstance()->isProductCategoryExist($aRow['id_skapiec'], $aCategory['id'])) {
                        Model_DbTable_ProductCategory::getInstance()->add($aRow['id_skapiec'], $aCategory['id']);
                    }

                    if (!Model_DbTable_Product::getInstance()->exists(array('id' => $aRow['id_skapiec']))) {

                        $aProduct = array();
                        $aProduct['id'] = $aRow['id_skapiec'];
                        $aProduct['name'] = $aRow['name'];
                        $aProduct['producer'] = isset($aRow['producer']) ? $aRow['producer'] : '';
                        $aLink = explode('#', $aRow['link']);
                        $aProduct['link'] = $aLink[0];
                        $aProduct['image'] = App_Image::saveImageFromWeb(sprintf('http://static4.skapiec.pl/%s-1-1-6.jpg', $aRow['id_skapiec']));
                        $aProduct['offers'] = $aRow['offersCount'];
                        $aProduct['min_price'] = $aRow['min_price'];

                        if (empty($aProduct['name']))
                        {
                           continue;
                        }

                        Model_DbTable_Product::getInstance()->add($aProduct);
                    } else {
                        Model_DbTable_Product::getInstance()->update(array('min_price' => $aRow['min_price'], 'offers' => $aRow['offersCount']), array('id = ?' => $aRow['id_skapiec']));
                    }
                }
            }

            $iOffset = (int) $aCategory['offset'] + 20;
            if ($aData['totalCount'] <= $iOffset) {
                $aUpdate = array('offset' => 0, 'job_products' => 1);
            }
            else
            {
                $aUpdate = array('offset' => $iOffset);
            }


            Model_DbTable_CategoryThree::getInstance()->update($aUpdate, array('id = ?' => $aCategory['id']));
            }
            catch (Exception $e)
            {
                $this->getLog()->log($e, Zend_Log::CRIT); 
                Model_DbTable_CategoryThree::getInstance()->update(array('job_products' => 2), array('id = ?' => $aCategory['id']));
            }
        }

        echo 'Done';
    }

    public function manageFiltersForCategory() {
        $aCategories = Model_DbTable_CategoryThree::getInstance()->getFilterListJob(1);

        if (empty($aCategories)) {
            echo 'Brak zadań do wykonania';
            return;
        }

        try {
            Zend_Db_Table::getDefaultAdapter()->beginTransaction();




            foreach ($aCategories as $aCategory) {
                $sApiCall = sprintf($this->sFiltersListUrl, $aCategory['id']);

                $aData = $this->_call($sApiCall);

                if (isset($aData['filter']) && !empty($aData['filter'])) {
                    foreach ($aData['filter'] as $aFilter) {
                        $aNewFilter = array();
                        $aNewFilter['id_category_three'] = $aCategory['id'];
                        $aNewFilter['id_skapiec'] = $aFilter['id'];
                        $aNewFilter['name'] = $aFilter['name'];
                        $aNewFilter['description'] = $aFilter['description'];
                        $aNewFilter['sort'] = (int) $aFilter['sort'];

                        $iFilterId = Model_DbTable_Filter::getInstance()->insert($aNewFilter);


                        foreach ($aFilter['option'] as $aOption) {
                            $aLink = explode('#', $aOption['link']);

                            $aNewParameter = array();
                            $aNewParameter['id_skapiec'] = $aOption['id'];
                            $aNewParameter['id_filter'] = $iFilterId;
                            $aNewParameter['name'] = $aOption['opis'];
                            $aNewParameter['link'] = $aLink[0];
                            $aNewParameter['sort'] = ($aOption['sort'] === null) ? '' : $aOption['sort'];

                            Model_DbTable_Parameter::getInstance()->insert($aNewParameter);
                        }
                    }
                }

                Model_DbTable_CategoryThree::getInstance()->update(array('job_filters' => Model_DbTable_CategoryThree::JOB_FINISHED), array('id = ?' => $aCategory['id']));
            }
            Zend_Db_Table::getDefaultAdapter()->commit();
        } catch (Exception $exc) {
            Zend_Db_Table::getDefaultAdapter()->rollBack();
            Model_DbTable_CategoryThree::getInstance()->update(array('job_filters' => 2), array('id = ?' => $aCategory['id']));
        }
        
        echo 'Done'; 
    }

    public function manageShops(App_Controller_Page_Abstract $oCtrl) {
        if (($iSkapiecId = $oCtrl->getRequest()->getParam('id', null)) === null) {
            throw new Exception(500);
        }

        $sApiCall = sprintf($this->sShopsListUrl, $iSkapiecId);

        $aData = $this->_call($sApiCall);

            return $oCtrl->view->partial('_partial/shops.phtml' , array('shops' => $aData['component'][0]['offers']));

    }
    
    public function getByMergedFilterLinks($aData, $aSubcategory)
    {
        
        if(empty($aData) || !is_array($aData)) return false; 
        
        
        $aMergedParams = array(); 
        
        
        
        foreach($aData as $aRow)
        {
            
                $sLink = preg_replace('/\/site\/cat\/\d+\/filtr\/_/', '', $aRow['link']); 
            
                $aLinkElements = explode('_' , $sLink); 
                
                foreach($aLinkElements as $iLinkElemKey => $aLinkElement)
                {
                    if(!empty($aLinkElement))
                    {
                        $aMergedParams[$iLinkElemKey] = $aLinkElement; 
                    }
                }
                $sCount = count($aLinkElements); 
        }
       
        
        
        
              for($i = 0; $i < $sCount; $i++)
                {
                    if(!isset($aMergedParams[$i]))
                    {
                        $aMergedParams[$i] = 0; 
                    }
                }
                
                ksort($aMergedParams); 
        
               
                return implode('_', $aMergedParams);
    }


        private function _call($sCall, $aPost = array()) {

        $oClient = new Zend_Http_Client(null, array('timeout' => 30));
        $oClient->setUri($sCall);
        $oClient->setAuth('marcin@domset.pl', '123qwe');
        $oResponse = $oClient->request();
        sleep(2);
        if ($oResponse->isSuccessful()) {
            return Zend_Json::decode($oResponse->getBody());
        } else {
           throw new Exception($oResponse);
        }
    }

}