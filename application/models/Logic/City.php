<?php

/**
 * Model_Logic_Admin
 *
 * @author Krzysztof Janicki
 */
class Model_Logic_City extends Model_Logic_Abstract {

    /**
     * Instancja klasy.
     * 
     * @var Model_Logic_City
     */
    static private $_oInstance;

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_Logic_City
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    private function getLink() {
        return APPLICATION_PATH . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'city';
    }

    public function getList() {
        return $this->names(file($this->getLink()));
    }

    public function getRandomLinks($iRandom = 5) {
        $aData = file($this->getLink());
        shuffle($aData);
        return $this->names(array_slice($aData, 0, $iRandom));
    }

    public function names($aData) {
        $aReturn = array();

        foreach ($aData as $aRow) {
            $aElems = explode(';', $aRow);
            $aReturn[] = $aElems[0];
        }
        return $aReturn;
    }

    public function getBySeo($sName) {
        $aCities = $this->getList();

        foreach ($aCities as $aCity) {
            if ($this->seo($aCity[0]) == $sName) {
                return $aCity;
            }
        }
    }

}