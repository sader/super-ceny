<?php

/**
 * Model_Logic_Api
 *
 * @author Krzysztof Janicki
 */
class Model_Logic_ProxyMarket_Api extends Model_Logic_Abstract {

    private $_sKey = 'cd2f899702726147487f33bb9935073f';
    private $_sUrl = 'http://www.proxygen.pl/apibr/';


    /**
     * Instancja klasy.
     * 
     * @var Model_Logic_ProxyMarket_Api
     */
    static private $_oInstance;

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_Logic_ProxyMarket_Api
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }
    
    public function getProxy()
    {
        $aData = $this->_call($this->_sUrl.$this->_sKey);
        
        $aProxies = explode("\n", $aData); 
        
        return  array_filter($aProxies); 
        
        
    }


        private function _call($sCall) {

        $oClient = new Zend_Http_Client(null, array('timeout' => 30));
        $oClient->setUri($sCall);
        $oResponse = $oClient->request();

        if ($oResponse->isSuccessful()) {
            
            return $oResponse->getBody(); 
            
        }
        else
            throw new Exception('API call failed', 500);
    }

}