<?php

class Model_Logic_Abstract {
    const DEFAULT_ITEM_PER_PAGE = 20;

    /**
     *
     * @return Zend_Log
     */
    protected function getLog() {
        return Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('log');
    }

    protected function getResourceView() {
        return Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
    }

    /**
     *
     * @return Zend_Cache_Core
     */
    public function getCache($sName = 'default') {

        return Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('cachemanager')->getCache($sName);
    }

  public function seo($str, $replace = '-') {
        $str = iconv('UTF-8', 'ASCII//TRANSLIT', trim($str, ' -' . $replace));
        $str = preg_replace('/[^a-zA-Z0-9\s\.' . $replace . ']/', '', strtolower($str));
        $str = preg_replace('/' . $replace . '/', ' ', $str);
        $str = preg_replace('/ +/', ' ', $str);

        return str_replace(' ', $replace, $str);
    }
    
    protected function required($sName, $sMessage = 'Required field %s is not set')
    {
      if (($sParam = Zend_Controller_Front::getInstance()->getRequest()->getParam($sName)) === false)
      {
          throw new Exception(sprintf($sMessage, $sName), 500); 
      }
        return $sParam;         
    }

}

