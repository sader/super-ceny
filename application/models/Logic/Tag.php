<?php

/**
 * Model_Logic_Filter
 *
 * @author Krzysztof Janicki
 */
class Model_Logic_Tag extends Model_Logic_Abstract {

    /**
     * Instancja klasy.
     * @var Model_Logic_Tag
     */
    static private $_oInstance;

    /**
     * Zwraca instancje klasy.
     * 
     * @return  Model_Logic_Tag
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }
    
    const TYPE_PRODUCT = 1; 
    const TYPE_SEARCH = 2; 
    const TYPE_CATEGORY = 3; 
    const TYPE_CATEGORY_FILTER = 4; 
    const FILL_CATEGORY_SET = 'all';
    const MARKER = '[x]'; 
    
    public static function getTagTypes()
    {
        return array( self::TYPE_PRODUCT => 'Strona produktu' , 
                      self::TYPE_SEARCH => 'Strona wyszukiwania', 
                      self::TYPE_CATEGORY => 'Strona kategorii' , 
                      self::TYPE_CATEGORY_FILTER => 'Strona kategorii z nałożonym filtrem'); 
    }
    
    
    public function manageAdd(App_Controller_Admin_Abstract $oCtrl)
    {
        $oForm = new Admin_Form_Meta(); 
        if ($oCtrl->getRequest()->isPost() && $oForm->isValid($oCtrl->getRequest()->getPost())) {

            $aFormData = $oForm->getValues();

            try {
                Zend_Db_Table::getDefaultAdapter()->beginTransaction();
                Model_DbTable_Tag::getInstance()->add($aFormData);
                Zend_Db_Table::getDefaultAdapter()->commit();
                $oCtrl->successMessage('Nowy tag dodany');
            } catch (Exception $e) {
                Zend_Db_Table::getDefaultAdapter()->rollBack();
                $this->getLog()->log($e, Zend_Log::CRIT);
                $oCtrl->errorMessage('Błąd podczas dodania tagu');
            }

            $oCtrl->getHelper('Redirector')->goToUrl($oCtrl->view->url(array('action' => 'list')));
        }
        
        $oCtrl->view->form = $oForm;
        $oCtrl->view->headScript()->appendFile('/scripts/admin/tag.js');
    }
    
    
      public function manageEdit(App_Controller_Admin_Abstract $oCtrl)
    {
          
        $iId = $this->required('id');   
        
        $aTag  = Model_DbTable_Tag::getInstance()->getById($iId); 
        
        $oForm = new Admin_Form_Meta(); 
        $oForm->editMode(); 
        
        if ($oCtrl->getRequest()->isPost() && $oForm->isValid($oCtrl->getRequest()->getPost())) {

            $aFormData = $oForm->getValues();

            try {
                Zend_Db_Table::getDefaultAdapter()->beginTransaction();
                Model_DbTable_Tag::getInstance()->edit($aFormData, $iId);
                Zend_Db_Table::getDefaultAdapter()->commit();
                $oCtrl->successMessage('Edycja tagu zakończona');
            } catch (Exception $e) {
                Zend_Db_Table::getDefaultAdapter()->rollBack();
                $this->getLog()->log($e, Zend_Log::CRIT);
                $oCtrl->errorMessage('Błąd podczas edycji tagu');
            }

            $oCtrl->getHelper('Redirector')->goToUrl($oCtrl->view->url(array('action' => 'list')));
        }
            
        $aTag['type'] = explode(',', $aTag['type']); 
        
        $oForm->populate($aTag); 
        $oCtrl->view->form = $oForm;
        $oCtrl->view->tag = $aTag; 
        $oCtrl->view->headScript()->appendFile('/scripts/admin/tag.js');
    }
    
    
     public function manageRemove(App_Controller_Admin_Abstract $oCtrl) {

        if (($iId = $oCtrl->getRequest()->getParam('id')) === null) {
            throw new Exception('Błąd identyfikatora');
        }

        Model_DbTable_Tag::getInstance()->remove($iId);
        $oCtrl->successMessage('Typ usunięty');
        $oCtrl->getHelper('Redirector')->goToUrl($_SERVER['HTTP_REFERER']);
    }

    public function manageList(App_Controller_Admin_Abstract $oCtrl) {

        $oGrid = new Admin_Grid_Tag();

        $oCtrl->view->grid = $oGrid->deploy(); 
    }
    
    public function subcategory(App_Controller_Page_Abstract $oCtrl)
    {
        $sTag = $oCtrl->getRequest()->getParam('tag'); 
        
          $sTag = $oCtrl->getRequest()->getParam('tag'); 
        
     if (($aTag =  Model_DbTable_Tag::getInstance()->getByNameAndType($sTag, self::TYPE_CATEGORY)) == false)
     {
         throw new Exception('No tag' , 404);
     }
     
     if($aTag['cat3res'] != self::FILL_CATEGORY_SET)
     {
         if(!in_array($oCtrl->view->categoriesThree['id'], explode(',', $aTag['cat3res'])))
         {
            throw new Exception('No tag' , 404); 
         }
     }
          
        $oCtrl->view->headTitle(str_replace(self::MARKER ,$oCtrl->view->categoriesThree['name'], $aTag['title']));
        $oCtrl->view->headMeta()->appendName('description', str_replace(self::MARKER ,$oCtrl->view->categoriesThree['name'], $aTag['description']));
        $oCtrl->view->headMeta()->appendName('keywords', str_replace(self::MARKER ,$oCtrl->view->categoriesThree['name'], $aTag['keywords']));
        
    }
    
    public function subcategoryFilter(App_Controller_Page_Abstract $oCtrl)
    {
        $sTag = $oCtrl->getRequest()->getParam('tag'); 
        
     if (($aTag =  Model_DbTable_Tag::getInstance()->getByNameAndType($sTag, self::TYPE_CATEGORY_FILTER)) == false)
     {
         throw new Exception('No tag' , 404);
     }
     
     if($aTag['cat4res'] != self::FILL_CATEGORY_SET)
     {
         if(!in_array($oCtrl->view->categoriesThree['id'], explode(',', $aTag['cat4res'])))
         {
            throw new Exception('No tag' , 404); 
         }
     }
          
        $oCtrl->view->headTitle(str_replace(self::MARKER ,$oCtrl->view->categoriesThree['name'], $aTag['title']));
        $oCtrl->view->headMeta()->appendName('description', str_replace(self::MARKER ,$oCtrl->view->categoriesThree['name'], $aTag['description']));
        $oCtrl->view->headMeta()->appendName('keywords', str_replace(self::MARKER ,$oCtrl->view->categoriesThree['name'], $aTag['keywords']));
     
        
    }
    
    public function search(App_Controller_Page_Abstract $oCtrl)
    {
        
       $sTag = $oCtrl->getRequest()->getParam('tag'); 
      $sSearch = $oCtrl->getRequest()->getParam('search');
       
       
     if (($aTag =  Model_DbTable_Tag::getInstance()->getByNameAndType($sTag, self::TYPE_SEARCH)) == false)
     {
         throw new Exception('No tag' , 404);
     }
     
      $oCtrl->view->headTitle(str_replace(self::MARKER ,$sSearch, $aTag['title']));
        $oCtrl->view->headMeta()->appendName('description', str_replace(self::MARKER ,$sSearch, $aTag['description']));
        $oCtrl->view->headMeta()->appendName('keywords', str_replace(self::MARKER ,$sSearch, $aTag['keywords']));
       
     
        
    }
    
    public function product(App_Controller_Page_Abstract $oCtrl)
    {
        
                $sTag = $oCtrl->getRequest()->getParam('tag'); 
        
         if (($aTag =  Model_DbTable_Tag::getInstance()->getByNameAndType($sTag, self::TYPE_PRODUCT)) == false)
     {
         throw new Exception('No tag' , 404);
     }
     
      $oCtrl->view->headTitle(str_replace(self::MARKER ,$oCtrl->view->product['name'], $aTag['title']));
        $oCtrl->view->headMeta()->appendName('description', str_replace(self::MARKER ,$oCtrl->view->product['name'], $aTag['description']));
        $oCtrl->view->headMeta()->appendName('keywords', str_replace(self::MARKER ,$oCtrl->view->product['name'], $aTag['keywords']));
       
    
    }
   
    
}