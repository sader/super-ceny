<?php

/**
 * Model_Logic_Filter
 *
 * @author Krzysztof Janicki
 */
class Model_Logic_Search extends Model_Logic_Abstract {

    /**
     * Instancja klasy.
     * @var Model_Logic_Search
     */
    static private $_oInstance;

    /**
     * Zwraca instancje klasy.
     * 
     * @return  Model_Logic_Search
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public static function compare($a, $b) {
        return $a['score'] - $b['score'];
    }

    public function manageSearch(App_Controller_Page_Abstract $oCtrl) {


        if (($sSearch = $oCtrl->getRequest()->getParam('search', null)) === null) {
            $oCtrl->getHelper('Redirector')->goToUrl($oCtrl->view->url(array(), 'home'));
        }
        
        
        
        $sSearch = str_replace('-', ' ', $sSearch); 
        
        $aSearchRow = array('name' => $sSearch, 'search' => $this->seo($sSearch, ' '));
        
        $aParams = $this->getParams($oCtrl->getRequest(), $aSearchRow['search']);
        
        $iPerPage = 28;
        $iPage = isset($aParams['page']) ? $aParams['page'] : 1; 
        
        
        $aLuceneResult  = Model_DbTable_ProductSearch::getInstance()->similiar($aSearchRow['search'],(($iPage - 1) * $iPerPage), $iPerPage + 1); 

        if(count($aLuceneResult) == $iPerPage + 1)
        {
            
            $nextPage = true; 
            $aLuceneResult = array_slice($aLuceneResult, 0 ,$iPerPage); 
        }
        
            $oCtrl->view->results = $aLuceneResult; 
            $oCtrl->view->data = array('back' => $iPage > 1 , 'forward' => $nextPage, 'page' => $iPage); 
            $oCtrl->view->search = $sSearch;
            $oCtrl->view->params = $aParams;
            $oCtrl->view->similiars = Model_DbTable_GoogleResults::getInstance()->similiar($sSearch);
            $oCtrl->view->tags = Model_Logic_Product::getInstance()->rabatTags();
        
    }

    public function manageSearchX(App_Controller_Page_Abstract $oCtrl) {

        if (($sSearch = $oCtrl->getRequest()->getParam('search', null)) === null) {
            $oCtrl->getHelper('Redirector')->goToUrl($oCtrl->view->url(array(), 'home'));
        }


        $aSearchRow = array('name' => $sSearch, 'search' => $this->seo($sSearch, ' '));

        $aLuceneResult = $this->luceneSearch($aSearchRow['search']);




        //  if (!Model_DbTable_Search::getInstance()->exists(array('url' => $aSearchRow['url']))) {
        //      Model_DbTable_Search::getInstance()->insert($aSearchRow);
        //  }


        $aFullCategoryMatch = Model_DbTable_CategoryOne::getInstance()->fullCategorySearchByUrl($sSearch);

        if (!empty($aFullCategoryMatch)) {
            $oCtrl->getHelper('Redirector')->goToUrl('/' . $aFullCategoryMatch['type'] . '/' . $aFullCategoryMatch['seo']);
        }

        $aLuceneResult = $this->luceneSearch($aSearchRow['search']);

        if (empty($aLuceneResult['id'])) {
            $bResultsFound = false;
            $oAdapter = new Zend_Paginator_Adapter_Array(array());
            $oCtrl->view->categoriesGroup = array();
        } else {

            $bResultsFound = true;

            $aParams = $this->getParams($oCtrl->getRequest(), $aSearchRow['search']);



            $oQuery = Model_DbTable_Product::getInstance()->search($aLuceneResult['id'], $aParams);

            if (!empty($aParams['category'])):

                $oQuery->where('p.id IN(SELECT id_product FROM product_category_three
                              WHERE product_category_three.id_category_three = (SELECT id from category_three
                                                              WHERE category_three.url = "' . $aParams['category'] . '" LIMIT 1))');


            elseif (!empty($aParams['group'])):

                $oQuery->where('p.id IN(SELECT id_product FROM product_category_three
                              WHERE product_category_three.id_category_three IN (SELECT id_category_three from category_two_three
                                                              WHERE id_category_two = (SELECT id from category_two
                                                              WHERE category_two.url = "' . $aParams['group'] . '" LIMIT 1)))');

            endif;

            $aResults = $oQuery->query()->fetchAll(Zend_Db::FETCH_ASSOC);




            foreach ($aResults as &$aUsnortedProduct) {

                if (strpos($aUsnortedProduct['categories'] . ' ' . $aUsnortedProduct['name'], $aSearchRow['name']) !== false) {
                    $prec = 100;
                } else {
                    similar_text($aSearchRow['name'], $aUsnortedProduct['categories'] . ' ' . $aUsnortedProduct['name'], $prec);
                }

                $aUsnortedProduct['score'] = $prec;
            }

            usort($aResults, array(__CLASS__, 'compare'));

            $aResults = array_reverse($aResults);


            $oAdapter = new Zend_Paginator_Adapter_Array($aResults);
        }


        $iPage = isset($aParams['page']) ? $aParams['page'] : 1;




        $oPaginator = new Zend_Paginator($oAdapter);
        $oPaginator->setCurrentPageNumber($iPage)
                ->setItemCountPerPage(28);



        $oCtrl->view->paginator = $oPaginator;
        $oCtrl->view->ids = $aLuceneResult['id'];
        $oCtrl->view->search = $sSearch;
        $oCtrl->view->params = $aParams;
        $oCtrl->view->similiars = Model_DbTable_GoogleResults::getInstance()->similiar($sSearch);
        $oCtrl->view->tags = Model_Logic_Product::getInstance()->rabatTags();


        if (!empty($aParams['group'])) {
            $oCtrl->view->subcategories = Model_DbTable_CategoryThree::getInstance()->getListForCategoryTwoByIds($aParams['group'], $aMatched);
        }
    }

    private function getParams($oRequest, $sSearchName) {
        $aParams = array();

        $aParams['search'] = $sSearchName;

        $aParams['filters'] = $this->buildFilterArray($oRequest->getParam('filters'));

        $aParams['group'] = $oRequest->getParam('group', null);

        if (!empty($aParams['group'])) {
            $aParams['group'] = substr($aParams['group'], 2);
        }

        $aParams['category'] = $oRequest->getParam('category', null);
        if (!empty($aParams['category'])) {
            $aParams['category'] = substr($aParams['category'], 2);
        }
        if ($oRequest->getParam('page', null) !== null) {
            $aParams['page'] = substr($oRequest->getParam('page'), 2);
        }

        return $aParams;
    }

    private function buildFilter($aFilters, $iIds) {
        $aReturn = array();

        foreach ($aFilters as $aFilter) {

            $aReturn[$aFilter['id']]['name'] = $aFilter['name'];

            $sCount = empty($aFilter['products']) ? 0 : count(array_intersect(explode(',', $aFilter['products']), $iIds));

            $aReturn[$aFilter['id']]['elems'][] = array('id' => $aFilter['param_id'], 'name' => $aFilter['param_name'], 'count' => $sCount);
        }


        return $aReturn;
    }

    private function buildCategoriesForSearch($aData) {
        $aReturn = array();

        foreach ($aData as $sRow) {
            isset($aReturn[$sRow]) ? $aReturn[$sRow] ++ : $aReturn[$sRow] = 1;
        }

        return $aReturn;
    }

    public function getPath() {
        return APPLICATION_PATH . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'lucene';
    }

    public function resetLucene() {
        array_map('unlink', glob($this->getPath() . "/*"));
    }

    public function luceneSearch($sSearch) {
        $oLucene = Zend_Search_Lucene::open($this->getPath());

        // Zend_Search_Lucene::setResultSetLimit(100);

        $aResults = $oLucene->find($sSearch);
        $aReturn = array();
        /**
          $query = new Zend_Search_Lucene_Search_Query_Boolean();
          foreach (explode('-', $sSearch) as $word) {

          if (strlen($word) > 2) {
          $query->addSubquery(
          new Zend_Search_Lucene_Search_Query_Fuzzy(
          new Zend_Search_Lucene_Index_Term($word), 0.6)
          , null);
          }
          }

          $aData = $oLucene->find($query);
          $aDataSliced = array_slice($aData, 0, 500);

          $aReturn = array('id' => array(), 'category' => array());
          if (!empty($aData)) {
          foreach ($aDataSliced as $aRow) {


          $aReturn['id'][] = $aRow->offer_id;
          $aReturn['category'][] = $aRow->category;
          }
          }
         * 
         * 
         * 
         */
        if ($aResults) {
            foreach ($aResults as $aRow) {
                $aReturn[] = $aRow->offer_id;
            }
        }
        return $aReturn;
    }

    public function populate($iOffset = 0, $iLimit = 20000) {

        $bRunning = true;



        $this->resetLucene();

        $oLucene = Zend_Search_Lucene::create($this->getPath());



        do {
            $aRes = Model_DbTable_Product::getInstance()->getForLucene($iOffset, $iLimit);

            $this->buildLuc($aRes, $oLucene);

            if (count($aRes) < $iLimit) {
                $bRunning = false;
            } else {
                $iOffset += $iLimit;
            }
        } while ($bRunning);

        $oLucene->optimize();
    }

    public function buildLuc($aRes, $oLucene) {
        foreach ($aRes as $aRow) {

            $aData = array();
            $aData['offer_id'] = $aRow['id'];


            $aExtactedCategories = array();
            $aCategories = explode('||', $aRow['category']);

            foreach ($aCategories as $sCat) {

                $aDetailCat = explode('|', $sCat);

                $aExtactedCategories['id'][] = $aDetailCat[0];
                $aExtactedCategories['name'][] = $aDetailCat[1];
                $aExtactedCategories['url'][] = $aDetailCat[2];
            }



            $aData['name'] = $this->seo(implode(' ', $aExtactedCategories['name']) . " " . $aRow['parameters'] . " " . $aRow['name'], ' ');
            $oDoc = new Zend_Search_Lucene_Document();
            $oDoc->addField(Zend_Search_Lucene_Field::keyword('offer_id', $aData['offer_id']));
            $oDoc->addField(Zend_Search_Lucene_Field::keyword('category', implode(',', $aExtactedCategories['url'])));
            $oDoc->addField(Zend_Search_Lucene_Field::unStored('name', $aData['name'], 'utf-8'));

            $oLucene->addDocument($oDoc);
        }
    }

    public function similiars($sWord) {
        if (($aSimiliars = $this->getCache()->load(md5($sWord . 'search'))) === false) {
            $aSimiliars = Model_DbTable_Search::getInstance()->similiar($sWord);

            $this->getCache()->save($aSimiliars, md5($sWord . 'search'));
        }

        return $aSimiliars;
    }

    public function buildFilterArray($sFilters) {
        $aReturn = array();



        if (!empty($sFilters)) {
            $sFilters = substr($sFilters, 2);


            $aPairs = explode(',', $sFilters);
            foreach ($aPairs as $aElem) {
                $aFragments = explode('-', $aElem);
                if (!empty($aFragments[1])) {
                    $aReturn[$aFragments[0]] = $aFragments[1];
                }
            }
        }

        return $aReturn;
    }

}
