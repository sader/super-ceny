<?php

/**
 * Model_Logic_Filter
 *
 * @author Krzysztof Janicki
 */
class Model_Logic_Product extends Model_Logic_Abstract {

    const MAIN_PAGE_PROMOTED_LIMIT = 15;

    /**
     * Instancja klasy.
     * @var Model_Logic_Product
     */
    static private $_oInstance;

    /**
     * Zwraca instancje klasy.
     * 
     * @return  Model_Logic_Product
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function manageMainPagePromoted(App_Controller_Page_Abstract $oCtrl) {
        if (($aPromoted = $this->getCache()->load('promoted_products')) === false) {

            $aPromotedSubCategories = Model_DbTable_CategoryThree::getInstance()->getPromoted();
            $aRandomPromotedCategory = $aPromotedSubCategories[array_rand($aPromotedSubCategories)];

            $aProducts = Model_DbTable_Product::getInstance()->getByCategoryThree($aRandomPromotedCategory);
            shuffle($aProducts);

            $aPromoted = array_slice($aProducts, 0, 15);


            $this->getCache()->save($aPromoted, 'promoted_products');
        }
        $oCtrl->view->promoted = $aPromoted;
    }

    public function manageCategoryPromoted(App_Controller_Page_Abstract $oCtrl) {

        $sUrl = $oCtrl->getRequest()->getParam('url');


        if (($aPromoted = $this->getCache()->load(md5('promoted_category_products' . $sUrl))) === false) {

            $aPromotedSubCategories = Model_DbTable_CategoryThree::getInstance()->getPromotedByCategoryOneUri($sUrl);

            $aRandomPromotedCategory = $aPromotedSubCategories[array_rand($aPromotedSubCategories)];

            $aProducts = Model_DbTable_Product::getInstance()->getByCategoryThree($aRandomPromotedCategory);
            shuffle($aProducts);

            $aPromoted = array_slice($aProducts, 0, 15);


            $this->getCache()->save($aPromoted, md5('promoted_category_products' . $sUrl));
        }
        $oCtrl->view->promoted = $aPromoted;
    }

    public function manageSubcategory(App_Controller_Page_Abstract $oCtrl) {
        if (($sUrl = $oCtrl->getRequest()->getParam('url', null)) === null) {
            throw new Exception('Account id failed', 500);
        }

        if (($aSubcategory = Model_DbTable_CategoryThree::getInstance()->getByUrl($sUrl)) == false) {
            throw new Exception('Category failed', 404);
        }

        $aFilters = Model_DbTable_Filter::getInstance()->getByCategoryThree($aSubcategory['id']);




        $aParams = array();
        $aParams['url'] = $sUrl;
        $aParams['filters'] = $this->buildFilterArray($oCtrl->getRequest()->getParam('filters', null));
        $aParams['page'] = $oCtrl->getRequest()->getParam('page', null);

        if (!empty($aParams['page'])) {
            $aParams['page'] = str_replace('p/', '', $aParams['page']);
        }


        $aParametized = Model_Logic_Filter::getInstance()->buildFilterParametersForListing($aParams, $aSubcategory);


        $aData = array();
        $aData['page'] = empty($aParams['page']) ? 1 : $aParams['page'];
        $iElementsPerPage = 28;
        $iCount = $iElementsPerPage + 1;

        if ($aParametized === false) {


            $iOffset = ($aData['page'] - 1) * $iElementsPerPage;

            $aData['rows'] = Model_DbTable_Product::getInstance()
                    ->getByCategoryThreeSelect($aSubcategory['id'], null, $aParams)
                    ->limit($iCount, $iOffset)
                    ->query()
                    ->fetchAll(Zend_Db::FETCH_ASSOC);
            
            
            $aData['forward'] = count($aData['rows']) === $iCount;
            $aData['rows'] = array_slice($aData['rows'] , 0 , $iElementsPerPage); 
            $aData['back'] = $aData['page'] > 1;
            
        } elseif ($aParametized['total'] == 0 && (count($aParams['filters']) == 2)) {


            $aParams['filters'] = array_splice($aParams['filters'], 0, 1, true);

            $aParametized = Model_Logic_Filter::getInstance()->buildFilterParametersForListing($aParams, $aSubcategory);

            $aData['rows'] = Model_DbTable_Product::getInstance()
                    ->getByCategoryThreeSelect($aSubcategory['id'], null, $aParams)
                    ->limit($iCount, $iOffset)
                    ->query()
                    ->fetchAll(Zend_Db::FETCH_ASSOC);

            $aData['back'] = $aData['page'] > 1;
            $aData['forward'] = count($aData['rows']) === $iCount;
        } elseif ($aParametized['total'] == 0) {
            $aData['rows'] = array();
            $aData['back'] = false;
            $aData['forward'] = false;
        } else {

            $oPaginator = new Zend_Paginator(new Zend_Paginator_Adapter_Null($aParametized['total']));
            $oPaginator->setCurrentPageNumber(empty($aParams['page']) ? 1 : $aParams['page'])
                    ->setItemCountPerPage(28)
                    ->setPageRange(4);
            
            $aData['rows'] = Model_DbTable_Product::getInstance()->getByCategoryThreeSelect($aSubcategory['id'], $aParametized['ids'], $aParams)->query()->fetchAll(Zend_Db::FETCH_ASSOC);
            $aData['back'] = $aData['page'] > 1;
            $aData['forward'] = count($aData['rows']) === $iCount;
        }




        $oCtrl->view->categoriesTwo = Model_DbTable_CategoryTwo::getInstance()->getByCategoryOneReal($aSubcategory['one_id']);
        $oCtrl->view->categoriesThree = Model_DbTable_CategoryThree::getInstance()->getByIdCategoryTwo($aSubcategory['two_id']);
        $oCtrl->view->paginator = $oPaginator;
        $oCtrl->view->data = $aData;

        $oCtrl->view->params = $aParams;
        $oCtrl->view->actual = $aSubcategory;
        $oCtrl->view->tags = $this->rabatTags();
        $oCtrl->view->similiars = Model_Logic_Search::getInstance()->similiars($aSubcategory['name']);
        $oCtrl->view->filters = $this->buildFilter($aFilters, array());
    }

    public function manageSubcategoryExtra(App_Controller_Page_Abstract $oCtrl) {
        if (($sUrl = $oCtrl->getRequest()->getParam('url', null)) === null) {
            throw new Exception('Account id failed', 500);
        }

        if (($aSubcategory = Model_DbTable_CategoryThree::getInstance()->getByUrl($sUrl)) == false) {
            throw new Exception('Category failed', 404);
        }

        $aParams = array();
        $aParams['url'] = $sUrl;

        $aParams['sort'] = $oCtrl->getRequest()->getParam('sort', null);
        $aParams['page'] = $oCtrl->getRequest()->getParam('page', null);

        $aFilters = Model_DbTable_Filter::getInstance()->getByCategoryThree($aSubcategory['id']);

        //pobierz 100 produktow po popularnosci
        $oSelect = Model_DbTable_Product::getInstance()->getByCategoryThreeSelect($aSubcategory['id'], $aParams);


        $aIds = $oSelect->order('id ASC')->query()->fetchAll(Zend_Db::FETCH_COLUMN, 0);


        $oPaginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($oSelect->query()->fetchAll(Zend_Db::FETCH_ASSOC)));
        $oPaginator->setCurrentPageNumber($oCtrl->getRequest()->getParam('page', 1))
                ->setPageRange(4);

        $oCtrl->view->categoriesTwo = Model_DbTable_CategoryTwo::getInstance()->getByCategoryOne($aSubcategory['one_id']);
        $oCtrl->view->categoriesThree = Model_DbTable_CategoryThree::getInstance()->getByIdCategoryTwo($aSubcategory['two_id']);
        $oCtrl->view->cities = Model_Logic_City::getInstance()->getList();
        $oCtrl->view->paginator = $oPaginator;
        $oCtrl->view->ids = $aIds;
        $oCtrl->view->params = $aParams;
        $oCtrl->view->actual = $aSubcategory;
        $oCtrl->view->filters = $this->buildFilter($aFilters, $aIds);
    }

    public function manageGroup(App_Controller_Page_Abstract $oCtrl) {
        if (($sUrl = $oCtrl->getRequest()->getParam('url', null)) === null) {
            throw new Exception('Account id failed', 500);
        }

        if (($aSubcategory = Model_DbTable_CategoryTwo::getInstance()->getByUrl($sUrl)) == false) {
            throw new Exception('Category failed', 404);
        }


        $aParams = array();
        $aParams['url'] = $sUrl;
        $aParams['page'] = $oCtrl->getRequest()->getParam('page', null);
        $aParams['filters'] = $this->buildFilterArray($oCtrl->getRequest()->getParam('filters', null));

        // $aFilters = Model_DbTable_Filter::getInstance()->getProducentsForGroup(explode(',', $aSubcategory['ids']));


        $oSelect = Model_DbTable_Product::getInstance()->getByCategoryTwoSelect(explode(',', $aSubcategory['ids']), $aParams);

        $aIds = $oSelect->order('id ASC')->query()->fetchAll(Zend_Db::FETCH_COLUMN, 0);


        $oPaginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($oSelect));
        $oPaginator->setCurrentPageNumber($oCtrl->getRequest()->getParam('page', 1))
                ->setItemCountPerPage(20)
                ->setPageRange(4);


        $oCtrl->view->categoriesTwo = Model_DbTable_CategoryTwo::getInstance()->getByCategoryOneReal($aSubcategory['one_id']);
        $oCtrl->view->subcategories = Model_DbTable_CategoryThree::getInstance()->getByIdCategoryTwoWithCount($aSubcategory['id']);
        $oCtrl->view->cities = Model_Logic_City::getInstance()->getList();
        $oCtrl->view->paginator = $oPaginator;
        $oCtrl->view->ids = $aIds;
        $oCtrl->view->params = $aParams;
        $oCtrl->view->actual = $aSubcategory;
        $oCtrl->view->tags = $this->rabatTags();
        $oCtrl->view->similiars = Model_Logic_Search::getInstance()->similiars($aSubcategory['name']);
    }

    private function buildFilter($aFilters, $iIds) {

        if (empty($aFilters)) {
            return array();
        }

        $aReturn = array();
        $aBuild = array(0 => array('name' => 'Cena', 'elems' => array()), 1 => array('name' => 'Producent', 'elems' => array()));

        foreach ($aFilters as $aFilter) {

            $aReturn[$aFilter['id']]['name'] = $aFilter['name'];

            $sCount = empty($aFilter['products']) ? 0 : count(array_intersect(explode(',', $aFilter['products']), $iIds));

            $aReturn[$aFilter['id']]['elems'][] = array('filter_id' => $aFilter['id'], 'id' => $aFilter['param_id'], 'name' => $aFilter['param_name'], 'count' => $sCount);
        }

        foreach ($aReturn as $aElem) {
            switch ($aElem['name']) {
                case 'Cena':
                    $aBuild[0] = $aElem;
                    break;
                case 'Producent':
                    $aBuild[1] = $aElem;
                    break;
                default:
                    $aBuild[] = $aElem;
                    break;
            }
        }


        return $aBuild;
    }

    private function buildProducentFilter($aFilters, $iIds) {
        $aReturn = array();

        foreach ($aFilters as $aFilter) {

            $aReturn[0]['name'] = 'Producent';

            $sCount = empty($aFilter['products']) ? 0 : count(array_intersect(explode(',', $aFilter['products']), $iIds));


            if (isset($aReturn[0]['elems'][$aFilter['param_name']])) {
                $aReturn[0]['elems'][$aFilter['param_name']]['count'] += $sCount;
            } else {

                $aReturn[0]['elems'][$aFilter['param_name']] = array('id' => $aFilter['param_id'], 'name' => $aFilter['param_name'], 'count' => $sCount);
            }
        }

        return $aReturn;
    }

    public function manageRedirectId(App_Controller_Page_Abstract $oCtrl) {


        $iId = $this->required('id');
        $sHash = $this->required('hash');
        $aProduct = Model_DbTable_Product::getInstance()->getById($iId);

        $aActual = Model_DbTable_Product::getInstance()->getCategoryThreeForProduct($iId);

        $oCtrl->view->category_one = Model_DbTable_CategoryOne::getInstance()->getList()->query()->fetchAll(Zend_Db::FETCH_ASSOC);
        $oCtrl->view->category_two = Model_DbTable_CategoryTwo::getInstance()->getByIdCategoryOne($aActual['one_id']);
        $oCtrl->view->category_three = Model_DbTable_CategoryThree::getInstance()->getByIdCategoryTwo($aActual['two_id']);
        $oCtrl->view->producents = Model_DbTable_Filter::getInstance()->getProducent($aActual['three_id']);


        $oCtrl->view->tags = $this->rabatTags();
        $oCtrl->view->similiarsProducts = $this->similiarsGrid($iId, $aActual['three_id']);
        $oCtrl->view->actual = $aActual;
        $oCtrl->view->product = $aProduct;
        $oCtrl->view->iframe = 'http://www.nokaut.pl/Click/offer/?click=' . $sHash . '&ref=' . urlencode("http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
        App_Addons::getInstance()->add('raty');
    }

    public function manageRedirectName(App_Controller_Page_Abstract $oCtrl) {


        $iId = $this->required('id');
        $aProduct = Model_DbTable_Product::getInstance()->getById($iId);

        $aActual = Model_DbTable_Product::getInstance()->getCategoryThreeForProduct($iId);

        $oCtrl->view->category_one = Model_DbTable_CategoryOne::getInstance()->getList()->query()->fetchAll(Zend_Db::FETCH_ASSOC);
        $oCtrl->view->category_two = Model_DbTable_CategoryTwo::getInstance()->getByIdCategoryOne($aActual['one_id']);
        $oCtrl->view->category_three = Model_DbTable_CategoryThree::getInstance()->getByIdCategoryTwo($aActual['two_id']);
        $oCtrl->view->producents = Model_DbTable_Filter::getInstance()->getProducent($aActual['three_id']);
        $oCtrl->view->tags = $this->rabatTags();
        $oCtrl->view->similiarsProducts = $this->similiarsGrid($iId, $aActual['three_id']);
        $oCtrl->view->iframe = $this->iframedProduct($aProduct, $oCtrl->view->similiarsProducts, false);



        $oCtrl->view->actual = $aActual;
        $oCtrl->view->product = $aProduct;
        App_Addons::getInstance()->add('raty');
    }

    private function iframedProduct($aProduct, $aSilimiars, $repeated = 0) {
        

        
        $iRandomPool = 4;
        $iRepeatLimit =  6; 
        
        if($repeated >= $iRepeatLimit)
        {
            return 'http://nokaut.pl'; 
        }

        
        if (($sIframe = $this->getCache('long')->load(md5('iframed_' . $aProduct['name']))) === false) {
            
            $aData = Model_Logic_Nokaut_Api::getInstance()->manageShopsByProductName($aProduct['name']);
            $aData = array_values($aData); 
            
            var_dump('Pętla nr' . $repeated); 
            var_dump($aProduct['name']); 
            var_dump($aData); 
            
            if(isset($aData[0]))
            {
                   $aData = array_slice($aData, 0, $iRandomPool);
                   shuffle($aData);
                    $aScore = array();
                   preg_match('/\?click=(.*?)\'+/', $aData[0]['direct_click_url'], $aScore);
                    $sIframe = 'http://www.nokaut.pl/Click/offer/?click=' .
                    $aScore[1] .
                    '&ref=' . urlencode("http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);


            $this->getCache('long')->save($sIframe, md5('iframed_' . $aProduct['name']));
              return $sIframe; 
              
            }
            else
            {                      
             if($repeated == 0)
             {
                    $aProductToPick = array();

                foreach ($aSilimiars as $aSimilar) {

                    $iKey = abs(($aProduct['min_price'] - $aSimilar['min_price']) * 100);
                    $aProductToPick[$iKey] = $aSimilar;
                }

                ksort($aProductToPick);
             }
             else
             {
                 $aProductToPick = $aSilimiars; 
             }
             
             unset($aProductToPick[0]); 
             $aProductToPick = array_values($aProductToPick);
             
             return $this->iframedProduct($aProductToPick[0], $aProductToPick, $repeated + 1); 
            }
        }
        return $sIframe;
    }

    public function manageDetails(App_Controller_Page_Abstract $oCtrl) {

        $iId = $this->required('id');
        $aProduct = Model_DbTable_Product::getInstance()->getDetailedById($iId);

        $aActual = Model_DbTable_Product::getInstance()->getCategoryThreeForProduct($iId);

        $aParams = array();
        $aParams['url'] = $this->seo($aActual['three_name']);
        $aParams['filters'] = $this->buildFilterArray($oCtrl->getRequest()->getParam('filters', null));
        $aParams['page'] = $oCtrl->getRequest()->getParam('page', null);

        if (!empty($aParams['page'])) {
            $aParams['page'] = str_replace('p/', '', $aParams['page']);
        }



        $oCtrl->view->category_one = Model_DbTable_CategoryOne::getInstance()->getList()->query()->fetchAll(Zend_Db::FETCH_ASSOC);
        $oCtrl->view->category_two = Model_DbTable_CategoryTwo::getInstance()->getByIdCategoryOne($aActual['one_id']);
        $oCtrl->view->category_three = Model_DbTable_CategoryThree::getInstance()->getByIdCategoryTwo($aActual['two_id']);

        $oCtrl->view->producent = Model_DbTable_Filter::getInstance()->getActualProducent($iId);
        $oCtrl->view->producents = Model_DbTable_Filter::getInstance()->getProducentByCategory($aActual['three_id']);

        $aFilters = Model_DbTable_Filter::getInstance()->getByCategoryThree($aActual['three_id']);

        $oCtrl->view->producents = Model_DbTable_Filter::getInstance()->getProducent($aActual['three_id']);
        $oCtrl->view->tags = $this->rabatTags();
        $oCtrl->view->similiarsProducts = $this->similiarsGrid($iId, $aActual['three_id']);
        $oCtrl->view->actual = $aActual;
        $oCtrl->view->product = $aProduct;
        $oCtrl->view->similiars = Model_DbTable_GoogleResults::getInstance()->similiar($aProduct['name']);
        $oCtrl->view->filters = $this->buildFilter($aFilters, array($aProduct['id']));
        $oCtrl->view->params = $aParams;

        App_Addons::getInstance()->add('raty');
    }

    private function similiarsGrid($iProductId, $iCategoryThreeId) {
        if (($aData = $this->getCache()->load(md5('sim' . $iProductId . $iCategoryThreeId))) === false) {
            $aData = Model_DbTable_Product::getInstance()->getSimiliarProductsBySameParams($iProductId);
            $aProductsCount = count($aData);

            if ($aProductsCount < 20) {
                $aData = array_merge($aData, Model_DbTable_Product::getInstance()->getByCategoryThreeSelect($iCategoryThreeId)
                                ->limit(20 - $aProductsCount)
                                ->query()
                                ->fetchAll(Zend_Db::FETCH_ASSOC));
            }

            shuffle($aData);
            $this->getCache()->save($aData, md5('sim' . $iProductId . $iCategoryThreeId), array(), 3600 * 24);
        }
        return $aData;
    }

    /**
      public function manageSimiliars(App_Controller_Page_Abstract $oCtrl) {

      $iId = $this->required('id');

      if (($aData = $this->getCache('long')->load(md5('product_' . $iId))) === false) {

      $aRandomReduce = array(6, 8, 9, 13, 15, 22, 14, 18, 11, 3, 28);

      $aParameters = Model_DbTable_ProductParameter::getInstance()->getByProductId($iId);

      if (!empty($aParameters)) {
      $aProducts = Model_DbTable_ProductParameter::getInstance()->getByParameterId($aParameters[array_rand($aParameters)]);
      shuffle($aProducts);
      $aIds = array_slice($aProducts, 0, 15);
      $aData = Model_DbTable_Product::getInstance()->getByIds($aIds);

      foreach ($aData as $iKey => $aProduct) {
      if (rand(1, 3) == 3) {
      $aData[$iKey]['promo'] = $aRandomReduce[array_rand($aRandomReduce)];
      } else
      $aData[$iKey]['promo'] = false;
      }
      } else
      $aData = array();
      $this->getCache('long')->save($aData, md5('product_' . $iId));
      }

      $oCtrl->view->similiars = $aData;
      }
     */
    public function getRandomPromoted($iLimit) {
        $aProducts = Model_DbTable_Product::getInstance()->getByPromotedCategory();
        shuffle($aProducts);
        if (!empty($aProducts)) {

            return Model_DbTable_Product::getInstance()->getByIds(array_slice($aProducts, 0, $iLimit));
        } else {
            return array();
        }
    }

    private function convertFitersFromQueryToArray($sFilters) {

        $aReturn = array();

        if (!empty($sFilters)) {


            $sFilters = substr($sFilters, 2);

            $aPairs = explode(',', $sFilters);
            foreach ($aPairs as $aPair) {
                $aElems = explode('-', $aPair);
                $aReturn[$aElems[0]] = $aElems[1];
            }
            return $aReturn;
        } else {
            return null;
        }
    }

    public function rabatTags() {
        if (($aRabatTags = $this->getCache()->load(md5('rabattags'))) === false) {
            for ($i = 0; $i < 20; $i++) {

                $aRabatTags[] = ($i % 3 == 0) ? rand(5, 30) : null;
            }
            $this->getCache()->save($aRabatTags, md5('rabattags'), array(), 3600 * 24);
        }
        return $aRabatTags;
    }

    public function buildFilterArray($sFilters) {
        $aReturn = array();



        if (!empty($sFilters)) {
            $sFilters = substr($sFilters, 2);


            $aPairs = explode(',', $sFilters);
            foreach ($aPairs as $aElem) {
                $aFragments = explode('-', $aElem);
                if (!empty($aFragments[1])) {
                    $aReturn[$aFragments[0]] = $aFragments[1];
                }
            }
        }

        return $aReturn;
    }

    public function managetShopsForProduct(App_Controller_Page_Abstract $oCtrl) {

        $sName = $oCtrl->getRequest()->getParam('name');
        $iId = $this->required('id');

        if (($aData = $this->getCache()->load(md5('shops' . $sName))) === false) {
            $aData = Model_Logic_Nokaut_Api::getInstance()->manageShopsByProductName($sName);
            $this->getCache()->save($aData, md5('shops' . $sName), array(), 3600 * 24);
        }

        return array('content' => $oCtrl->view->partial('_partial/prodshops.phtml', array('shops' => $aData, 'id' => $iId, 'tags' => $this->rabatTags())));
    }

}
