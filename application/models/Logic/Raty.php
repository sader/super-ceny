<?php

/**
 * Model_Logic_Admin
 *
 * @author Krzysztof Janicki
 */
class Model_Logic_Raty extends Model_Logic_Abstract {

    
    
    /**
     * Instancja klasy.
     * 
     * @var Model_Logic_Raty
     */
    static private $_oInstance;

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_Logic_Raty
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function manageAdd(App_Controller_Abstract $oCtrl) {

       if (($iId = $oCtrl->getRequest()->getParam('id')) === null) {
            throw new Exception('Błąd identyfikatora');
        }
        
          if (($iScore = $oCtrl->getRequest()->getParam('score')) === null) {
            throw new Exception('Błąd oceny');
        }
        
        $aScore = Model_DbTable_Raty::getInstance()->getRate($iId); 
       
        if(empty ($aScore))
        {
            Model_DbTable_Raty::getInstance()->add($iId, $iScore); 
        }
        else
        {
            $aData = array(); 
            $aData['votes'] = $aScore['votes'] + 1; 
            $aData['sum'] = $aScore['sum'] + $iScore; 
            $aData['actual'] =  round($aData['sum'] / $aData['votes'], 1); 
            Model_DbTable_Raty::getInstance()->update($aData, array('id_product = ?' => $aScore['id_product'])); 
        }
        
        return array('status' => true, 'message' => 'Dziękujemy za dodanie oceny'); 
        
        
    }
    
    
    public function manageAddRaw($iId, $iScore) {

        
        $aScore = Model_DbTable_Raty::getInstance()->getRate($iId); 
       
        if(empty ($aScore))
        {
            Model_DbTable_Raty::getInstance()->add($iId, $iScore); 
        }
        else
        {
            $aData = array(); 
            $aData['votes'] = $aScore['votes'] + 1; 
            $aData['sum'] = $aScore['sum'] + $iScore; 
            $aData['actual'] =  round($aData['sum'] / $aData['votes'], 1); 
            Model_DbTable_Raty::getInstance()->update($aData, array('id_product = ?' => $aScore['id_product'])); 
        }
        
        
        
    }


}