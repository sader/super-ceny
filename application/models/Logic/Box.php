<?php

/**
 * Model_Logic_Filter
 *
 * @author Krzysztof Janicki
 */
class Model_Logic_Box extends Model_Logic_Abstract {

    /**
     * Instancja klasy.
     * @var Model_Logic_Box
     */
    static private $_oInstance;

    /**
     * Zwraca instancje klasy.
     * 
     * @return  Model_Logic_Box
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function mainPage(App_Controller_Page_Abstract $oCtrl) {

            
        if (($aUrls = $this->getCache()->load('main_page')) === false) {

            $aLastSearches = $this->lastSearches(5, $oCtrl);
            $aRandomSearches = $this->randomSearches(15, $oCtrl);
            $aRandomSubcategories = $this->randomSubcategories(10, $oCtrl);
            $aRandomProductsInPromotedSubcategories = $this->randomProductsInPromotedSubcategories(10, $oCtrl);

            $aUrls = array_merge($aLastSearches, $aRandomSearches, $aRandomSubcategories, $aRandomProductsInPromotedSubcategories);

            shuffle($aUrls);
            $this->getCache()->save($aUrls, 'main_page');
        }

        $oCtrl->view->box = $aUrls;
    }

    public function category(App_Controller_Page_Abstract $oCtrl) {
        
         
        $sUrl = $oCtrl->getRequest()->getParam('url');

        if (($aUrls = $this->getCache()->load(md5('category_search_box_' . $sUrl))) === false) {

            $aLinks = $this->groupSubcategoriesWithFilters($sUrl);
            $aPromotedSubcategoriesRandomFilter = $this->promotedSubcategoriesRandomFilter(7, $oCtrl, $aLinks['rest']);
            $aPromotedSubcategoriesProducentFilter = $this->promotedSubcategoriesProducentFilter(5, $oCtrl, $aLinks['producent']);
            $aPromotedSubcategoriesCityFilter = $this->promotedSubcategoriesCityFilter(2, $oCtrl, $aLinks['subcategory']);
            $aLastSearches = $this->lastSearches(5, $oCtrl);
            $aRandomSearches = $this->randomSearches(12, $oCtrl);

            $aUrls = array_merge($aPromotedSubcategoriesRandomFilter, $aPromotedSubcategoriesProducentFilter, $aPromotedSubcategoriesCityFilter, $aLastSearches, $aRandomSearches);
            
            
            
            shuffle($aUrls);
            $this->getCache()->save($aUrls, md5('category_search_box_' . $sUrl));
        }

        $oCtrl->view->box = $aUrls;
    }

    public function group(App_Controller_Page_Abstract $oCtrl) {

         
        $sGroupUrl = $oCtrl->getRequest()->getParam('url');
        $sGroupPage = $oCtrl->getRequest()->getParam('page' , 1);
        $aCategory = Model_DbTable_CategoryTwo::getInstance()->getByUrl($sGroupUrl);
        $sUrl = $aCategory['one_name'];

        if (($aUrls = $this->getCache()->load(md5('group_box_' . $sUrl.$sGroupPage))) === false) {

            $aRandomSearches = $this->randomSearches(6, $oCtrl);
            $aRandomProductsFromGroup = $this->randomProductsForGroup(6, $aCategory['id'], $oCtrl); 
          //  $aRandomSubcategoriesWithProducents = $this->randomSubcategoriesWithProducents(4, $aCategory['id'], $oCtrl); 
         //   $aRandomSubcategoriesWithFilters = $this->randomSubcategoriesWithFilters(4, $aCategory['id'], $oCtrl); 
            
            
            $aUrls = array_merge($aRandomSearches, $aRandomProductsFromGroup);

            shuffle($aUrls);
            $this->getCache()->save($aUrls, md5('group_box_' . $sUrl.$sGroupPage), array(), 3600 * 5);
        }

        $oCtrl->view->box = $aUrls;
    }

    public function subcategory(App_Controller_Page_Abstract $oCtrl) {
        
         
       $sGroupUrl = $oCtrl->getRequest()->getParam('url');
        $sGroupPage = $oCtrl->getRequest()->getParam('page' , 1);
        $aCategoryThree = Model_DbTable_CategoryThree::getInstance()->getByUrl($sGroupUrl);
        $aCategory = Model_DbTable_CategoryTwo::getInstance()->getByIdCategoryThree($aCategoryThree['id']); 
        $sUrl = $aCategory['name'];
        $sTag = $oCtrl->getRequest()->getParam('page' , '');

        if (($aUrls = $this->getCache()->load(md5('group_box_' . $sUrl.$sGroupPage.$sTag))) === false) {

            $aRandomSearches = $this->randomSearches(6, $oCtrl);
            $aRandomProductsFromGroup = $this->randomProductsForGroup(6, $aCategory['id'], $oCtrl); 
           // $aRandomSubcategoriesWithProducents = $this->randomSubcategoriesWithProducents(4, $aCategory['id'], $oCtrl); 
           // $aRandomSubcategoriesWithFilters = $this->randomSubcategoriesWithFilters(4, $aCategory['id'], $oCtrl); 
            
            
            $aUrls = array_merge($aRandomSearches, $aRandomProductsFromGroup);

            shuffle($aUrls);
            $this->getCache()->save($aUrls, md5('group_box_' . $sUrl.$sGroupPage.$sTag), array(), 3600 * 5);
        }

        $oCtrl->view->box = $aUrls;
    }

    public function product(App_Controller_Page_Abstract $oCtrl) {
        
         
        $iId = $oCtrl->getRequest()->getParam('id'); 
        $aCategory = Model_DbTable_CategoryTwo::getInstance()->getByProductId($iId); 
        $sUrl = $aCategory['name'];
         $sTag = $oCtrl->getRequest()->getParam('page' , '');

        if (($aUrls = $this->getCache()->load(md5('group_box_' . $sUrl.$iId.$sTag))) === false) {

            $aRandomSearches = $this->randomSearches(6, $oCtrl);
            $aRandomProductsFromGroup = $this->randomProductsForGroup(6, $aCategory['id'], $oCtrl); 
           // $aRandomSubcategoriesWithProducents = $this->randomSubcategoriesWithProducents(4, $aCategory['id'], $oCtrl); 
          //  $aRandomSubcategoriesWithFilters = $this->randomSubcategoriesWithFilters(4, $aCategory['id'], $oCtrl); 
            
            
            $aUrls = array_merge($aRandomSearches, $aRandomProductsFromGroup);

            shuffle($aUrls);
            $this->getCache()->save($aUrls, md5('group_box_' . $sUrl.$iId.$sTag), array(), 3600 * 5);
        }

        $oCtrl->view->box = $aUrls;
    }


    public function search(App_Controller_Page_Abstract $oCtrl) {
        
         
        $sUri = $_SERVER['REQUEST_URI'];
         

        if (($aUrls = $this->getCache()->load(md5($sUri . '1'))) === false) {

            
            $aRandomSearches = $this->randomSearches(6, $oCtrl);
            $aRandomSubcategories = $this->randomSubcategories(4, $oCtrl);
            $aRandomProductsInPromotedSubcategories = $this->randomProductsInPromotedSubcategories(6, $oCtrl);
         //   $aRandomSubcategoriesWithFilters =  $this->randomSubcategoriesWithFilters(4, rand(1,22), $oCtrl);
        //    $aRandomSubcategoriesWithProducents =  $this->randomSubcategoriesWithProducents(4, rand(1,22), $oCtrl);

            
            
            $aUrls = array_merge($aRandomSearches,
                                 $aRandomSubcategories,
                             //    $aRandomSubcategoriesWithFilters,
                               
                                 $aRandomProductsInPromotedSubcategories
                              );
            shuffle($aUrls);
            
            
            
            $this->getCache()->save($aUrls, md5($sUri));
        }

        
        
        $oCtrl->view->box = $aUrls;
    }


    private function lastSearches($iNum, $oCtrl) {
        $aUrls = array();

        $aLastSearches = Model_DbTable_GoogleResults::getInstance()->getLast($iNum);

        foreach ($aLastSearches as $aUrl) {
            $aUrls[] = '<a href="' . $oCtrl->view->searchUrl(array('search' => $oCtrl->view->seo($aUrl['name']))) . '">' . $aUrl['name'] . '</a>';
        }

        return $aUrls;
    }

    private function randomSearches($iNum, $oCtrl) {

        $aUrls = array();
        $aRandomSearches = Model_DbTable_GoogleResults::getInstance()->random($iNum);
        foreach ($aRandomSearches as $aUrl) {
            $aUrls[] = '<a href="' . $oCtrl->view->searchUrl(array('search' => $oCtrl->view->seo($aUrl['name']))) . '">' . $aUrl['name'] . '</a>';
        }

        return $aUrls;
    }

    private function randomSubcategories($iNum, $oCtrl) {
        $aUrls = array();
        $aRandomCategories = Model_DbTable_CategoryThree::getInstance()->random($iNum);
        foreach ($aRandomCategories as $aUrl) {
            $aUrls[] = '<a href="' . $oCtrl->view->subcategoryUrl(array('url' => $aUrl['url'])) . '">' . $aUrl['name'] . '</a>';
        }
        return $aUrls;
    }

    private function randomProductsInPromotedSubcategories($iNum, $oCtrl) {
        $aUrls = array();
        $aProducts = Model_Logic_Product::getInstance()->getRandomPromoted($iNum);
      
        if(!empty($aProducts))
        {
        foreach ($aProducts as $aUrl) {
            $aUrls[] = '<a href="' . $oCtrl->view->url(array('id' => $aUrl['id'], 'url' => $this->seo($aUrl['name'])), 'product', false) . '">' . $aUrl['name'] . '</a>';
        }
        }
        return $aUrls;
    }

    private function groupSubcategoriesWithFilters($sUrl) {

        $aGroupedLinks = array('producent' => array() , 'rent' => array(), 'subcategory' => array());
        $aPromotedSubcategoriesForCategory = Model_DbTable_CategoryThree::getInstance()->getPromotedByCategoryOneUrl($sUrl);

        foreach ($aPromotedSubcategoriesForCategory as $aPromotedSubcategory) {

            if ($aPromotedSubcategory['filter_name'] == 'Producent') {
                $aGroupedLinks['producent'][] = $aPromotedSubcategory;
            } else {
                $aGroupedLinks['rest'][] = $aPromotedSubcategory;
            }

            if (!in_array($aPromotedSubcategory['name'], $aGroupedLinks['subcategory'])) {
                $aGroupedLinks['subcategory'][] = $aPromotedSubcategory['name'];
            }
        }
        return $aGroupedLinks;
    }

    private function promotedSubcategoriesRandomFilter($iNum, $oCtrl, $aLinks) {
        $aUrls = array();
        shuffle($aLinks);
        $aData = array_slice($aLinks, 0, $iNum);
        foreach ($aData as $aElem) {
            $aUrls[] = '<a href="' . $oCtrl->view->subcategoryUrl(array('url' => $aElem['url'], 'params' => array($aElem['filter_id'] => $aElem['parameter_id']))) . '">' . $aElem['name'] . ' ' . $aElem['parameter_name'] . '</a>';
        }

        return $aUrls;
    }

    private function promotedSubcategoriesProducentFilter($iNum, $oCtrl, $aLinks) {
        $aUrls = array();
        shuffle($aLinks);
        $aData = array_slice($aLinks, 0, $iNum);

        foreach ($aData as $aElem) {
            $aUrls[] = '<a href="' . $oCtrl->view->subcategoryUrl(array('url' => $aElem['url'], 'producent' => $oCtrl->view->seo($aElem['parameter_name']))) . '">' . $aElem['name'] . ' ' . $aElem['parameter_name'] . '</a>';
        }
        return $aUrls; 
    }


    private function promotedSubcategoriesCityFilter($iNum, $oCtrl, $aLinks) {
        $aUrls = array();
        shuffle($aLinks);
        $aData = array_slice($aLinks, 0, $iNum);
        $aCities = Model_Logic_City::getInstance()->getRandomLinks($iNum);

        foreach ($aData as $iKey => $aElem) {
            $aUrls[] = '<a href="' . $oCtrl->view->subcategoryUrl(array('url' => $aElem['url'], 'city' => $oCtrl->view->seo($aCities[$iKey]))) . '">' . $aElem['name'] . ' ' . $aCities[$iKey] . '</a>';
        }
        return $aUrls;
    }

    private function similiarSearches($iNum, $oCtrl, $sText) {
        $aUrls = array();

        $aSearches = Model_DbTable_Search::getInstance()->similiar($sText);

        if (!empty($aSearches)) {
            shuffle($aSearches);
            $aData = array_slice($aSearches, 0, $iNum);
            foreach ($aData as $aUrl) {
                $aUrls[] = '<a href="' . $oCtrl->view->searchUrl(array('search' => $aUrl['seo'])) . '">' . $aUrl['name'] . '</a>';
            }
        }

        return $aUrls;
    }
    
    private function randomProductsForGroup($iNum, $iCategoryId, $oCtrl)
    {
         $aUrls = array();
        $aSearches = Model_DbTable_Product::getInstance()->getByCategoryTwoSelect($iCategoryId)->query()->fetchAll(Zend_Db::FETCH_ASSOC); 

        if (!empty($aSearches)) {
            shuffle($aSearches);
            $aData = array_slice($aSearches, 0, $iNum);
            foreach ($aData as $aUrl) {
                $aUrls[] = '<a href="' . $oCtrl->view->url(array('url' => $aUrl['name'], 'id' => $aUrl['id']), 'product') . '">' . $aUrl['name'] . '</a>';
            }
        }
        return $aUrls;
    }
    
    private function randomSubcategoriesWithProducents($iNum, $iCategoryId, $oCtrl)
    {
        $aUrls = array(); 
        $aSubIds = array(); 
        $aSubcategories = Model_DbTable_CategoryThree::getInstance()->getFinishedByIdCategoryTwo($iCategoryId); 
        foreach($aSubcategories as $aSub)
        {
            $aSubIds[] = $aSub['id']; 
        }
        
        $aProducents = Model_DbTable_Filter::getInstance()->getProducentMultiple($aSubIds); 
        
        
        
        shuffle($aSubcategories); 
        shuffle($aProducents); 
        
        for($i = 0; $i < $iNum; $i++)
        {
            
           
           $aUrls[] = '<a href="'.$oCtrl->view->subcategoryUrl(array('url' => $aSubcategories[$i]['url'] , 'filters' => array($aProducents[$i]['id'] => $aProducents[$i]['filter_id']) )).'">' . $aSubcategories[$i]['name'].' '. $aProducents[$i]['name'] . '</a>';  
        }
        
     
        return $aUrls; 
        
    }
    
     private function randomSubcategoriesWithFilters($iNum, $iCategoryId, $oCtrl)
    {
        $aUrls = array(); 
        $aSubIds = array(); 
        $aSubcategories = Model_DbTable_CategoryThree::getInstance()->getFinishedByIdCategoryTwo($iCategoryId); 
        foreach($aSubcategories as $aSub)
        {
            $aSubIds[] = $aSub['id']; 
        }
        
        $aProducents = Model_DbTable_Filter::getInstance()->getFiltersMultiple($aSubIds); 
        
        
        shuffle($aSubcategories); 
        shuffle($aProducents); 
        
                
        for($i = 0; $i < $iNum; $i++)
        {
            
            
          $aUrls[] = '<a href="'.$oCtrl->view->subcategoryUrl(array('url' => $aSubcategories[$i]['url'] , 'filters' => array($aProducents[$i]['id'] => $aProducents[$i]['filter_id']) )).'">' . $aSubcategories[$i]['name'].' '. $aProducents[$i]['name'] . '</a>';  
        }
        
        return $aUrls; 
    }
    
}