<?php

/**
 * Model_Logic_Api
 *
 * @author Krzysztof Janicki
 */
class Model_Logic_Google_Parser extends Model_Logic_Abstract {

    /**
     * Instancja klasy.
     * 
     * @var Model_Logic_Google_Parser
     */
    static private $_oInstance;

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_Logic_Google_Parser
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function getOkazjeInfoWords($aWord, $sProxy) {
            
        
        
        
        require_once('phpQuery.php');
        $aReturn = array();
        
        for ($i = 0; $i < 5; $i++) {
            $iPage = $i * 100;

            $sUrl = sprintf('https://www.google.pl/search?q=site:okazje.info.pl/szukaj+%s&num=100&start=%s', urlencode($aWord['name']), $iPage);
           
            try
            {
            $aData = $this->_call($sUrl, $sProxy);
            }
            catch(Exception $e)
            {
                return 'Requested failed by proxy error'; 
            }

            $aElems = pq($aData)->find('#center_col h3.r a');


            foreach ($aElems as $aElem) {
                
                
                $sLink =  pq($aElem)->text();
                $aLinkParts = explode(' - spraw', $sLink); 
                
                
                if(isset($aLinkParts[1]) && !Model_DbTable_GoogleResults::getInstance()->exists(array('name' => $aLinkParts[0])))
                {
                $aReturn[] = array('id_name' => $aWord['id'], 'name' => $aLinkParts[0]); 
                }
            }
        }
        Model_DbTable_GoogleResults::getInstance()->multipleInsert($aReturn); 
    }

    private function _call($sCall, $sProxy) {

        list($sProxyAddr, $sProxyPort) = explode(':', $sProxy);

        $config = array(
            'adapter' => 'Zend_Http_Client_Adapter_Curl',
            'useragent' => 'Mozilla/5.0 (Windows; U; Windows NT 6.1; fr; rv:1.9.0.6) Gecko/2009011913 Firefox/3.0.6',
            'curloptions' => array(CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_TIMEOUT => 2500,
                CURLOPT_PROXY => $sProxyAddr.':'.$sProxyPort
              // CURLOPT_PORT => 
        ));
        

      /**  
        $config = array(
            'adapter' => 'Zend_Http_Client_Adapter_Proxy',
            'proxy_host' => $sProxyAddr,
            'proxy_port' => $sProxyPort,
        );
        */


        $oClient = new Zend_Http_Client(null, $config);
        $oClient->setUri($sCall);
        $oResponse = $oClient->request();

        if ($oResponse->isSuccessful()) {

            return phpQuery::newDocumentHTML($oResponse->getBody(), 'utf-8');
        } else
            throw new Exception($oResponse->getHeadersAsString(), 500);
    }

}
