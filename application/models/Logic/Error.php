<?php

/**
 * Model_Logic_Admin
 *
 * @author Krzysztof Janicki
 */
class Model_Logic_Error extends Model_Logic_Abstract {

    /**
     * Instancja klasy.
     * 
     * @var Model_Logic_Error
     */
    static private $_oInstance;

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_Logic_Error
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }
    
    
    public function manageSend(App_Controller_Page_Abstract $oCtrl)
    {
        $sContent = $oCtrl->getRequest()->getParam('content', null); 
        
        $sSource = $oCtrl->getRequest()->getParam('source', ''); 
        
        if($sContent !== null)
        {
            
            Model_DbTable_Error::getInstance()->add($sContent, $sSource); 
            
            return array('status' => true, 'message' => 'Dziękujemy za zgłoszenie błędu');
        }
        else  return array('status' => false, 'message' => 'Zgłoszenie nie jest poprawne'); 
        
        
    }
    
    public function manageList(App_Controller_Admin_Abstract $oCtrl)
    {
        $oGrid = new Admin_Grid_Errors(); 
        $oCtrl->view->list = $oGrid->deploy(); 
        $oCtrl->view->headScript()->appendFile('/scripts/admin/errors.js');
    }
    
       public function manageRemove(App_Controller_Admin_Abstract $oCtrl) {
        
            if (($iId = $oCtrl->getRequest()->getParam('id', null)) === null)
            throw new Exception('Account id failed', 500);

        Model_DbTable_Error::getInstance()->delete(array('id = ?' => $iId));

        return array('status' => true);
    }
    
}