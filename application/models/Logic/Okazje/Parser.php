<?php

/**
 * Model_Logic_NoakutApi
 *
 * @author Krzysztof Janicki
 */
class Model_Logic_Okazje_Parser extends Model_Logic_Abstract {

    private $_sMainPageUrl = 'http://www.okazje.info.pl/';

    /**
     * Instancja klasy.
     * 
     * @var Model_Logic_Okazje_Parser 
     */
    static private $_oInstance;

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_Logic_Okazje_Parser
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function getMainCategories() {


        require('phpQuery.php');


        $aData = $this->_call($this->_sMainPageUrl . 'mapa-kategorii.html');

        $aBoxes = pq($aData)->find('.tree');

        foreach ($aBoxes as $aBox) {

            $aCatTwoElems = pq($aBox)->find('h3');
            foreach ($aCatTwoElems as $aCatTwo) {

                $aData = array();
                $aData['level'] = 1;
                $aData['name'] = pq($aCatTwo)->find('a')->attr('title');
                $aData['href'] = pq($aCatTwo)->find('a')->attr('href');
                $aData['seo'] = $this->seo($aData['name']);
                $aData['parent_id'] = 0;
                Model_DbTable_CategoryInfo::getInstance()->add($aData);
            }
        }
    }

    public function manageCategory() {


        require('phpQuery.php');


        $aJob = Model_DbTable_CategoryInfo::getInstance()->getJob();


        $iParentId = $aJob['id'];

        $aData = $this->_call($aJob['href'], array(), array(), false);


        $sDesc = pq($aData)->find('#ye1')->html();




        Model_DbTable_CategoryInfo::getInstance()->update(array('description' => preg_replace('/<img.*?>/', '', $sDesc)), array('id = ?' => $aJob['id']));


        $aLinks = pq($aData)->find('.linkedSearchedLinks')->find('a');


        if (!empty($aLinks)) {
            foreach ($aLinks as $aLink) {
                $sName = pq($aLink)->text();

                if (!Model_DbTable_InfoSearch::getInstance()->exists(array('name' => $sName))) {
                    Model_DbTable_InfoSearch::getInstance()->insert(array('name' => $sName, 'url' => $this->seo($sName)));
                }
            }
        }
        if ($aJob['level'] == 1) {
            $aRows = pq($aData)->find('.tree')->find('div');

            foreach ($aRows as $aRow) {

                $aRowElems = pq($aRow)->children();
                foreach ($aRowElems as $aRowElem) {

                    if (pq($aRowElem)->is('h2')) {
                        $aData = array();
                        $aData['level'] = 2;
                        $aData['name'] = pq($aRowElem)->find('a')->attr('title');
                        $aData['href'] = pq($aRowElem)->find('a')->attr('href');
                        $aData['seo'] = $this->seo($aData['name']);
                        $aData['parent_id'] = $iParentId;
                        $iActualCategoryTwo = Model_DbTable_CategoryInfo::getInstance()->add($aData);
                    }


                    if (pq($aRowElem)->is('h3')) {


                        $aData = array();
                        $aData['level'] = 3;
                        $aData['name'] = pq($aRowElem)->find('a')->attr('title');
                        $aData['href'] = pq($aRowElem)->find('a')->attr('href');
                        $aData['seo'] = $this->seo($aData['name']);
                        $aData['parent_id'] = $iActualCategoryTwo;
                        Model_DbTable_CategoryInfo::getInstance()->add($aData);
                    }
                }
            }
        }
    }

    private function _call($sCall, $aPost = array(), $aCookies = array(), $bStaticDecode = true) {

        $oClient = new Zend_Http_Client(null, array('timeout' => 30));
        $oClient->setUri($sCall);

        if (!empty($aPost)) {
            foreach ($aPost as $sName => $sValue) {
                $oClient->setParameterPost($sName, $sValue);
            }
        }

        if (!empty($aCookies)) {


            foreach ($aCookies as $sName => $sValue) {
                $oClient->setCookie($sName, $sValue);
            }
        }

        $oResponse = $oClient->request(Zend_Http_Client::POST);


        if ($oResponse->isSuccessful()) {

            $oResp = $oResponse->getBody();

            if ($bStaticDecode) {
                $oResp = $this->toUtf($oResp);
            }


            $htmlCode = mb_convert_encoding($oResp, 'HTML-ENTITIES', "UTF-8");

            return phpQuery::newDocumentHTML($htmlCode, 'utf-8');
        } else {
            throw new Exception($sCall.'::::'.$oResponse->getHeadersAsString(), 500);
        }
    }

    private function toUtf($txt) {
        $txt = str_replace(chr(0xB1), 'ą', $txt);
        $txt = str_replace(chr(0xE6), 'ć', $txt);
        $txt = str_replace(chr(0xEA), 'ę', $txt);
        $txt = str_replace(chr(0xB3), 'ł', $txt);
        $txt = str_replace(chr(0xF1), 'ń', $txt);
        $txt = str_replace(chr(0xF3), 'ó', $txt);
        $txt = str_replace(chr(0xB6), 'ś', $txt);
        $txt = str_replace(chr(0xBC), 'ź', $txt);
        $txt = str_replace(chr(0xBF), 'ż', $txt);
        $txt = str_replace(chr(0xA1), 'Ą', $txt);
        $txt = str_replace(chr(0xC6), 'Ć', $txt);
        $txt = str_replace(chr(0xCA), 'Ę', $txt);
        $txt = str_replace(chr(0xA3), 'Ł', $txt);
        $txt = str_replace(chr(0xD1), 'Ń', $txt);
        $txt = str_replace(chr(0xD3), 'Ó', $txt);
        $txt = str_replace(chr(0xA6), 'Ś', $txt);
        $txt = str_replace(chr(0xAC), 'Ź', $txt);
        $txt = str_replace(chr(0xAF), 'Ż', $txt);


        return $txt;
    }

}
