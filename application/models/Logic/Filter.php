<?php

/**
 * Model_Logic_Filter
 *
 * @author Krzysztof Janicki
 */
class Model_Logic_Filter extends Model_Logic_Abstract {

    /**
     * Instancja klasy.
     * 
     * @var Model_Logic_Filter
     */
    static private $_oInstance;

    /**
     * Zwraca instancje klasy.
     * 
     * @return  Model_Logic_Filter 
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function manageDisplay(App_Controller_Admin_Abstract $oCtrl) {
        if (($iId = $oCtrl->getRequest()->getParam('id', null)) === null)
            throw new Model_Logic_Exception('Account id failed', 500);

        $aFilters = Model_DbTable_Filter::getInstance()->getByCategory($iId);

        return array('content' => $oCtrl->view->partial('filter/display.phtml', array('filters' => $aFilters)));
    }

    public function getProducentParamId($iId, $sName) {
        $aData = Model_DbTable_Filter::getInstance()->getProducent($iId);

        foreach ($aData as $aRow) {
            if ($sName == $this->seo($aRow['name'])) {
                return $aRow['id'];
            }
        }

        return false; 
    }

     public function getProducentParamIdMultiple($aIds, $sName) {
        $aData = Model_DbTable_Filter::getInstance()->getProducentMultiple($aIds);

        foreach ($aData as $aRow) {
            if ($sName == $this->seo($aRow['name'])) {
                return $aRow['id'];
            }
        }

        return false;
    }

     

     public function getProducentParamIdForSearch($aIds, $sName) {
        $aData = Model_DbTable_Filter::getInstance()->getProducentForSearch($aIds);

        foreach ($aData as $aRow) {
            if ($sName == $this->seo($aRow['name'])) {
                return $aRow['id'];
            }
        }

        return false;
    }
    
    
    public function buildFilterParametersForListing($aParams, $aSubcategory)
    {
      
       
        
        if(!empty($aParams['filters']))
        {
           
            
            $aFilters = array(); 
            foreach($aParams['filters'] as $iFilterId => $iParamId)
            {
                if(!in_array($iFilterId , array('pf' , 'pt')))
                {
                $aFilters[] = $iFilterId.'-'.$iParamId; 
                }
            }
            
            if(empty($aFilters)) return false; 
            
            
            $aDbResult = Model_DbTable_Filter::getInstance()->getFilterWithParams($aFilters);
            
            //tu ta pojebana logika
            $sCallLink =  Model_Logic_Skapiec_Api::getInstance()->getByMergedFilterLinks($aDbResult, $aSubcategory); 
           
            
         return Model_Logic_Skapiec_Api::getInstance()->manageListForCategoryWithFilters($aSubcategory, $sCallLink, $aParams['page']);
        }
        else return false; 
        
      
    }
    
        

}