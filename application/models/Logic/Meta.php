<?php

/**
 * Model_Logic_Admin
 *
 * @author Krzysztof Janicki
 */
class Model_Logic_Meta extends Model_Logic_Abstract {

    /**
     * Instancja klasy.
     * 
     * @var Model_Logic_Meta
     */
    static private $_oInstance;

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_Logic_Meta
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function index(App_Controller_Page_Abstract $oCtrl) {
        $oCtrl->view->headTitle("Wyszukaj i kupuj najtaniej – porównywarka cena dobra-cena.pl");
        $oCtrl->view->headMeta()->appendName('description', "Dobra-cena.pl – najszybsza i najlepsza porównywarka cen. Wyszykuj, porównuj i
                                                             kupuj najtaniej. Najlepszy sposób na tanie zakupy.");
        $oCtrl->view->headMeta()->appendName('keywords', "kup tanio, porównywarka cen, dobre ceny");
    }

    public function category(App_Controller_Page_Abstract $oCtrl) {

        $sName = $oCtrl->view->category['name'];

        $oCtrl->view->headTitle("$sName – kupuj najtaniej w dobra-cena.pl");
        $oCtrl->view->headMeta()->appendName('description', "$sName - porównaj ceny i kupuj najtaniej. Znajdź idealny produkt dla siebie i kupuj 
            po dobrej cenie. Zobacz opisy $sName, opinie i recenzje. Kupuj tanio $sName w sklepach internetowych i stacjonarnie.");
        $oCtrl->view->headMeta()->appendName('keywords', "$sName, dobre ceny");
    }

    public function group(App_Controller_Page_Abstract $oCtrl) {

        $sOneName = $oCtrl->view->actual['one_name'];
        $sTwoName = $oCtrl->view->actual['name'];

        $oCtrl->view->headTitle("$sTwoName – kupuj najtaniej w dobra-cena.pl");
        $oCtrl->view->headMeta()->appendName('description', "$sTwoName porównaj ceny i kupuj najtaniej. Znajdź idealny produkt dla siebie i kupuj
                                                            po dobrej cenie. Zobacz opisy $sTwoName, opinie i recenzje. Kupuj tanio $sTwoName w sklepach internetowych i stacjonarnie.");
        $oCtrl->view->headMeta()->appendName('keywords', "$sOneName, $sTwoName, dobre ceny");
    }

    public function subcategory(App_Controller_Page_Abstract $oCtrl) {

        $sOneName = $oCtrl->view->actual['one_name'];
        $sTwoName = $oCtrl->view->actual['two_name'];
        $sThreeName = $oCtrl->view->actual['name'];

        $oCtrl->view->headTitle("$sThreeName – kupuj najtaniej w dobra-cena.pl");
        $oCtrl->view->headMeta()->appendName('description', "$sThreeName porównaj ceny i kupuj najtaniej. Znajdź idealny produkt dla siebie i kupuj
                                                              po dobrej cenie. Zobacz opisy $sThreeName, opinie i recenzje. Kupuj tanio $sTwoName w sklepach  internetowych i stacjonarnie.");
        $oCtrl->view->headMeta()->appendName('keywords', "$sOneName, $sTwoName, $sThreeName, dobre ceny");
    }

    public function subcategoryFilters(App_Controller_Page_Abstract $oCtrl) {


        $sFilterStr = '';
        $sParamsStr = '';
        $aIds = '';

        foreach ($oCtrl->view->params as $sName => $sParamId) {
            switch ($sName) {
                case 'pf':
                    $sFilterStr .= ' Cena od ' . $sParamId . ' zł';
                    break;
                case 'pt':
                    $sFilterStr .= ' Cena do ' . $sParamId . ' zł';
                    break;
                default:
                    $aIds[] = $sParamId;
                    break;
            }
        }

        if (!empty($aIds)) {
            $aFilters = Model_DbTable_Parameter::getInstance()->getParamNames($aIds);

            foreach ($aFilters as $aFilter) {
                $sFilterStr .= ' ' . $aFilter['filter'] . ' ' . $aFilter['parameter'];
                $sParamsStr .= ' ' . $aFilter['parameter'];
            }
        }

        $sOneName = $oCtrl->view->actual['one_name'];
        $sTwoName = $oCtrl->view->actual['two_name'];
        $sThreeName = $oCtrl->view->actual['name'];

        $oCtrl->view->headTitle("$sThreeName $sFilterStr – kupuj najtaniej w dobra-cena.pl");
        $oCtrl->view->headMeta()->appendName('description', "$sThreeName $sFilterStr porównaj ceny i kupuj najtaniej. Znajdź idealny
                                                              produkt dla siebie i kupuj po dobrej cenie. Zobacz opisy $sThreeName $sFilterStr . opinie i recenzje. Kupuj tanio $sTwoName w sklepach internetowych i stacjonarnie.");
        $oCtrl->view->headMeta()->appendName('keywords', "– $sOneName, $sTwoName, $sThreeName $sFilterStr, dobre ceny");
    }

    public function detail(App_Controller_Page_Abstract $oCtrl) {

        $sOneName = $oCtrl->view->actual['one_name'];
        $sTwoName = $oCtrl->view->actual['two_name'];
        $sThreeName = $oCtrl->view->actual['three_name'];
        $sProdName = $oCtrl->view->product['name'];


        $oCtrl->view->headTitle("$sProdName – kupuj najtaniej w dobra-cena.pl");
        $oCtrl->view->headMeta()->appendName('description', "$sProdName porównaj ceny i kupuj najtaniej $sThreeName. Znajdź idealny produkt dla siebie i
                                                            kupuj po dobrej cenie. Zobacz opisy $sProdName, opinie i recenzje. Kupuj tanio $sProdName w sklepach internetowych i stacjonarnie.");
        $oCtrl->view->headMeta()->appendName('keywords', "$sOneName, $sTwoName, $sThreeName $sProdName dobre ceny");
    }

    public function randomId(App_Controller_Page_Abstract $oCtrl) {

         $sOneName = $oCtrl->view->actual['one_name'];
        $sTwoName = $oCtrl->view->actual['two_name'];
        $sThreeName = $oCtrl->view->actual['three_name'];
        $sProdName = $oCtrl->view->product['name'];


        $oCtrl->view->headTitle("$sProdName – kupuj najtaniej w dobra-cena.pl");
        $oCtrl->view->headMeta()->appendName('description', "$sProdName porównaj ceny i kupuj najtaniej $sThreeName. Znajdź idealny produkt dla siebie i
                                                            kupuj po dobrej cenie. Zobacz opisy $sProdName, opinie i recenzje. Kupuj tanio $sProdName w sklepach internetowych i stacjonarnie.");
        $oCtrl->view->headMeta()->appendName('keywords', "$sOneName, $sTwoName, $sThreeName $sProdName dobre ceny");
    }

    public function randomName(App_Controller_Page_Abstract $oCtrl) {

         $sOneName = $oCtrl->view->actual['one_name'];
        $sTwoName = $oCtrl->view->actual['two_name'];
        $sThreeName = $oCtrl->view->actual['three_name'];
        $sProdName = $oCtrl->view->product['name'];


        $oCtrl->view->headTitle("$sProdName – kupuj najtaniej w dobra-cena.pl");
        $oCtrl->view->headMeta()->appendName('description', "$sProdName porównaj ceny i kupuj najtaniej $sThreeName. Znajdź idealny produkt dla siebie i
                                                            kupuj po dobrej cenie. Zobacz opisy $sProdName, opinie i recenzje. Kupuj tanio $sProdName w sklepach internetowych i stacjonarnie.");
        $oCtrl->view->headMeta()->appendName('keywords', "$sOneName, $sTwoName, $sThreeName $sProdName dobre ceny");
    }

    public function search(App_Controller_Page_Abstract $oCtrl) {

        $sSearch = $oCtrl->view->search;
        $oCtrl->view->headTitle("$sSearch - kupuj najtaniej w dobra-cena.pl");
        $oCtrl->view->headMeta()->appendName('description', "$sSearch porównaj ceny i kupuj najtaniej $sSearch");
        $oCtrl->view->headMeta()->appendName('keywords', "$sSearch dobre ceny");
    }

    public function subcategoryExtra(App_Controller_Page_Abstract $oCtrl) {

        $sExtra = $oCtrl->getRequest()->getParam('extra');
        $sName = $oCtrl->view->actual['name'];

        switch ($sExtra) {
            case 'tanie':
                $oCtrl->view->headTitle("Tanie $sExtra - kup najtaniej w dobra-cena.pl");
                $oCtrl->view->headMeta()->appendName('description', "Zobacz tanie $sName najniższe ceny, kup najtaniej, najtańsze produkty, opisy,  porównanie cen w sklepach internetowych, recenzje, zdjęcia i filmy w kategorii najtańsze $sName na dobra-cena.pl");
                break;
            case 'ranking':
                $aMonth = array(1 => 'styczeń', 2 => 'luty', 3 => 'marzec', 4 => 'kwieceń', 5 => 'maj', 6 => 'czerwiec', 7 => 'lipiec', 8 => 'sierpień', 9 => 'wrzesień', 10 => 'październik', 11 => 'listopad', 12 => 'grudzień');
                $oCtrl->view->headTitle("Popularne [nazwa], aktualny ranking " + $aMonth[date('n')] + "  " + date('Y') + ", co wybrać.");
                $oCtrl->view->headMeta()->appendName('description', "Zobacz dobre i najlepsze $sName, najnowszy ranking " + $aMonth[date('n')] + "  " + date('Y') + ", najpopularniejsze i najczęściej kupowane $sName, porównanie $sName, najniższe ceny, najtańsze produkty, opisy,  porównanie cen w sklepach internetowych, recenzje, zdjęcia i filmy w kategorii dobre $sName na dobra-cena.pl");
                break;
        }

        $aSubcategories = Model_DbTable_CategoryThree::getInstance()->getByIdCategoryThreeMeta($oCtrl->view->actual['id']);
        $aProducent = $this->producentNameByGroupId($oCtrl->view->actual['id']);

        $oCtrl->view->headLink(array('rel' => 'canonical', 'href' => $oCtrl->getRequest->getRequestUri()), 'APPEND');

        $oCtrl->view
                ->headMeta()
                ->appendName('keywords', sprintf('%s najniższe ceny, %s %s, %s %s, %s %s, %s', $sName, $sName, $aProducent[0], $sName, $aProducent[1], $sName, $aProducent[2], implode(', ', $aSubcategories)));

        $oCtrl->view->headMeta()->appendName('robots', 'index,follow');
    }

}
