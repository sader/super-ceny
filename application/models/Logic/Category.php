<?php

/**
 * Model_Logic_Category
 *
 * @author Krzysztof Janicki
 */
class Model_Logic_Category extends Model_Logic_Abstract {

    const PRODUCT_PER_PAGE = 40;

    /**
     * Instancja klasy.
     * 
     * @var Model_Logic_Category
     */
    static private $_oInstance;

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_Logic_Category
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function manageList(App_Controller_Admin_Abstract $oCtrl) {


        $oAdapter = Model_DbTable_CategoryOne::getInstance()->getList();

        $oPaginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($oAdapter));
        $oPaginator->setCurrentPageNumber($oCtrl->getRequest()->getParam('page', 1))
                ->setItemCountPerPage(40);

        $oCtrl->view->paginator = $oPaginator;
        App_Addons::getInstance()->add('DropZone');
        $oCtrl->view->headScript()->appendFile('/scripts/admin/categories.js');
    }

    public function manageSubcategoryTwo(App_Controller_Admin_Abstract $oCtrl) {
        if (($iId = $oCtrl->getRequest()->getParam('id', null)) === null)
            throw new Exception('Account id failed', 500);

        $aCategories = Model_DbTable_CategoryTwo::getInstance()->getByIdCategoryOne($iId);
        $oResp = array('content' => $oCtrl->view->partial('_partial/categoryTableTwo.phtml', array('categories' => $aCategories)));

        return $oResp;
    }

    public function manageSubcategoryThree(App_Controller_Admin_Abstract $oCtrl) {
        if (($iId = $oCtrl->getRequest()->getParam('id', null)) === null)
            throw new Exception('Account id failed', 500);

        $aCategories = Model_DbTable_CategoryThree::getInstance()->getByIdCategoryTwo($iId);

        $oResp = array('content' => $oCtrl->view->partial('_partial/categoryTableThree.phtml', array('categories' => $aCategories)));

        return $oResp;
    }

    public function manageCategoryList(App_Controller_Page_Abstract $oCtrl) {
  if (($sUrl = $oCtrl->getRequest()->getParam('url', null)) === null)
            throw new Exception('Account id failed', 500);

  
  
        $aCategoryOne = Model_DbTable_CategoryOne::getInstance()->getByUrl($sUrl);
        
        
        App_Addons::getInstance()->add('mansory');
        $aData = Model_DbTable_CategoryTwo::getInstance()->getByCategoryOne($aCategoryOne['id']);
          $aReturn = array();

        foreach($aData as $aRow)
        {
            $aReturn[$aRow['main_id']]['name'] = $aRow['main_name'];
            $aReturn[$aRow['main_id']]['seo'] = $aRow['main_seo'];
            $aReturn[$aRow['main_id']]['elems'][] = array('id' => $aRow['id'], 'name' => $aRow['name'], 'url' => $aRow['url'], 'count' => $aRow['count']);
        }
      
      $oCtrl->view->category = $aCategoryOne; 
      $oCtrl->view->category_one = Model_DbTable_CategoryOne::getInstance()->getList()->query()->fetchAll(Zend_Db::FETCH_ASSOC);
      $oCtrl->view->category_two = $aData;
      $oCtrl->view->categories = $aReturn;
       
    }

    public function manageSubcategoryList(App_Controller_Page_Abstract $oCtrl) {
      

        

    }

    public function manageRootCategoryList(App_Controller_Page_Abstract $oCtrl) {
      
    }

   
    public function manageThumb(App_Controller_Admin_Abstract $oCtrl) {

        if (($iId = $oCtrl->getRequest()->getParam('id', null)) === null)
            throw new Exception('Account id failed', 500);

        
                
        if (($iLevel = $oCtrl->getRequest()->getParam('level', null)) === null)
            throw new Exception('Level failed', 500);

        $sImage = App_Image::saveImageFromUpload('file');
        if ($sImage) {
            switch ($iLevel) {
                case 1:
                    Model_DbTable_CategoryOne::getInstance()->update(array('image' => $sImage), array('id = ?' => $iId));
                    break;
                case 2:
                    Model_DbTable_CategoryTwo::getInstance()->update(array('image' => $sImage), array('id = ?' => $iId));
                    break;
                case 3:
                    Model_DbTable_CategoryThree::getInstance()->update(array('image' => $sImage), array('id = ?' => $iId));
                    break;
            }
        }
        return App_Image::uri($sImage);
    }

    public function manageDescription(App_Controller_Admin_Abstract $oCtrl) {

        if (($iId = $oCtrl->getRequest()->getParam('id', null)) === null)
            throw new Exception('Account id failed', 500);

        $sDescription = $oCtrl->getRequest()->getParam('description', '');
        $sLevel = $oCtrl->getRequest()->getParam('level', null);


        switch ($sLevel) {
             case 1:
                Model_DbTable_CategoryOne::getInstance()->setDescription($iId, $sDescription);
                break;
            case 2:
                Model_DbTable_CategoryTwo::getInstance()->setDescription($iId, $sDescription);
                break;
            case 3:
                Model_DbTable_CategoryThree::getInstance()->setDescription($iId, $sDescription);
                break;

            default:
                throw new Exception('Wrong level', 500);
                break;
        }
    }

    public function manageCategoryGrid(App_Controller_Page_Abstract $oCtrl)
    {

        $aData = Model_DbTable_CategoryOne::getInstance()->getListGrid();

       $aReturn = array(); 

        foreach($aData as $aRow)
        {
            $aReturn[$aRow['main_id']]['name'] = $aRow['main_name'];
            $aReturn[$aRow['main_id']]['seo'] = $aRow['main_seo'];
            $aReturn[$aRow['main_id']]['elems'][] = array('id' => $aRow['id'], 'name' => $aRow['name'], 'url' => $aRow['url']);
        }
        
       $oCtrl->view->categories = $aReturn; 

    }
    
    public function categorySelectInput(App_Controller_Admin_Abstract $oCtrl)
    {
        $aReturn = array(); 
        $aCategories = Model_DbTable_CategoryThree::getInstance()->getDetailedList(); 
        
        foreach($aCategories as $aCat)
        {
            $aReturn[$aCat['two_name']][] = array('id' => $aCat['id'],  'name' => $aCat['name']); 
        }
        
       return $aReturn;  
        
        
        
    }

    
   
   

}