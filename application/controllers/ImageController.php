<?php

class ImageController extends Zend_Controller_Action {
    
    
    const IMAGE_WIDTH = 100; 
    const IMAGE_HEIGHT = 100; 
    
    public function imagesAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $sImage = $this->getRequest()->getParam('file');
        

        $sWidth = $this->getRequest()->getParam('width', self::IMAGE_WIDTH);
        $sHeight = $this->getRequest()->getParam('height', self::IMAGE_HEIGHT);

        $sImgPath = APPLICATION_PATH .
                DIRECTORY_SEPARATOR .
                '..' .
                DIRECTORY_SEPARATOR .
                'public_html' .
                DIRECTORY_SEPARATOR .
                'images' .
                DIRECTORY_SEPARATOR;
                

        if (file_exists($sImgPath . $sImage)) {
            include_once ('WideImage/WideImage.php');
            WideImage::loadFromFile($sImgPath . $sImage)->resize($sWidth, $sHeight)->output('jpg');
        }
    }
    
    
    public function imageAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $sImage = $this->getRequest()->getParam('file');
        $sWidth = $this->getRequest()->getParam('width');
        $sHeight = $this->getRequest()->getParam('width');
        $sExtension = $this->getRequest()->getParam('ext'); 

        
        $sUrl = $_SERVER['REQUEST_URI']; 
        

        $sImgPath = APPLICATION_PATH .
                DIRECTORY_SEPARATOR .
                '..' .
                DIRECTORY_SEPARATOR .
                'public_html'; 
             
                
        $sRequestedRealFile = $sImgPath . DIRECTORY_SEPARATOR . $sImage.'.'.$sExtension; 

        if (file_exists($sRequestedRealFile)) {
        
            $sWorkFile = $sRequestedRealFile; 
            $sSave = 1; 
        }
        else
        {
            $sWorkFile = $sImgPath . DIRECTORY_SEPARATOR .'images'.DIRECTORY_SEPARATOR.'logo'.DIRECTORY_SEPARATOR.'logo.png';
            $sSave = 0; 
        }   
            
            include_once ('WideImage/WideImage.php');
            
            $oFile = WideImage::loadFromFile($sWorkFile)->resizeDown($sWidth, $sHeight); 
            
            
            if($sSave)
            {
             $oSaved = clone($oFile); 
            //    $oSaved->saveToFile($sImgPath . DIRECTORY_SEPARATOR . $sUrl); 
                 $oFile->output($sExtension); 
            }
            else
            {
                $oFile->output('.png');  
            }
         
        
    }

}

