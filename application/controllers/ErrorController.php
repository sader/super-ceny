<?php

class ErrorController extends Zend_Controller_Action
{
    public function errorAction()
    {
        $errors = $this->_getParam('error_handler');
        $this->_helper->layout()->disableLayout(); 
        
        
          if ($log = $this->getLog()) {
            $log->log($errors->exception, Zend_Log::CRIT);
        }
        
        
        switch ($errors->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ROUTE:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
                $this->getResponse()->setHttpResponseCode(404);
                if(APPLICATION_ENV == 'production')
                {
              //      $this->_redirect('/'); 
                    echo $this->view->partial('error/404.phtml');
                    die();
                }
                else
                {
            // 404 error -- controller or action not found
                var_dump($errors);
                $this->view->message = 'Page not found';
                }
                break;
            default:
                
                $this->getResponse()->setHttpResponseCode(500);
                if(APPLICATION_ENV == 'production')
                {
               //      $this->_redirect('/'); 
                    echo $this->view->partial('error/404.phtml'); 
                    die();
                }
                else
                {
               echo '<pre>'.$errors->exception.'</pre>';
                die(); 
                $this->view->message = 'Application error';
                }
                break;
        }
        
        // Log exception, if logger available
        if ($log = $this->getLog()) {
            $log->log($errors->exception, Zend_Log::CRIT);
        }
        
        // conditionally display exceptions
        if ($this->getInvokeArg('displayExceptions') == true) {
            $this->view->exception = $errors->exception;
        }
        
        $this->view->request   = $errors->request;
    }

    public function getLog()
    {
        $bootstrap = $this->getInvokeArg('bootstrap');
        if (!$bootstrap->hasPluginResource('Log')) {
            return false;
        }
        $log = $bootstrap->getResource('Log');
        return $log;
    }


}

